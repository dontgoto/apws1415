\section{Auswertung}
\label{sec:Auswertung}

\subsection{Temperaturverlauf}
In Graph~\ref{fig:temperaturverlauf} werden die Temperaturen $T_1$ und $T_2$ der beiden Reservoire gegen die Zeit aufgetragen. 
$T_2$ bleibt erstaunlich konstant, der Grund dafür ist, dass die Rührer verspätet eingeschaltet wurden.
Die Steigung von $T_2$ ist hingegen vor und nach einschalten weitgehend konstant.
Es ist gut zu sehen wie sich beim verspäteten Einschalten der Rührer bei $t=\SI{420}{\second}$ $T_2$ sich abrupt ändert.

\begin{figure}
	\centering
    \includegraphics[width=1.00\textwidth]{plots/atemperaturverlauf.pdf}
	\caption{Temperaturen der Reservoire in der Zeit.}
	\label{fig:temperaturverlauf}
\end{figure}

\input{tabellen/a1.tex}

\subsection{Ausgleichsrechnung}
Im folgenden werden die beiden Temperaturverläufe jeweils durch ein Polynom der Form $A+ Bx + Cx^2 $ mit Hilfe der Methode der kleinsten Quadrate genähert.
Dazu wird folgende Gleichung 
\begin{equation*}
    \sum _i \left(y_i - A - Bx_i - Cx_i^2 \right) ^2
\end{equation*}
minimiert. 
Hierbei wird $y_i$ jeweils durch $T_1$ und $T_2$ und $x_i$ durch $t$ ersetzt.
Da die kaum vorhandene Änderung von $T_2$ bis $t=\SI{480}{\second}$ und dessen abruptes Ansteigen von 480--660\, s nicht mit den im folgenden zu bestimmenden Messgrößen der Wärmepumpe zusammenhängt, sondern mit dem verspäteten Einschalten der Rührer werden die Werte von $T_2$ zwischen 60\, s und 660\, s nicht zur Bestimmung des Polynoms verwendet.
Wenn man die Kurvenverläufe von $T_1$ und $T_2$ aus Abblidung~\ref{fig:fit} mit einander vergleicht kann man zu dem Schluss kommen, dass dies eine akzeptable Annahme ist.
Die Fehler der Differentialkoeffizienten berechnen sich nach Gaußscher Fehlerfortpflanzung aus den Fehlern der Koeffizienten:
~
\begin{equation}
   \Delta\frac{dT}{dt}=\sqrt{\left(2t\Delta{A}\right)^{2}+(\Delta{B})}\nonumber
\end{equation}
Die so berechneten Faktoren sind in Tabelle~\ref{tab:faktoren}.

\begin{table}
  \centering
  \caption{Faktoren der genäherten Polynome.}
  \label{tab:faktoren}
  \begin{tabular}{c c c}
    \toprule
    konst & $ T_1 [\si{\kelvin} ]$ & $ T_2 [\si{\kelvin} ] $  \\
    \midrule
    A $[\si{\kelvin} ] $ & $292.1\pm 0.88664$ & $299.3\pm 0.91434$\\
    B $[\si{\kelvin\per\second} ]$& $0.03472\pm 0.0031578$ & $ -0.04680\pm 0.0025798$\\
    C $[\si{\kelvin\per\second\squared} ]$& $-8e-06\pm 2e-06$& $ 2.077e-05\pm 0.17558e-05$\\
    \bottomrule
   \end{tabular}
\end{table}

\begin{figure}
	\centering
    \includegraphics[width=1.00\textwidth]{plots/bRegression.pdf}
	\caption{Angepasste Messwertmenge von $T_1$ und gefittete Polynome. Temperatur in Kelvin. }
	\label{fig:fit}
\end{figure}


\subsection{Güteziffer}

\begin{table}
  \centering
  \caption{Werte zur Bestimmung der Güteziffer.}
  \label{tab:Gueteziffer}
  \begin{tabular}{c c c c c}
    \toprule
    $t [ \si{\second} ]$ &  $ T_1'  [ \si{\kelvin} ]$ &   $\nu_\text{real} $ &  $\nu_\text{ideal} $ & $\bar{\mathup{N}} \si{\watt}$\\
    \midrule
    300 &  0.020296 & 1.34 & 38.66 & $180 \pm 13.69$ \\
    600 &  0.025107 & 1.71 & 11.96 & $194 \pm 16.06$  \\
    900 &  0.029917 & 2.19 & 7.52  & $200 \pm 14.88$ \\
    1200 & 0.033766 & 2.79 & 6.52  & $205 \pm 13.51$ \\
    \bottomrule
   \end{tabular}
\end{table}

$T_1 \prime$ ist die Ableitung der zuvor bestimmten Gleichung $T_{1(t)}$.
Durch die Gleichung~\eqref{eqn:Gueteziffer_real} lässt sich die Güteziffer bestimmen
 anhand der in~\ref{tab:Gueteziffer} zu findenden Werten und den Konstanten $m_1=3[\text{kg}]$, $c_w=4.182 \si{\joule\per\kilo\gram\kelvin}$ und $m_k c_k=\SI{660}{\joule\per\kelvin}$. %cite wiki EINHEITEN

\begin{equation}
    \nu_\text{real} = \frac{m_\mathup{w} c_\mathup{w} + m_\mathup{k} c_\mathup{k}}{\bar{N}} \frac{\Delta T_1}{\Delta t}
  \label{eqn:Gueteziffer_real}
\end{equation}

Die Arbeit $\bar{N}$ wurde durch \eqref{eqn:Ngemittelt} gemittelt,

\begin{equation}
  \bar{N}=\frac{1}{n}\sum_{i=1}^{n} N_i
  \label{eqn:Ngemittelt}
\end{equation}
 wobei $n$ hier die Anzahl der Watt-Messwerte jeweils bis $t$ sind.
Die ideelle Güteziffer ergibt sich aus:

\begin{equation}
  \nu_\text{ideal} = \frac{T_1}{T_1 - T_2}.
  \label{eqn:guetteziffer_ideal}
\end{equation}

Die errechneten Werte sind in~\ref{tab:Gueteziffer} zu finden.

\subsection{Massendurchsatz}

Der Massendurchsatz ergibt sich aus~\eqref{eqn:massendurchsatz} und ist in~\ref{tab:Massendurchsatz} zu finden.

\begin{equation}
  \frac{\Delta m}{\Delta t} = \frac{\Delta Q_2}{L \Delta t }
  \label{eqn:massendurchsatz}
\end{equation}
$L$ wird über eine Ausgleichsgerade, die mit der Methode der kleinsten Quadrate bestimmt wird, berechnet.
Hierfür wird die Funktion
\begin{equation*}
    \sum _i \left( y_i - A- Bx_i \right)^2
\end{equation*}
minimiert. Dabei ist $y_i = \ln (p_b)$ und $x_i = 1/T_1$.
Die berechneten Werte sind
\begin{eqnarray*}
    A = (8.1 \pm 0.2)\, [\mathup{K}]\\
    B = -1790 \pm 70
\end{eqnarray*}
Messwerte und Ausgleichsgerade sind in Graph~\ref{fig:linfit}.


\begin{figure}
	\centering
    \includegraphics[width=1.00\textwidth]{plots/dampfdruckkurve.pdf}
    \caption{Plot zur Bestimmung der Ausgleichsgeraden und Verdampfungswärme. }
	\label{fig:linfit}
\end{figure}

$L$ ist für \ce{CL2F2C} $\SI{123}{\joule\per\gram}$ mit einem Fehler von $\SI{5}{\joule\per\gram}$.

\begin{table}
  \centering
  \caption{Werte zur Bestimmung des Massendurchsatzes.}
  \label{tab:Massendurchsatz}
  \begin{tabular}{c c c}
    \toprule
    $t[ i\si{\second} ]$ & $T_2 \prime[\si{\kelvin} ]$ & $\frac{dm}{dt}[\si{\gram\per\second} ]$\\
    \midrule
    300 & -0.09416 & 3.6873 \\
    600 & -0.21880 & 2.3492 \\
    900 & -0.34343 & 1.0110 \\
    1200& -0.44313 & 3.2712 \\
    \bottomrule
   \end{tabular}
\end{table}


\subsection{Mechanische Arbeit des Kompressors}

\begin{table}
  \centering
  \caption{Konstanten zur Bestimmung der Kompressorleistung.}
  \label{tab:Kompressorleistung}
  \begin{tabular}{c c c}
    \toprule
    $\rho_0[\si{\gram\per\liter} $] & $\kappa$ & $p_a[ \si{\bar} ]$\\
    \midrule
    5.51 & 1.14 & 2.2\\
    \bottomrule
   \end{tabular}
\end{table}

Die zur Bestimmung der Kompressionsleistung nötigen Konstanten sind in~\ref{tab:Kompressorleistung} zu finden.
Der Wert $p_a$ wird hier auch als konstant angenommen, da er sich in den Messungen nicht über einen Lesefehler hinaus änderte.
Zur konkreten Berechnung der Kompressorleistung $W_m$ wird~\eqref{eqn:Kompressorleistung} verwendet.
~
\begin{equation}
  \label{eqn:Kompressorleistung}
  W_m = \frac{1}{\kappa-1}\left[p_b\left(\frac{p_a}{p_b}\right)^{\frac{1}{\kappa}}-p_a\right]\frac{1}{\rho_0}\frac{\Delta m}{\Delta t}
   \end{equation}
Die Ergebnisse von~\eqref{eqn:Kompressorleistung} sind in Tabelle ~\ref{tab:KompressorleistungErgebnisse} zu finden.
\begin{table}
  \centering
  \caption{Ergebnisse aus \eqref{eqn:Kompressorleistung} zur Kompressorleistung.}
  \label{tab:KompressorleistungErgebnisse}
  \begin{tabular}{c c}
    \toprule
    $p_b \si{\bar} $ & $W_{m}[\si{\watt}]$ \\ 
    \midrule
    8.8 &  23.915 \\
    10.6 &  70.815 \\
    11.4 & 159.56 \\
    12.0 & 202.33 \\
    \bottomrule
   \end{tabular}
\end{table}



#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat


##Messwerte aus Datei einlesen
zeit, t1, t2, pa, pb, N = np.genfromtxt('../messwerte/messwerte.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!

#Umrechnen
zeit = zeit*60 #von minuten nach sekunden
pb = np.log((pb+1)) #mit Korrektur +1bar
t1 = 1/(const.C2K(t1))
#Messwerte plotten
#Polynom fitten
def f1(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x   #+++
params1t1, covariance1t1 = curve_fit(f1, t1, pb) #+++ Namen der Variablen der Messwerte eintragen
error1t1 = np.sqrt(np.diag(covariance1t1))
paramswitherror = unp.uarray(params1t1, error1t1)
np.savetxt('../plots/parameter/dampfdruck', paramswitherror , fmt='%r', header='#dampfdruck A + B*x mit Fehler') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(np.linspace(0,10), f1(np.linspace(0,10),  *params1t1), 'b-', label='Ausgleichsgerade') #+++ Label anpassen
plt.plot(t1, pb, 'kx', label='Messwerte') #+++
print(params1t1)


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel('$1/T\, [1/\mathrm{K}]$') #+++
plt.ylabel('$\ln (p_b /\mathrm{bar})$') #+++
plt.xlim(0.00305, 0.00343 ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(1.9 ,2.6 ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.grid(True) #+++
#set_yscale("log") #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX


##Plot als Grafik speichern
plt.savefig('../plots/dampfdruckkurve.pdf') #+++ Dateiname und Typ anpassen

import numpy as np
import scipy.constants as const

zeit, t1C, t2C, pa, pb, N = np.genfromtxt('../messwerte/messwerte.txt', skip_header=1, unpack=True)
#zeit = zeit * 60

dT1dt = np.genfromtxt('../tabellen/dT1dt', skip_header=1, unpack=True)
#dT2dt = np.genfromtxt('../tabellen/dT2dt', skip_header=1, unpack=True)
t1 = const.C2K(t1C)
t2 = const.C2K(t2C)
cw = 4182
mkck = 660
m1 = 3
#mk =
c = m1*cw + mkck
print('c:', c)
#MN = [0, 0, 0, 0, 0]
j = 0
print("N:")
MN=np.array([np.mean(N[0:1:1]), np.mean(N[0:4:1]), np.mean(N[0:9:1]), np.mean(N[0:14:1]), np.mean(N[1:19:1])])
MNF=np.array([np.std(N[0:1:1]), np.std(N[0:4:1]), np.std(N[0:9:1]), np.std(N[0:14:1]), np.std(N[0:19:1])])
print("MN")
print(MN)
print("MNF")
print(MNF)
print("DONE")
#for k in [1, 5, 10 ,15, 20]:
#    z = k-1
#    while z >= 0:
#        MN[j] = MN[j] + N[z]
#        z = z -1
#    MN[j] = 1/k * MN[j]
#    print(MN[j])
#    j = j + 1
#j = 0
#60s, 300s, 600s, 900s, 1200s
#1m ,5m ,10m, 15m, 20m
v_real = [0, 0, 0, 0, 0]
i = 4
print(MN)
print(dT1dt)
print(c)
print("v_real:")
while i >= 0:
    v_real[i] = (c * dT1dt[i])/MN[i]
    print(v_real[i])
    i = i -1
print("v_ideal:")
v_ideal = [0, 0, 0, 0, 0]
for g in [0, 4, 9, 13, 18]:
    v_ideal[j] = t1[g]/(t1[g]-t2[g])
    print(v_ideal[j])
    j = j+1
np.savetxt('../tabellen/v_real', v_real, fmt='%1.4e', header="v_real bei 1m,5m,10m,15m,20m:")
np.savetxt('../tabellen/v_ideal', v_ideal, fmt='%1.4e', header="videal bei1m,5m,10m,15m,20m:")

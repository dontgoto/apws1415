# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
zeit, t1, t2, pa, pb, N = np.genfromtxt('../messwerte/messwerte.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!

#Umrechnen
zeit = zeit*60 #von minuten nach sekunden
pa = pa+1
pb = pb+1

#Messwerte plotten
plt.plot(zeit, t1, 'kx', label='Temperatur 1') #+++
plt.plot(zeit, t2, 'x', label='Temperatur 2') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel('Zeit[s]') #+++
plt.ylabel('Temperatur[°C]') #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.grid(True) #+++
#set_yscale("log") #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX


##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)

##Plot als Grafik speichern
plt.savefig('../plots/atemperaturverlauf.pdf') #+++ Dateiname und Typ anpassen

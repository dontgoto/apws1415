\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Transport von Wärme}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Aufbau der Wärmepumpe}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Bestimmung der Kenngrößen}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchführung}{5}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Temperaturverlauf}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Ausgleichsrechnung}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Güteziffer}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Massendurchsatz}{9}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Mechanische Arbeit des Kompressors}{11}{subsection.3.5}
\contentsline {section}{\numberline {4}Diskussion}{11}{section.4}
\contentsline {section}{\nonumberline Literatur}{12}{section*.12}

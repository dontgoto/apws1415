\select@language {ngerman}
\contentsline {section}{\numberline {1}Theorie}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Transport von W�e}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Prinzipieller Aufbau der W�epumpe}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Bestimmung der Kenngr�n}{2}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchf�ung}{3}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Temperaturverlauf}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Ausgleichsrechnung}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}G�ziffer der verwendeten W�epumpe}{6}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Bestimmung des Massendurchsatzes}{7}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Mechanische Kompressorleistung}{9}{subsection.3.5}
\contentsline {section}{\numberline {4}Diskussion}{10}{section.4}
\contentsline {section}{\numberline {5}Literatur}{10}{section.5}

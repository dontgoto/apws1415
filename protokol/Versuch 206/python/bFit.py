# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
zeit, t1, t2, pa, pb, N = np.genfromtxt('../messwerte/messwerte.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!


#Umrechnen
zeit = zeit*60 #von minuten nach sekunden
t1 = const.C2K(t1)
t2 = const.C2K(t2)
pa = (pa+1)*100000
pb = (pb+1)*100000


#Messwerte plotten
#plot erst ab der 13ten minute für t2 da vorher offensichtlich unbrauchbar, bei t1 keine so große veränderung durch einschalten der pumpe
plt.plot(zeit, t1, 'kx', label='Temperatur 1') #+++
plt.plot(zeit[np.array([11,12,13,14,15,16,17,18,19])], t2[np.array([11,12,13,14,15,16,17,18,19])], 'x', label='Temperatur 2') #+++
#Polynom fitten
def f1(x, a, b, c): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x + c*(x**2)   #+++
params1t1, covariance1t1 = curve_fit(f1, zeit, t1) #+++ Namen der Variablen der Messwerte eintragen
error1t1 = np.sqrt(np.diag(covariance1t1))
np.savetxt('../plots/parameter/f1t1', params1t1 , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung
np.savetxt('../plots/parameter/f1t1fehler', error1t1 , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung

params1t2, covariance1t2 = curve_fit(f1, zeit, t2) #+++ Namen der Variablen der Messwerte eintragen
error1t2 = np.sqrt(np.diag(covariance1t2))
params1t2, covariance1t2 = curve_fit(f1, zeit[np.array([1, 11, 12, 13, 14, 15, 16, 17, 18, 19, 19])], t2[np.array([1, 11, 12, 13, 14, 15, 16, 17, 18, 19, 19])])
error1t2 = np.sqrt(np.diag(covariance1t2))
np.savetxt('../plots/parameter/f1t2', params1t2 , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung
np.savetxt('../plots/parameter/f1t2fehler', error1t2 , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung
print('error1t1:',error1t1)


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(zeit, f1(zeit, *params1t1), '-', label='Fit von $T_1$') #+++ Label anpassen
plt.plot(zeit[np.array([11,12,13,14,15,16,17,18,19])],  f1(zeit[np.array([11,12,13,14,15,16,17,18,19])], *params1t2), 'b-', label='Fit von $T_2$') #+++ Label anpassen

##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel('Zeit [s]') #+++
plt.ylabel('Temperatur [K]') #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.grid(True) #+++
#set_yscale("log") #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX



##Plot als Grafik speichern
plt.savefig('../plots/bRegression.pdf') #+++ Dateiname und Typ anpassen

\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Die Linsen}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Brennweiten Bestimmung nach Bessel}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Brennweiten Bestimmung nach Abbe.}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchführung}{5}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Verifizierung der Linsengleichungen}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Bestimmung einer unbekannten Linsenbrennweite}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Berechnung der Brennweite nach Bessel}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Bestimmung der Brennweite nach Abbe}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}Diskussion}{11}{section.4}
\contentsline {section}{\nonumberline Literatur}{11}{section*.15}

# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
bildweite1, gegenstandsweite1, bildhöhe1 = np.genfromtxt('../messwerte/150mm.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
bildweite2, gegenstandsweite2 = np.genfromtxt('../messwerte/unbekanntMM.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!

#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen


brennweite1 = 1/(1/gegenstandsweite1 + 1/bildweite1)
brennweite1 *=10
print("brennweite1")
print(brennweite1)
for c1 in brennweite1:
    print ("%.0f" % (c1))
print(np.mean(brennweite1))
print(np.std(brennweite1))
#print(repr(x).rjust(2), repr(x*x).rjust(3), end=' ')
gegenstandshöhe = 2.968
bdurchg1 = bildweite1/gegenstandsweite1
BdurchG1 = bildhöhe1/gegenstandshöhe
print("BdurchG")
for c1 in BdurchG1:
    print ("%.2f" % (c1))

print("bdurchg")
for c1 in bdurchg1:
    print ("%.2f" % (c1))
print("bdurchg1 / BdurchG1")
print(bdurchg1 / BdurchG1)
print(bdurchg1 / BdurchG1)
print(np.mean(bdurchg1 / BdurchG1))
print(np.std(bdurchg1 / BdurchG1))
print("brennweite2")
brennweite2 = 1/(1/gegenstandsweite2 + 1/bildweite2)
for c1 in brennweite2:
    print ("%.1f" % (c1))
print(np.mean(brennweite2))
print(np.std(brennweite2))


plt.plot([gegenstandsweite1[1],0 ], [0,bildweite1[1]], 'r-', label='')
plt.plot([gegenstandsweite1[0],0 ], [0,bildweite1[0]], 'b-', label='')
plt.plot([gegenstandsweite1[2],0 ], [0,bildweite1[2]], 'g-', label='')
plt.plot([gegenstandsweite1[3],0 ], [0,bildweite1[3]], 'y-', label='')
plt.plot([gegenstandsweite1[4],0 ], [0,bildweite1[4]], 'k-', label='')
plt.plot([gegenstandsweite1[5],0 ], [0,bildweite1[5]], 'm-', label='')
plt.plot([gegenstandsweite1[6],0 ], [0,bildweite1[6]], 'o-', label='')
plt.plot([gegenstandsweite1[7],0 ], [0,bildweite1[7]], 'c-', label='')
plt.plot([gegenstandsweite1[8],0 ], [0,bildweite1[8]], 's-', label='')

#plt.show()
plt.savefig('../plots/150mm.pdf') #+++ Dateiname und Typ anpassen
plt.close()


plt.plot([gegenstandsweite2[1],0 ], [0,bildweite2[1]], 'r-', label='')
plt.plot([gegenstandsweite1[0],0 ], [0,bildweite2[0]], 'b-', label='')
plt.plot([gegenstandsweite2[2],0 ], [0,bildweite2[2]], 'g-', label='')
plt.plot([gegenstandsweite2[3],0 ], [0,bildweite2[3]], 'y-', label='')
plt.plot([gegenstandsweite2[4],0 ], [0,bildweite2[4]], 'k-', label='')
plt.plot([gegenstandsweite2[5],0 ], [0,bildweite2[5]], 'm-', label='')
plt.plot([gegenstandsweite2[6],0 ], [0,bildweite2[6]], 'o-', label='')
plt.plot([gegenstandsweite2[7],0 ], [0,bildweite2[7]], 'c-', label='')
plt.plot([gegenstandsweite2[8],0 ], [0,bildweite2[8]], 's-', label='')

#plt.show()
plt.savefig('../plots/unbekannt.pdf') #+++ Dateiname und Typ anpassen
plt.close()


b1, g1, b2,g2, gesamt, = np.genfromtxt('../messwerte/bessel.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
distanz = g1-b1
brennweite = (gesamt**2 - distanz**2)/(4*gesamt)
brennweite *= 10
print("brennweiteweiß")
print(brennweite)
for c1 in brennweite:
    print ("%.0f" % (c1))
print(np.mean(brennweite))
print(np.std(brennweite))


b1, g1, b2,g2, gesamt, = np.genfromtxt('../messwerte/besselblau.txt', skip_header=0, unpack=True) #+++Richtige Namen für Variablen!
distanz = g1-b1
brennweite = (gesamt**2 - distanz**2)/(4*gesamt)
brennweite *= 10
print("brennweitegrün")
print(brennweite)
for c1 in brennweite:
    print ("%.0f" % (c1))
print(np.mean(brennweite))
print(np.std(brennweite))

b1, g1, b2,g2, gesamt, = np.genfromtxt('../messwerte/besselrot.txt', skip_header=0, unpack=True) #+++Richtige Namen für Variablen!
distanz = g1-b1
brennweite = (gesamt[0:5]**2 - distanz[0:5]**2)/(4*gesamt[0:5])
brennweite *= 10
print("brennweiterot")
print(brennweite)
for c1 in brennweite:
    print ("%.0f" % (c1))
print(np.mean(brennweite))
print(np.std(brennweite))



gstrich, bstrich, bildhöhe = np.genfromtxt('../messwerte/abbe.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
V = bildhöhe/gegenstandshöhe
h = 3










xValue1 = (1+1/V)
yValue1 = gstrich
xValue2 = 1+V
yValue2 = bstrich

##Polynom fitten
def f(x, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return  b*x  #+++
params, covariance = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
uparam1= ufloat(params[0], errors[0])
#uparam2= ufloat(params1[1], errors[1])
print("abbe1")
print('{:L}'.format(uparam1))
#print('{:L}'.format(uparam2))

#unp.uarray.paramswitherror = (params, error)


##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(xValue1), np.amax(xValue1)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen

#Messwerte plotten
plt.plot(xValue1, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$g\' /\mathrm{cm}$') #+++
plt.xlabel(r'$1 + 1/V$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt

plt.savefig('../plots/abbe1.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
plt.close()

xValue1 = 1+V
yValue1 = bstrich

##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a+   b*x   #+++
params, covariance = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
uparam1= ufloat(params[0], errors[0])
uparam2= ufloat(params[1], errors[1])
print("abbe2")
print('{:L}'.format(uparam1))
print('{:L}'.format(uparam2))

#unp.uarray.paramswitherror = (params, error)


##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(xValue1), np.amax(xValue1)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen

#Messwerte plotten
plt.plot(xValue1, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$b\' /\mathrm{cm}$') #+++
plt.xlabel(r'$1 + V$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt

plt.savefig('../plots/abbe2.pdf') #+++ Dateiname und Typ anpassen
#plt.show()

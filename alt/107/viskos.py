import uncertainties
import scipy.constants as const
import matplotlib as mpl
import matplotlib.pyplot as plt
from uncertainties import ufloat
import uncertainties.unumpy as unp
import numpy as np
from scipy.optimize import curve_fit



rhofl=1000
dx=0.1
mkl=4.4531*0.001
rkl=ufloat(15.64*0.001/2, 0.02*0.001)
rgr= ufloat(15.80*0.00/2, 0.02*0.001)
rhokugel= mkl/((4/3) *np.pi* rkl**3)
mgr=rhokugel * (4/3) *np.pi* rgr**3
print("rhokugel", rhokugel)
print("mgr",mgr)
mgr=4.4987*0.001
Kkl= 0.0764* (0.01**3)/0.001
#2)
T=np.array([19.5,35,38,41,44,47,50,53,56,59,62])
T = const.C2K(T)
t = unp.uarray([0,0,0,0,0,0,0,0,0,0,0] , [0,0,0,0,0,0,0,0,0,0,0])
traumgr= ufloat(np.mean(np.genfromtxt("./messwerte/raumtempgross.txt")), np.std(np.genfromtxt("./messwerte/raumtempgross.txt")))
traumkl= ufloat(np.mean(np.genfromtxt("./messwerte/raumtempklein.txt")), np.std(np.genfromtxt("./messwerte/raumtempklein.txt")))
t[0] = traumgr
t[1] = ufloat(np.mean(np.genfromtxt("./messwerte/35.txt")) , np.std(np.genfromtxt("./messwerte/35.txt")))
t[2] = ufloat(np.mean(np.genfromtxt("./messwerte/38.txt")) , np.std(np.genfromtxt("./messwerte/38.txt")))
t[3] = ufloat(np.mean(np.genfromtxt("./messwerte/41.txt")) , np.std(np.genfromtxt("./messwerte/41.txt")))
t[4] = ufloat(np.mean(np.genfromtxt("./messwerte/44.txt")) , np.std(np.genfromtxt("./messwerte/44.txt")))
t[5] = ufloat(np.mean(np.genfromtxt("./messwerte/47.txt")) , np.std(np.genfromtxt("./messwerte/47.txt")))
t[6] = ufloat(np.mean(np.genfromtxt("./messwerte/50.txt")) , np.std(np.genfromtxt("./messwerte/50.txt")))
t[7] = ufloat(np.mean(np.genfromtxt("./messwerte/53.txt")) , np.std(np.genfromtxt("./messwerte/53.txt")))
t[8] = ufloat(np.mean(np.genfromtxt("./messwerte/56.txt")) , np.std(np.genfromtxt("./messwerte/56.txt")))
t[9] = ufloat(np.mean(np.genfromtxt("./messwerte/59.txt")) , np.std(np.genfromtxt("./messwerte/59.txt")))
t[10] = ufloat(np.mean(np.genfromtxt("./messwerte/62.txt")) , np.std(np.genfromtxt("./messwerte/62.txt")))
print("t:",t)
etaraum= Kkl*traumkl*(rhokugel-rhofl)
print("etaraum:",etaraum)
K = etaraum/((rhokugel-rhofl)*traumgr)
print("K:",K)

eta= K*t*(rhokugel-rhofl)
print(eta)
#Messwerte plotten
plt.plot(1/T, np.log(unp.nominal_values(eta) ), 'kx', label='Messwerte') #+++
##Achsenbeschriftungen, kosmetisches
plt.xlabel(r'$T\, [\mathrm{K^{-1}} ]$') #+++
plt.ylabel(r'$ \mathrm{ln} (\mathrm{\eta}/\mathrm{Pa \, s}) $') #+++
plt.grid(True) #+++
plt.xlim(1/340, 1/290 ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(0 ,0.004*1000 ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung


def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a*np.exp(b/x)  #+++
params, covariance = curve_fit(f, T, unp.nominal_values(eta)) #+++ Namen der Variablen der Messwerte eintragen
error = np.sqrt(np.diag(covariance))
#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
print("params:",params,"error:",error)

#Plot gefittetes Polynom
x_plot = np.linspace(np.amin(270), np.amax(350), num=1000) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(1/x_plot, np.log(f(x_plot, *params) ), 'b-', label='linearer Fit') #+++ Label anpassen

plt.legend(loc="best")
###Plot als Grafik speichern
plt.savefig('plots/plot1.pdf') #+++ Dateiname und Typ anpassen

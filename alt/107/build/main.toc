\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Theoretische Grundlagen}{3}{subsection.1.1}
\contentsline {section}{\numberline {2}Durchführung}{3}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Bestimmung der Apparaturkonstante $K$}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Temperaturabhängigkeit der Viskosität $\miteta $on destilliertem Wasser}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bestimmung der Reynoldszahl}{6}{subsection.3.3}
\contentsline {section}{\numberline {4}Diskussion}{9}{section.4}
\contentsline {section}{\nonumberline Literatur}{9}{section*.7}

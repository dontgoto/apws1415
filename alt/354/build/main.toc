\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Gedämpfte Schwingung}{3}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Überdämpfung oder Kriechfall: $\frac {R^2}{4L^2} > \frac {1}{LC}$}{4}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Schwingfall: $\frac {R^2}{4L^2} < \frac {1}{LC}$}{4}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Erzwungene Schwingung}{6}{subsection.1.2}
\contentsline {section}{\numberline {2}Durchführung}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Zeitabhängigkeit der Amplitude einer gedämpften Schwingung}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Aperiodischer Grenzfall}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Frequenzabhängigkeit der Kondensatorspannung und Phase}{7}{subsection.2.3}
\contentsline {section}{\numberline {3}Auswertung}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Zeitabhängigkeit der Schwingungsamplitude}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Aperiodischer Grenzfall}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bestimmung der Resonanzüberhöhung}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Frequenzabhängigkeit der Phase}{14}{subsection.3.4}
\contentsline {section}{\numberline {4}Diskussion}{15}{section.4}
\contentsline {section}{\nonumberline Literatur}{15}{section*.17}

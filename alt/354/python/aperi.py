import numpy as np
import uncertainties as unp
from uncertainties import ufloat
C = ufloat(2.098, 0.006)
L = ufloat(10.11, 0.03)
R = 2*(L/C)**(0.5)
print(R)

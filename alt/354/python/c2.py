# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from uncertainties import ufloat
from  uncertainties import unumpy as unp
#import uncertainties.unumpy as unp
#from uncertainties import ufloat

##Messwerte aus Datei einlesen
Uc, Ug, nu  = np.genfromtxt('../messwerte/c.txt', unpack=True) #+++Richtige Namen für Variablen!
test = Uc + Ug + nu
Uc = Uc*10
#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
#nu = np.log(nu)
U = Uc/Ug

#Messwerte plotten
plt.plot(nu, U, 'kx', label='Messwerte') #+++
plt.plot(np.array([0, 10000]),np.array([U.max(), U.max()])/np.sqrt(2) , 'r-', label='$\\frac{1}{\\sqrt{2}}U_\mathrm{max}$') #+++

C = ufloat(2.098, 0.006)*10**-9
L = ufloat(10.11 , 0.03)*10**-3
R = ufloat(509.5 + 50, 0.5) #R_2 + 50Ohm
reskurve = np.where(Uc > Uc.max()/np.sqrt(2))
breite = nu[reskurve].max()- nu[reskurve].min()
q = Uc.max()/Ug
breite_theo = R/(L*2*np.pi)
q_theo = 1/(R*C) * unp.sqrt(L*C) #irgendwo faktor 10 zu wenig
print('3 significant digits on the uncertainty: {:.3u}'.format(q_theo))
x = nu[reskurve]
plt.axvline(x.min(), linestyle='--')
plt.axvline(x.max(), linestyle='--')
print("breite:",breite)
print("breite_theo:",breite_theo)
print("q:",q.mean())
print("q_theo:",q_theo)
print("q_neu:", (1/(breite*1000 * unp.sqrt(L*C))))

##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$U_\mathrm{C} / U_\mathrm{G}$') #+++
plt.xlabel(r'$\nu [\mathrm{k}\mathrm{H}\mathrm{z}]$') #+++
#plt.grid(True) #+++
plt.xlim(25 , 45 ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.xscale('log') #+++ Vielleicht noch log Skalierung
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)

test = ufloat(5.48, 0.08)
print(1/test)


##Plot als Grafik speichern
plt.savefig('../plots/c2.pdf') #+++ Dateiname und Typ anpassen





#d)
nu1exp= 5.38*(80/13.5)
nu0exp= 5.88*(80/13.5)
nu2exp= 6.6*(80/13.5)
print("nu0exp:", nu0exp)
print("nu1exp", nu1exp)
print("nu2exp:", nu2exp)
print("nudiffexp:", nu2exp - nu1exp)
nu0 = unp.sqrt(1/(L*C))
nu0 = nu0/(2*np.pi)
nudiff= R/L
print("nu0:", nu0)
print("nudiff:", nudiff)
nures = unp.sqrt(1/(L*C) - (R**2)/(2*L**2))
nures = nures/(2*np.pi)
print("nures:",nures)
print("qneu1:",((8.807*1000)/(nures)))
print("qneu2:",(nu0exp/nudiff))

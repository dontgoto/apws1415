# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
#maxima
amplitude = np.array([6.15, 5.4, 4.95, 3.8, 3.2, 2.75, 2.4, 2.08, 1.75, 1.4, 1.2, 1.15, 0.9, 0.75, 0.65, 0.59 ])
amplitude = 500*10**(-3)*amplitude/2.08
print("A:",amplitude)
amplitude = np.log(amplitude)
#2.08cm=50µs = 500mV
t = np.array([0, 1.223, 2.446, 3.669, 4.893, 6.116, 7.34, 8.563, 9.786, 11.01, 1.223*10, 1.223*11, 1.223*12, 1.223*13, 1.223*14, 1.223*15])
t = t+ 1.68
t= (t/2.08)*50*10**(-6)
print("t:",t)
#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen

#Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*np.exp(-2*np.pi*c*x)  #+++
        return a + b*x  #+++
params, covariance = curve_fit(f, t,  amplitude ) #+++ Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
print("params:",params)
print("error:",errors)

print("L:",2*ufloat(params[1], errors[1])*ufloat(10.11, 0.03)*10**(-3))
#Plot gefittetes Polynom
x_plot = np.linspace(np.amin(t), np.amax(t)) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot*10**6, f(x_plot, *params), 'b-', label='Regressionsgerade') #+++ Label anpassen


#Messwerte plotten
plt.plot(t*10**6, amplitude, 'kx', label='Messpunkte') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$t \, [\mathrm{\mu s}]$') #+++
plt.ylabel(r'$\mathrm{ln}(A)$') #+++
#plt.grid(True) #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung



##Plot als Grafik speichern
plt.savefig('../plots/a1.pdf') #+++ Dateiname und Typ anpassen

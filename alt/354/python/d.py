# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
nu, a, Uc, Ug = np.genfromtxt('../messwerte/d.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
nu=nu*1000
a = a/1000000
b = 1/nu
#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
phi = a/b * 360

#Messwerte plotten
plt.plot(nu/1000, phi, 'kx', label='Messwerte') #+++

plt.plot(np.array([0,90]), np.array([45,45]), 'r----', label='45°') #+++
plt.plot(np.array([0,90]), np.array([135,135]), 'b----', label='135°') #+++
plt.plot(np.array([0,90]), np.array([90,90]), 'g----', label='90°') #+++
np.savetxt('../messwerte/dneu.txt', np.array([nu/1000, a*1000000, b*1000000, phi ]).T, delimiter=' &  ' , newline='\\\\' , header="", comments='%')

##Achsenbeschriftungen, kosmetisches
#plt.legend(loc="best")
plt.xlabel(r'$\nu \, [\mathrm{k}\mathrm{H}\mathrm{z}]$') #+++
plt.ylabel(r'Phasenverschiebung $[^\circ ]$') #+++
#plt.grid(True) #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#title('title') #+++ Titel eher als Annotation in LaTeX


##Plot als Grafik speichern
plt.savefig('../plots/d1.pdf') #+++ Dateiname und Typ anpassen

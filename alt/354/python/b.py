import numpy as np
import uncertainties as unp
from uncertainties import ufloat

#L = unp.ufloat(10.11 , 0.03)
#C= unp.ufloat(2.098 , 0.006)
L = unp.ufloat(10.14 , 0.03)
C= unp.ufloat(2.088 , 0.006)
R1= unp.ufloat(48.1, 0.1)
R2= unp.ufloat(509.5 ,0.5)
RG= unp.ufloat(50.0 ,0 )
Ri= unp.ufloat(10000000.0, 0)
Rap= unp.ufloat(3600.0 ,0 )
R = Ri + R2
#b):

RapTheo = (4*L/C)**(0.5)
print("b) Rap Theoriewert:", RapTheo)

#c):
#omega1 - omega = R/L
referenzwert = R/L
print("c) referenzwert Theoriewert:", referenzwert)

#d):
omega1 = R/(2*L) + (R**2 /(4* L**2)+ 1/(L*C))**(0.5)
omega2 = -R/(2*L) + (R**2 /(4* L**2)+ 1/(L*C))**(0.5)
#omegaRes = (-R**2/(4* L**2)+ 1/(L*C))**(0.5)
print("d) omega1 Theoriewert:", omega1)
print("d) omega2 Theoriewert:", omega2)
#print("d) omegaRes Theoriewert:", omegaRes)


#a)



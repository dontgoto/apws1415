# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
#from scipy.optimize import curve_fit
#from matplotlib import ticker
#import uncertainties.unumpy as unp
#from uncertainties import ufloat

##Messwerte aus Datei einlesen
Uc, Ug, nu  = np.genfromtxt('../messwerte/c.txt', unpack=True) #+++Richtige Namen für Variablen!
test = np.array([1,2,3,4])

#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
#nu = np.log(nu)
U = Uc/Ug

#Messwerte plotten
plt.plot(nu, U, 'kx', label='Messwerte') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$U_\mathrm{C} / U_\mathrm{G}$') #+++
plt.xlabel(r'$\nu\, [ \mathrm{kHz} ] $') #+++
#plt.grid(True) #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.xscale('log') #+++ Vielleicht noch log Skalierung
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##Plot als Grafik speichern
plt.savefig('../plots/c1.pdf') #+++ Dateiname und Typ anpassen

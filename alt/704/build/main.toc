\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Wirkungsquerschnitt und Absorptionsgesetz}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Gamma-Strahlung}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Beta-Strahlung}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchführung}{7}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Bestimmung der Absorptionskoeffizienten von Pb und Fe bei Gammastrahlung}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Bestimmung der Absorptionsmechanismen}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bestimmung der Absorptionskoeffizienten von Blei und Eisen bei Betastrahlung}{10}{subsection.3.3}
\contentsline {section}{\numberline {4}Diskussion}{11}{section.4}
\contentsline {section}{\nonumberline Literatur}{11}{section*.12}

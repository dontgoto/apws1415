# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
BleiD, BleiT, BleiN = np.genfromtxt('../messwerte/gamma.txt', skip_header=1, unpack=True)
EisenD, EisenT, EisenN = np.genfromtxt('../messwerte/eisengamma.txt', skip_header=1, unpack=True)
BetaD, BetaT, BetaN = np.genfromtxt('../messwerte/platten.txt', skip_header=1, unpack=True)

#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen
BleiD = BleiD/1000
EisenD = EisenD/1000
BetaD = BetaD*0.001*0.001
GammaNull = 713/650
BetaNull = 416/1010

#print('{:L}'.format(GammaNull))
BleiN = BleiN - (BleiT*GammaNull)
EisenN = EisenN - (EisenT*GammaNull)
BetaN = BetaN - (BetaT*BetaNull)
BleiNt = BleiN /BleiT
EisenNt = EisenN /EisenT
BetaNt = BetaN /BetaT
print("BetaNt")
print(BetaNt)
BleiFehler = np.sqrt(BleiN)
EisenFehler = np.sqrt(EisenN)
BetaFehler = np.sqrt(BetaN)
print(BleiN)
print("fehler blei")
for c1 in BleiFehler:
    print ("%.0f" % (c1))
print("fehler eisen")
for c1 in EisenFehler:
    print ("%.0f" % (c1))
print("fehler beta")
for c1 in BetaFehler:
    print ("%.0f" % (c1))

uEisenN = unp.uarray(EisenNt, np.sqrt(EisenN)/EisenT)
print("uEisenN")
print(uEisenN)
uBleiN  = unp.uarray(BleiNt, np.sqrt(BleiN)/BleiT)
uBetaN  = unp.uarray(BetaNt, np.sqrt(BetaN)/BetaT)

uBleiN  = unp.log(uBleiN)
uEisenN = unp.log(uEisenN)
uBetaN  = unp.log(uBetaN)

yValue1 =unp.nominal_values(uBleiN)
xValue1 =BleiD
yValue2 =unp.nominal_values(uEisenN)
xValue2 =EisenD
yValue3 =unp.nominal_values(uBetaN)
xValue3 =BetaD
#print('{:L}'.format(uBleiN))


ep = 1.295
sigmaCom = 2*np.pi*((2.82e-15)**2)* ( (1+ep)/ep**2 * ( (2*(1+ep))/(1+2*ep) - 1/ep * np.log(1+2*ep) ) +  1/(2*ep) * np.log(1+2*ep) - (1+3*ep)/((1+2*ep)**2)   ) #ist richtig
print("sigmaCom")
print(sigmaCom)

rho = np.array([11.34, 7.87])*1000
NL = 6.023e23
z = np.array([82, 26])
M = np.array([207.2, 55.85])
mu = rho * sigmaCom * z *NL / M
print("mu")
print(mu)


##Polynom fitten
def f(x, a, b ): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x  #+++
params1, covariance = curve_fit(f, xValue1*1000, yValue1) # Namen der Variablen der Messwerte eintragen
errors1 = np.sqrt(np.diag(covariance))
uparam1= ufloat(params1[0], errors1[0])
uparam2= ufloat(params1[1], errors1[1])

print( "BleiFit" )
print('{:L}'.format(uparam1))
print('{:L}'.format(uparam2))

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(xValue1*1000), np.amax(xValue1*1000)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params1), 'b-', label='Ausgleichsgerade') #+++ Label anpassen

#vllt errorbar plot
yerr1= unp.std_devs(uBleiN)
print("yerr1")
print(yerr1)
#xerr1=

plt.errorbar(xValue1*1000, yValue1, yerr=yerr1, fmt='kx', label='Messwerte mit Fehlerbalken')


##vllt 1sigma bereich darstellen
#plt.plot(x_plot, f(x_plot,*plus1-minus1)1000, 'r-', alpha=0)
#plt.plot(x_plot, f(x_plot,*plus1+minus1), 'r-', alpha=0)
#plt.fill_between(x_plot, f(x_plot,*plus1-minus1)*1000, f(x_plot,*plus1+minus1)*1000, facecolor='green', alpha=0.15)


#Messwerte plotten
plt.plot(xValue1*1000, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$\mathrm{ln}(n/\mathrm{s^{-1}})$') #+++
plt.xlabel(r'$d/\mathrm{mm}$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')



##Plot als Grafik speichern
plt.savefig('../plots/gammablei.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
plt.close()

params2, covariance = curve_fit(f, xValue2*1000, yValue2) # Namen der Variablen der Messwerte eintragen
errors2 = np.sqrt(np.diag(covariance))
uparam1= ufloat(params2[0], errors2[0])
uparam2= ufloat(params2[1], errors2[1])
print("EisenFit")
print('{:L}'.format(uparam1))
print('{:L}'.format(uparam2))

x_plot = np.linspace(np.amin(xValue2*1000), np.amax(xValue2*1000)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params2), 'b-', label='Ausgleichsgerade') #+++ Label anpassen

yerr2= unp.std_devs(uEisenN)
print("yerr2")
print(yerr2)
plt.plot(xValue2*1000, yValue2, 'kx', label='')
plt.errorbar(xValue2*1000, yValue2, yerr=yerr2, fmt='kx', label='Messwerte mit Fehlerbalken')

##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$\mathrm{ln}(n/\mathrm{s^{-1}})$') #+++
plt.xlabel(r'$d/\mathrm{mm}$') #+++

plt.savefig('../plots/gammaeisen.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
plt.close()




params3, covariance = curve_fit(f, xValue3[0:3:]*1000, yValue3[0:3:]) # Namen der Variablen der Messwerte eintragen
errors3 = np.sqrt(np.diag(covariance))
uparam11= ufloat(params3[0], errors3[0])
uparam21= ufloat(params3[1], errors3[1])
print("betafit1")
print('{:L}'.format(uparam11))
print('{:L}'.format(uparam21))

x_plot = np.linspace(np.amin(xValue3[0:2:]*1000), np.amax(xValue3[0:2:]*1000)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params3), 'b-', label='$y = A_1 + B_1*x$') #+++ Label anpassen

params3, covariance = curve_fit(f, xValue3[4::]*1000, yValue3[4::]) # Namen der Variablen der Messwerte eintragen
errors3 = np.sqrt(np.diag(covariance))
uparam12= ufloat(params3[0], errors3[0])
uparam22= ufloat(params3[1], errors3[1])
print("betafit2")
print('{:L}'.format(uparam12))
print('{:L}'.format(uparam22))

Rmax =2.7* (uparam22 - uparam21)/(uparam11 - uparam12)
print("Rmax")
print(Rmax)
Emax = 1.92 * unp.sqrt(Rmax**2 - 0.22*Rmax)
print("Emax")
print(Emax)

x_plot = np.linspace(np.amin(xValue3[4::]*1000), np.amax(xValue3[4::]*1000)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params3), 'g-', label='$y = A_2 + B_2*x$') #+++ Label anpassen

yerr3= unp.std_devs(uBetaN)
print("yerr3")
print(yerr3)
plt.plot(xValue3*1000, yValue3, 'kx', label='')
plt.errorbar(xValue3*1000, yValue3, yerr=yerr3, fmt='kx', label='Messwerte mit Fehlerbalken')

##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$\mathrm{ln}(n/\mathrm{s^{-1}})$') #+++
plt.xlabel(r'$d/\mathrm{mm}$') #+++

plt.savefig('../plots/beta.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
#plt.close()




print("EisenD")
print(BetaD*1000)
print(BetaT)
print(BetaN)
print(uBetaN)

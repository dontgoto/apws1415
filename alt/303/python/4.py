import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
import uncertainties.unumpy as unp
from uncertainties import ufloat
import uncertainties.unumpy as unp
import uncertainties


#Messwerte aus Datei einlesen
abstand, amplitude, gain = np.genfromtxt('../messwerte/4.txt', unpack=True) #+++
#amplitude = amplitude - amplitude[27]
amplitude = amplitude
amplitude = amplitude[0:26]
abstand = abstand[0:26]
abstand = abstand*0.01
abstand = abstand - 0.02
abstand = np.log(abstand)
gain = gain[0:26]
print(abstand)
bereinigt = amplitude/gain
print(bereinigt)
bereinigt = np.log(bereinigt)
table = np.array([abstand, amplitude, gain, bereinigt])


#Ausgabe der berechneten Werte zur Weiterverarbeitung in Tabellenform
np.savetxt('../tabellen/4.txt', table.T, delimiter=' & ' , newline='\\\\ ' , header="", comments='%', fmt='%1.3g')


#Vielleicht noch plotten...
##Polynom fitten
def f(x,a,  b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a+  b*x   #+++

params, covariance = curve_fit(f, abstand, bereinigt) #+++ Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
#np.savetxt('polynomfaktor.txt', unp.uarray([params, error]) , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung
##Messwerte plotten
plt.plot(abstand, bereinigt, 'kx', label='Signalstärke') #+++
print(params)
print(errors)
##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(abstand-5), np.amax(abstand+10)) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Regressionsgerade') #+++ Label anpassen


##Achsenbeschriftungen, kosmetisches
#plt.xlabel('') #+++
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xscale('log') #+++ Vielleicht noch log Skalierung
plt.xlabel(r'$\mathrm{ln}(\frac{\Delta x}{\mathrm{m}} )$') #+++
plt.ylabel(r'$\mathrm{ln}\left(\frac{U_\mathrm{aus}/\mathrm{Gain}}{\mathrm{V}}\right) \, $') #+++
plt.xlim(-2.5, 0) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(-5, 1) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.legend(loc="best")

##Plot als Grafik speichern
plt.savefig('../plots/4.pdf') #+++ Dateiname und Typ anpassen

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy.optimize import curve_fit

phi, Am, Gain = np.genfromtxt("../messwerte/2.2.txt", unpack = True)
Amp = Am/Gain
print(Amp)
thet = np.arange(361)
print(np.radians(thet))
table = np.array([phi, Am, Gain, Amp])
np.savetxt('../tabellen/3.txt', table.T, delimiter=' & ' , newline='\\\\ ' , header="", comments='%', fmt='%1.6g')
phase = phi
#U0=np.array(-(2/const.pi *6.61 * np.cos(thet)))
#print(U01)

#plt.plot(np.radians(thet)/const.pi, f( thet + 75), 'b-', label='Fit')

##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
	return -(2/const.pi)*a*6.61*2.1*np.cos(np.radians(x) + b)


def f(x, a): #+++ Namen der Vorfaktoren, nicht die der Messwerte
	    return -(2/const.pi)*6.6*2.1*np.cos(x+ a)

params, covariance = curve_fit(f, np.radians(phase), Amp) #+++ Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
print(params*180/np.pi)
print(errors*180/np.pi)
##Plot gefittetes Polynom
x_plot =np.linspace(np.amin(np.radians(phase-90), ), np.amax(np.radians(phase+90), ), 1000) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot/const.pi, f(x_plot, *params), 'b-', label='nichtlinearer Fit') #+++ Label anpassen



#plt.plot(thet*50, U0, 'b-', label='ideal')
plt.plot(np.radians(phi)/const.pi, Amp, 'kx', label='Messwerte')

plt.legend(loc='best')
plt.xlabel('Phase [rad]')
plt.ylabel('U[V]')
plt.xlim(-0.2, 2.2)
plt.ylim(np.amin(Amp)-1, np.amax(Amp)+1)
#plt.show()
plt.savefig('../plots/22neu.pdf')

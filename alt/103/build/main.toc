\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Das Hook'sche Gesetz}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Biegung bei einseitiger Einspannung}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Biegung bei beidseitiger Einspannung}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchführung}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Messprogramm}{5}{subsection.2.1}
\contentsline {section}{\numberline {3}Auswertung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Fehler und Regression}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Eckiger Aluminiumstab mit losem Ende}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Runder Messingstab mit losem Ende}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Runder Messingstab mit zwei festen Enden}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}Diskussion}{13}{section.4}
\contentsline {section}{\nonumberline Literatur}{14}{section*.14}

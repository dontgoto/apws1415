# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat
###Konstanten 
alul=46.4 / 100
messingl= 48.9 / 100
G = 500.4 / 1000
F= 9.81 * G
print("L")
print(alul)
print("G")
print(G)
print("F")
print(F)
##Messwerte aus Datei einlesen
alu1, alu2, messing = np.genfromtxt('../messwerte/dicke.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
aluxx, aa, bb  = np.genfromtxt('../messwerte/alu1.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!


#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
alux=aluxx/100
a=aa/1000
b=bb/1000
alub=np.mean(alu1/1000)
aluberr=np.std(alu1/1000)
aluh=np.mean(alu2/1000)
aluherr=np.std(alu2/1000)
D=np.absolute(a-b)
g= alul * alux**2 - alux**3/3
I=alub * aluh**3   #* (1/12)

##########ausgabe der ergebnisse
print("x")
print(np.sort(alux))
print("breite")
print(alub)
print(aluberr)
print("hoehe")
print(aluh)
print(aluherr)
print("D(x)")
print(np.sort(D))
print("g(x)")
print(np.sort(g))
print("I")
print(I)
##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x   #+++
params, covariance = curve_fit(f, g, D) #+++ Namen der Variablen der Messwerte eintragen
error = np.sqrt(np.diag(covariance))
unp.uarray.paramswitherror = (params, error)
np.savetxt('../plots/parameter/polynomfaktoralu_lose.txt', unp.uarray.paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(0.00), np.amax(0.07)) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen
E=F/(2*params[1]*I*1/12)
print("E")
print(E)
FI= np.sqrt((1/12*aluh**3 * aluberr)**2+(1/4 * alub * aluh**2 * aluherr)**2)
print("FI")
print(FI)
FE=np.sqrt((-(F)/(2 * I * params[1]**2)*error[1])**2)
print("FE")
print(FE)
#Messwerte plotten
plt.plot(g, D, 'kx', label='Messwerte', markersize='7') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel( r'$g_{(x)}[m^3]$') #+++
plt.ylabel( r'$D_{(x)}[m]$') #+++
plt.grid(True) #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/alu_lose.pdf') #+++ Dateiname und Typ anpassen



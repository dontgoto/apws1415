# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat
###Konstanten 
alul=46.4 / 100
messingl= 48.9 / 100
G = 4680.3 / 1000
F= 9.81 * G
print("G")
print(G)
print("F")
print(F)
##Messwerte aus Datei einlesen
alu1, alu2, messing = np.genfromtxt('../messwerte/dicke.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
messingxx1, aa1, bb1, messingxx2, aa2, bb2  = np.genfromtxt('../messwerte/messing2.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
messingx=messingxx2/100
a=aa2/1000
b=bb2/1000
messingr=np.mean(messing/1000)
messingrerr=np.std(messing/1000)
D=np.absolute(a-b)
g= 3 *messingl * messingx - 4 * messingx**3
I=const.pi/4  * messingr**4
##########ausgabe der ergebnisse#
print("x")
print(np.sort(messingx))
print("rad")
print(messingr)
print(messingrerr)
print("D(x)")
print(np.sort(D))
print("n(x)")
print(np.sort(g))
print("I")
print(I)
##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x   #+++
params, covariance = curve_fit(f, g, D) #+++ Namen der Variablen der Messwerte eintragen
error = np.sqrt(np.diag(covariance))
unp.uarray.paramswitherror = (params, error)
np.savetxt('../plots/parameter/polynomfaktormessing_fest2.txt', unp.uarray.paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
E=F/(48*params[1]*I)
print("E")
print(E)
FI=np.sqrt((const.pi * messingr**3 *messingrerr)**2)
print("FI")
print(FI)
FE=np.sqrt((-(F)/(248* I * params[1]**2)*error[1])**2+(-F/(48*I**2*params[1])* FI)**2)
print("FE")
print(FE)


##Plot gefittetes Polynom
x_plot = np.linspace(0 , 0.5) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen


#Messwerte plotten
plt.plot(g, D, 'kx', label='Messwerte', markersize='7') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel( r'$n_{(x)}[m^3]$') #+++
plt.ylabel( r'$D_{(x)}[m]$') #+++
plt.grid(True) #+++
plt.xlim(0,0.35 ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(0 ,0.0025 ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/messing_fest2.pdf') #+++ Dateiname und Typ anpassen

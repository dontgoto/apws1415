\section{Theorie}
\label{sec:Theorie}
Beugung beschreibt die Ablenkung von Licht wenn es durch Öffnungen hindurchtritt oder auf Hindernisse mit Abmessungen kleiner dem Strahldurchmesser trifft.
Das Huygensche Prinzip beschreibt die Tatsache, dass jeder Punkt der Wellenfront als Ausgangspunkt einer neuen Elementarwelle betrachtet werden kann.
Die Lage der gesamten Wellenfront ergibt sich aus der Interferenz dieser einzelnen Elementarwellen.

\subsection{Beugung am Einzelspalt}
\label{sub:beugung_am_einzelspalt}
Um einen mathematischen Zusammenhang zwischen Spaltgeometrie und Intensitätsverteilung des Beugungsmusters herzuleiten gibt es zwei Näherungen, die Frauenhofersche und die Fresnelsche.
Beide Beugungsnäherungen sind in Abbildung~\ref{beugungen} dargestellt.
\begin{figure}
  \centering
  \includegraphics[width=0.80\textwidth]{abbildungen/beugungen.png}
  \caption{Darstellung der Fresnelschen und Frauenhoferschen Beugung am Einzelspalt.\cite{anleitung}}
  \label{beugungen}
\end{figure}

\noindent Im folgenden wird die Frauenhofersche Näherung verwendet.
Dabei wird angenommen, dass die Spaltbreite klein gegenüber der Länge des Spalts ist und die Lichtquelle im unendlichen liegt, also alle Strahlen parallel auf den Spalt treffen und eine ebene Wellenfront bilden.
Die Feldstärke der Welle ergibt sich zu:
\begin{equation*}
    A_{z,t} = A_0 e^{i(\omega t - 2\pi z )/\lambda}
\end{equation*}

\noindent Nach der Beugung lässt sich die Amplitude $B$ in Abhängigkeit von $\varphi$ durch Interferenz der einzelnen Teilstrahlen, wie sie in Abbildung~\ref{richtung} dargestellt sind, berechnen.
\begin{figure}
  \centering
  \includegraphics[width=0.80\textwidth]{abbildungen/richtung.png}
  \caption{Phasenbeziehung zweier Teilstrahlen bei Frauenhoferscher Näherung.\cite{anleitung}}
  \label{richtung}
\end{figure}
Die Phasendifferenz zweier Strahlen ergibt sich zu:
\begin{equation*}
\delta = \frac{2 \pi x \sin \varphi }{\lambda}
\end{equation*}
Für die Amplitude ergibt sich so:
\begin{equation*}
    B(z,t,\varphi ) = A_0 e^{i\left( \omega t -\frac{2\pi z}{\lambda} \right) } e^{\frac{i \pi \sin \varphi }{\lambda} } \frac{\lambda}{\pi \sin \varphi} \sin \left( \frac{\pi b \sin \varphi }{\lambda } \right)
\end{equation*}
Da nur die über die Zeit gemittelten Intensitätswerte gemessen werden können ergibt sich:
\begin{equation}
    B(\varphi) = A_0 \frac{\lambda}{\pi b \sin \varphi } \sin \left( \frac{\pi b \sin \varphi }{\lambda} \right) \propto \sqrt{I(\varphi ) } \, .
\end{equation}
Nullstellen finden sich bei $\varphi _n = \pm n \frac{\lambda}{b}$.


\subsection{Beugung am Doppelspalt}
\label{sub:beugung_am_doppelspalt}
Die Intensität des Doppelspalts ergibt sich, wie in Abbildung~\ref{doppelspalt} dargestellt, aus der Intensität zweier überlagerter Einzelspalte.
\begin{figure}
  \centering
  \includegraphics[width=0.80\textwidth]{abbildungen/doppelspalt.png}
  \caption{Schematische Darstellung eines Doppelspalts.\cite{anleitung}}
  \label{doppelspalt}
\end{figure}

So ergibt sich:
\begin{equation}
    I (\varphi) \propto B(\varphi)^2 = 4 \cos ^2 \left(\frac{ \pi s \sin \varphi }{\lambda } \right) \left( \frac{\lambda}{\pi b \sin \varphi } \right) ^2 \sin ^2 \left( \frac{\pi b \sin \varphi }{\lambda}\right)
\end{equation}

\noindent Zusätzlich zu den Nullstellen der Einzelspalte finden sich noch Minima bei:
\begin{equation*}
    \varphi (k) = \mathrm{arcsin} \left( \frac{2k +1 }{2s}\right) \lambda
\end{equation*}

\noindent Eine solche Amplitudenverteilung ist in Abbildung~\ref{amplitude} dargestellt.
\begin{figure}
  \centering
  \includegraphics[width=0.80\textwidth]{abbildungen/amplitude.png}
  \caption{Amplitudenverteilung für einen Doppelspalt.\cite{anleitung}}
  \label{amplitude}
\end{figure}

\subsection{Fouriertransformation der Amplitudenverteilung}
\label{sub:fouriertransformation_der_amplitudenverteilung}
Durch Fouriertransformation der Blendenfunktion ergibt sich die Amplitudenfunktion und umgekehrt.
Diese ist definiert als:
\begin{equation*}
    g (\xi ) = \int _{-\infty } ^\infty f(x) e^{ix\xi}\mathrm{d} \xi
\end{equation*}
Die Blendenfunktion für einen unendlich langen Spalt ist:
\begin{equation*}
    f(x) \mapsto \begin{cases}
        A_0 , & 0 \leq x \leq b \\
        0 , & \text{sonst}
    \end{cases}
\end{equation*}

\noindent Daraus folgt:
\begin{equation*}
    g(\xi) = \frac{2 A_0}{\xi} e^{\frac{i\xi b }{2}} \sin \left( \frac{\xi b }{2} \right)
\end{equation*}

\noindent Umgekehrt kann auch durch Fouriertransformation der Amplitudenfunktion die Blendenfunktion berechnet werden.

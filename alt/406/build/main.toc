\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Beugung am Einzelspalt}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Beugung am Doppelspalt}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Fouriertransformation der Amplitudenverteilung}{5}{subsection.1.3}
\contentsline {section}{\numberline {2}Aufbau und Durchführung}{6}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Beugung am Einzelspalt}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Beugung am Doppelspalt}{9}{subsection.3.2}
\contentsline {section}{\numberline {4}Diskussion}{11}{section.4}
\contentsline {section}{\nonumberline Literatur}{11}{section*.13}

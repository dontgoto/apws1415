# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from scipy.optimize import leastsq
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
mm_thin, muA_thin = np.genfromtxt('../messwerte/thin.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
mm_thick, muA_thick = np.genfromtxt('../messwerte/thick.txt', skip_header=1, unpack=True)
mm_doubble, muA_doubble = np.genfromtxt('../messwerte/doubble.txt', skip_header=1, unpack=True)
#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
l = 0.000633
I_0 = 400* 10**(-12)
muI_0=400 *10**(-6)
L = 10**(3)
A_thin = muA_thin*10**(-6)
A_thick = muA_thick*10**(-6)
A_doubble = muA_doubble*10**(-6)
print("Dicke messwerte:")
for i in range(0, 22):
    print("        ",mm_thick[i]," & ",muA_thick[i]," & ",mm_thick[i+((mm_thick.size-1)/2)]," & ",muA_thick[i+((mm_thick.size-1)/2)],"  \\\\")
print("Duenne Messwerte:",mm_thick.size)
for i in range(0,25):
    print("        ",mm_thin[i]," & ",muA_thin[i]," & ",mm_thin[i+((mm_thin.size)/2)]," & ",muA_thin[i+((mm_thin.size)/2)],"  \\\\")
print("Doppel messwerte:",mm_doubble.size)
for i in range(0,25):
    print("        ",mm_doubble[i]," & ",muA_doubble[i]," & ",mm_doubble[i+((mm_doubble.size)/2)]," & ",muA_doubble[i+((mm_doubble.size)/2)],"  \\\\")
muA_thick = muA_thick - muI_0
muA_thick = muA_thick - muI_0
muA_doubble = muA_doubble - muI_0
##Polynom fitten
thick = 1
thin  = 1
doubble = 1
if(thick):
    plt.figure()
    print("Thick max:", max(muA_thick))
    muA_thick /= max(muA_thick)
    muA_thick = np.absolute(muA_thick)
    print("muA_thck:",muA_thick)
    print("mm_thick;",mm_thick)
    p0 = np.array([0.41 , 1])
    def f (t, b, a):
        return (np.sin(np.pi * b * (t / 1000) / l) / ((np.pi * b * (t / 1000)) / l)) ** 2
    params, covariance = curve_fit(f, mm_thick, muA_thick, p0 ) # Namen der Variablen der Messwerte eintragen
    #errors = np.sqrt(np.diag(covariance))
    #uparam1= ufloat(params[0], errors[0])
    #uparam2= ufloat(params[1], errors[1])
    #uparam3= ufloat(params1[2], errors[2])
    #uparam4= ufloat(params1[3], errors[3])
    #print("thick b: ",'{:L}'.format(uparam1))
    #print("thick l: ",'{:L}'.format(uparam2))
    #print('{:L}'.format(uparam3))
    #print('{:L}'.format(uparam4))
    print("p:",params)
    #unp.uarray.paramswitherror = (params, errors)
    #np.savetxt('../plots/parameter/polynomfaktor_thick.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
    #plsq = leastsq(f, p0, args=(muA_thick, mm_thick))
    #print("plsq: ",plsq)
    ##Plot gefittetes Polynom
    x_plot = np.linspace(np.amin(mm_thick)-1, np.amax(mm_thick)+1 ,1000000) # den Fit möglichst nur da plotten wo auch Messwerte sind
    plt.plot(x_plot, f(x_plot, *params), 'r-', label='Nichtlinearer Fit') #+++ Label anpassen

#Messwerte plotten
    plt.plot(mm_thick, muA_thick, 'kx', label='Messwerte')


##Achsenbeschriftungen, kosmetisches
    plt.legend(loc="best")
    plt.xlabel(r"$\varphi / 10^{-3} \mathrm{rad}$")
    plt.ylabel(r"$I / 18\,\mathrm{\mu A}$")
    #plt.grid(True) #+++
    #plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
    #plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt

    plt.savefig('../plots/thick.pdf') #+++ Dateiname und Typ anpassen
    plt.show()
    #plt.close()

if(thin):
    plt.figure()
    print("max thin:", max(muA_thin))
    muA_thin /= max(muA_thin)
    print("muA_thin:",muA_thin)
    print("mm_thin;",mm_thin)
    def f (t, b):
        return(np.sin(np.pi * b * (t / 1000) / l) / ((np.pi * b * (t / 1000)) / l)) ** 2
    params, covariance = curve_fit(f, mm_thin, muA_thin, p0 = np.array([0.16])) # Namen der Variablen der Messwerte eintragen
    #errors = np.sqrt(np.diag(covariance))
    #uparam1= ufloat(params[0], errors[0])
    #uparam2= ufloat(params[1], errors[1])
    #uparam3= ufloat(params1[2], errors[2])
    #uparam4= ufloat(params1[3], errors[3])
    #print("thick b: ",'{:L}'.format(uparam1))
    #print("thick l: ",'{:L}'.format(uparam2))
    #print('{:L}'.format(uparam3))
    #print('{:L}'.format(uparam4))
    print("p:",params)
    #unp.uarray.paramswitherror = (params, errors)
    #np.savetxt('../plots/parameter/polynomfaktor_thick.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


    ##Plot gefittetes Polynom
    x_plot = np.linspace(np.amin(mm_thin), np.amax(mm_thin),1000000) # den Fit möglichst nur da plotten wo auch Messwerte sind
    plt.plot(x_plot, f(x_plot, *params), 'r-', label='Nichtlinearer Fit') #+++ Label anpassen

#Messwerte plotten
    plt.plot(mm_thin, muA_thin, 'kx', label='Messwerte')


##Achsenbeschriftungen, kosmetisches
    plt.legend(loc="best")
    plt.xlabel(r"$\varphi / 10^{-3} \mathrm{rad}$") #+++
    plt.ylabel(r"$I / 2\,\mathrm{\mu A}$") #+++
    #plt.grid(True) #+++
    #plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
    #plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt

    plt.savefig('../plots/thin.pdf') #+++ Dateiname und Typ anpassen
    plt.show()
    #plt.close()

if(doubble):
    plt.figure()
    print("max doubble:", max(muA_doubble))
    muA_doubble /= max(muA_doubble)
    print("muA_doubble:",muA_doubble)
    print("mm_doubble;",mm_doubble)
    def f (t, b, s):
        return np.cos(np.pi * s * (t / 1000) / l) ** 2 * (np.sin(np.pi * b * (t / 1000) / l) / ((np.pi * b * (t / 1000)) / l)) ** 2
    def g (t, b):
        return (np.sin(np.pi * b * (t / 1000) / l) / ((np.pi * b * (t / 1000)) / l)) ** 2
    params, covariance = curve_fit(f, mm_doubble, muA_doubble, p0 = np.array([0.14, 1.1])) # Namen der Variablen der Messwerte eintragen
    #errors = np.sqrt(np.diag(covariance))
    #uparam1= ufloat(params[0], errors[0])
    #uparam2= ufloat(params[1], errors[1])
    #uparam3= ufloat(params1[2], errors[2])
    #uparam4= ufloat(params1[3], errors[3])
    #print("thick b: ",'{:L}'.format(uparam1))
    #print("thick l: ",'{:L}'.format(uparam2))
    #print('{:L}'.format(uparam3))
    #print('{:L}'.format(uparam4))
    print("p:",params)
    #unp.uarray.paramswitherror = (params, errors)
    #np.savetxt('../plots/parameter/polynomfaktor_thick.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


    ##Plot gefittetes Polynom
    x_plot = np.linspace(np.amin(mm_doubble), np.amax(mm_doubble),1000000) # den Fit möglichst nur da plotten wo auch Messwerte sind
    plt.plot(x_plot, f(x_plot, *params), 'r-', label='Nichtlinearer Fit') #+++ Label anpassen
    plt.plot(x_plot, g(x_plot, 0.14), 'b-', label='Einzelspalt Fit')
#Messwerte plotten
    plt.plot(mm_doubble, muA_doubble, 'kx', label='Messwerte')


##Achsenbeschriftungen, kosmetisches
    plt.legend(loc="best")
    plt.xlabel(r"$\varphi / 10^{-3} \mathrm{rad}$") #+++
    plt.ylabel(r"$I / 3.8\,\mathrm{\mu A}$") #+++
    #plt.grid(True) #+++
    #plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
    #plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt

    plt.savefig('../plots/doubble.pdf') #+++ Dateiname und Typ anpassen
    plt.show()
    #plt.close()
print("Lineal Messung:")
mm = 17.5
mikdick = 7
mikduenn = 2.5
doppelsp = 4
doppelab = 19
print("dick: ", mikdick/mm)
print("Duenn: ", mikduenn/mm)
print("Doppelsp: ", doppelsp/mm)
print("Doppelab: ", doppelab/mm)

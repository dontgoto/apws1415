import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as const
import sympy
from uncertainties import ufloat
from scipy.optimize import curve_fit
from scipy.stats import sem

#############################
### Impuls-Echo-Verfahren ###
#############################

mrk, mrm, mrg, mbk, mbm, mbg = np.genfromtxt('impuls.txt', unpack=True)
rk = mrk*0.000001
rm = mrm*0.000001
rg = mrg*0.000001
bk = mbk*0.000001
bm = mbm*0.000001
bg = mbg*0.000001

def geschw(a, b):
	return (2*a*(1/b))

hk, hm, hg = np.genfromtxt('abmessung_zyl.txt', unpack=True)

print ('rot, klein = ', geschw(hk, rk))
print ('rot, mittel = ', geschw(hm, rm))
print ('rot, gross = ', geschw(hg, rg))
print ('blau, klein = ', geschw(hk, bk))
print ('blau, mittel = ', geschw(hm, bm))
print ('blau, gross = ', geschw(hg, bg))

a = geschw(hk, rk)
b = geschw(hm, rm)
c = geschw(hg, rg)
d = geschw(hk, bk)
e = geschw(hm, bm)
f = geschw(hg, bg)

z=np.array([a,b,c,d,e,f])
mschall = ufloat(np.mean(z), np.std(z))
print ('mittelwert der schallgeschwindigkeit =', mschall)

def s(a):
	return ((1/2)*mschall*a)

ds1 = abs(s(rk)-s(bk))
ds2 = abs(s(rm)-s(bm))
ds3 = abs(s(rg)-s(bg))

print ('differenzen')

print ('klein', ds1)
print ('mittel', ds2)
print ('gross', ds3)

print ('geschwindigkeit nach korrektur')

ks1 = 2*(1/rk)*((s(rk)+s(bk)-ds1)/2)
ks2 = 2*(1/rm)*((s(rm)+s(bm)-ds2)/2)
ks3 = 2*(1/rg)*((s(rg)+s(bg)-ds3)/2)

print (ks1)
print (ks2)
print (ks3)

#################################
### Durchschallungs-Verfahren ###
#################################

mdk, mdm, mdg = np.genfromtxt('durchschall.txt', unpack=True)
dk = mdk*0.000001
dm = mdm*0.000001
dg = mdg*0.000001

print ('rot, klein, durchschall = ', geschw(hk, dk))
print ('rot, mittel, durchschall = ', geschw(hm, dm))
print ('rot, gross, durchschall = ', geschw(hg, dg))

##################
### Acrylblock ###
##################

a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11 = np.genfromtxt('abmessung_block2.txt', unpack=True)#abst
b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11 = np.genfromtxt('block.txt', unpack=True)#messewerte

mb1 = b1*0.000001
mb2 = b2*0.000001
mb3 = b3*0.000001
mb4 = b4*0.000001
mb5 = b5*0.000001
mb6 = b6*0.000001
mb7 = b7*0.000001
mb8 = b8*0.000001
mb9 = b9*0.000001
mb10 = b10*0.000001
mb11 = b11*0.000001

print ('berechnete strecke acrylblock')

print (s(mb1))
print (s(mb2))
print (s(mb3))
print (s(mb4))
print (s(mb5))
print (s(mb6))
print (s(mb7))
print (s(mb8))
print (s(mb9))
print (s(mb10))
print (s(mb11))

c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11 = np.genfromtxt('abmessung_block.txt', unpack=True)#durchmesser

u1 = 0.0795-(a1*0.01)-c1*(0.01)
u2 = 0.0795-(a2*0.01)-c2*(0.01)
u3 = 0.0795-(a3*0.01)-c3*(0.01)
u4 = 0.0795-(a4*0.01)-c4*(0.01)
u5 = 0.0795-(a5*0.01)-c5*(0.01)
u6 = 0.0795-(a6*0.01)-c6*(0.01)
u7 = 0.0795-(a7*0.01)-c7*(0.01)
u8 = 0.0795-(a8*0.01)-c8*(0.01)
u9 = 0.0795-(a9*0.01)-c9*(0.01)
u10 = 0.0795-(a10*0.01)-c10*(0.01)
u11 = 0.0795-(a11*0.01)-c11*(0.01)

print ('strecke Acrylblock von unten')

print (u1)
print (u2)
print (u3)
print (u4)
print (u5)
print (u6)
print (u7)
print (u8)
print (u9)
print (u10)
print (u11)

print ('durchmesser der leerstellen')

print (0.0795-s(mb1)+u1)
print (0.0795-s(mb2)+u2)
print (0.0795-s(mb3)+u3)
print (0.0795-s(mb4)+u4)
print (0.0795-s(mb5)+u5)
print (0.0795-s(mb6)+u6)
print (0.0795-s(mb7)+u7)
print (0.0795-s(mb8)+u8)
print (0.0795-s(mb9)+u9)
print (0.0795-s(mb10)+u10)
print (0.0795-s(mb11)+u11)

###################
### Augenmodell ###
###################

miris, mretina = np.genfromtxt('auge.txt', unpack=True)
iris = miris*0.000001
retina = mretina*0.000001

iris1 = (1/2)*1410*iris
retina1 = (1/2)*1410*retina

print ('s fuer auge')
print (iris1)
print (retina1)

#eof

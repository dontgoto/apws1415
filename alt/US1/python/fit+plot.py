# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
Zcm = np.genfromtxt('../Messwerte/ZylinderAbmessungen.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
Dcm, Ucm, Ocm = np.genfromtxt('../Messwerte/BohrungesAbstaende.txt', skip_header=1, unpack=True)
DSDIFFmm, DSAMPv = np.genfromtxt('../Messwerte/DurchSchallung.txt', skip_header=1, unpack=True)
REF1DIFFmm, REF1AMP1v, REF1AMP2v = np.genfromtxt('../Messwerte/1MHzRefl.txt', skip_header=1, unpack=True)
REF2DIFFmm, REF2AMP1v, REF2AMP2v = np.genfromtxt('../Messwerte/2MHzRefl.txt', skip_header=1, unpack=True) 
BMUmm, BMOmm = np.genfromtxt('../Messwerte/BohrungsMessung.txt', skip_header=1, unpack=True)
#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
REF1DIFFmm=REF1DIFFmm*0.000001
REF2DIFFmm=REF2DIFFmm*0.000001
Zcm=Zcm*0.01
print("Reflektion 1Mhz Diff in s:",REF1DIFFmm)
print("Reflektion 2Mhz Diff in s:",REF2DIFFmm)
#A1
def geschw(a, b):
	return (a*(1/b))
print("            1MHz             2MHz")
for i in range(0,3):
    print("        ",(geschw(Zcm[i], REF1DIFFmm[i]))," & ",(geschw(Zcm[i], REF2DIFFmm[i])),"  \\\\")

cAc=np.array([geschw(Zcm, REF1DIFFmm), geschw(Zcm, REF2DIFFmm)])
np.reshape(cAc, 6)
mschall=ufloat(np.mean(cAc),np.std(cAc))
print("mschlaa: ",'{:+.4uL}'.format(mschall))
def s(a):
	return ((1/2)*mschall*a)
Diff = np.absolute(s(REF1DIFFmm)-s(REF2DIFFmm))
print("        Zyl             Diff")
for i in range(0,3):
    print("        ",i+1," & ", '{:.4ueL}'.format(Diff[i]),"\\\\")
#ks1 = 2*(1/rk)*((s(rk)+s(bk)-ds1)/2)
def kschall(c1,c2,diff):
    return(2*(1/c1)*((s(c1)+s(c2))-diff)/2)
kc=kschall(REF2DIFFmm, REF1DIFFmm, Diff)
mschall=np.mean(kc)
print("mittlerer korrigiertel schall:",'{:+.4uL}'.format(mschall))
print("       Zyl        korrigierter schall")
for i in range(0,3):
    print("        ",i+1," & ", '{:.4uL}'.format(kc[i]),"\\\\")
#A2
DSDIFFmm=DSDIFFmm*0.000001
cDSchall=ufloat(np.mean(geschw(Zcm,DSDIFFmm)),np.std(geschw(Zcm,DSDIFFmm)))
print("cDschall: ", '{:.4uL}'.format(cDSchall))
#A3
mschall=ufloat(2730,1) 
####BMUmm, BMOmm ##### Dcm, Ucm, Ocm ######
BMUmm=BMUmm*0.000001
BMOmm=BMOmm*0.000001
Dcm=Dcm*0.01
Ucm=Ucm*0.01
Ocm=Ocm*0.01
length=ufloat(np.mean(Dcm+Ucm+Ocm),np.std(Dcm+Ucm+Ocm))
print("length:",length)
def d(messungO,messungU):
    return(length-s(messungO)-s(messungU))

print("+++++++TABELLE++++++++++")
print("#Ug, Us, Ud, Og, Os, Od, Dg, Ds, Dd##")
for i in range (0,11):
    #Ug, Us, Ud, Og, Os, Od, Dg, Ds, Dd##
    print("        ",i+1,Ocm[i]," & "," & ",Ucm[i]," & ",Dcm[i]," & ","\(",'{:.2ueL}'.format(d(BMOmm[i],BMUmm[i])),"\)"," & ","\(",'{:.2ueL}'.format(np.absolute(Dcm[i]-d(BMOmm[i],BMUmm[i]))),"\)","  \\\\")
#=,Ucm[i]," & ","\(",'{:.4ueL}'.format(s(BMOmm[i])),"\)"," & ","\(",'{:.4ueL}'.format(np.absolute(Ucm[i]-s(BMUmm[i]))),"\)"," & ",Ocm[i]," & ","\(",'{:.4ueL}'.format(s(BMOmm[i])),"\)"," & ","\(",'{:.4ueL}'.format(np.absolute(Ocm[i]-s(BMOmm[i]))),"\)"," & "
print("+++++++++++TABELLE+++++++++")
#print("Ugemessen: ",Ucm)
#print("Uberechnet: ",s(BMUmm))
print("UDiff: ", np.absolute(Ucm-s(BMUmm)))
#print("Ogemessen: ", Ocm)
#print("Oberrenchnet: ", s(BMOmm))
#print("DurchmesserGemessen: ", Dcm)
print("ODiff: ", np.absolute(Ocm-s(BMOmm)))
#print("DurchmesserBerrechnet", d(BMOmm,BMUmm))
#A4
def sA(c,ms):
    return(((1/2)*c*ms))
Auge = np.genfromtxt('../Messwerte/Auge.txt', skip_header=1, unpack=True)
Auge=Auge*0.000001
cL=2500
cGK=1410
iris=sA(cGK,Auge[0])
Linse1=sA(cGK,Auge[1])
Linse2=sA(cL,Auge[2]-Auge[1])+Linse1
Retina=sA(cGK,Auge[3]-Auge[2])+Linse1+Linse2
print("Iris",iris,",Linse1",Linse1,",Linse2",Linse2,",Retina",Retina)
#xValue1 =
#yValue1 =

##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++
#params, covariance = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#uparam1= ufloat(params1[0], errors[0])
#uparam2= ufloat(params1[1], errors[1])
#uparam3= ufloat(params1[2], errors[2])
#uparam4= ufloat(params1[3], errors[3])

#print('{:L}'.format(uparam1))
#print('{:L}'.format(uparam2))
#print('{:L}'.format(uparam3))
#print('{:L}'.format(uparam4))

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(xValue1), np.amax(xValue1)) # den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen

#vllt errorbar plot
#xerr1=
#yerr1=

#plt.errorbar(x, y, xerr=xerr1, yerr=yerr1)


##vllt 1sigma bereich darstellen
#plt.plot(x_plot, f(x_plot,*plus1-minus1)1000, 'r-', alpha=0)
#plt.plot(x_plot, f(x_plot,*plus1+minus1), 'r-', alpha=0)
#plt.fill_between(x_plot, f(x_plot,*plus1-minus1)*1000, f(x_plot,*plus1+minus1)*1000, facecolor='green', alpha=0.15)


#Messwerte plotten
#plt.plot(xValue1, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
#plt.legend(loc="best")
#plt.xlabel(r'$/\mathrm{}$') #+++
#plt.ylabel(r'$/\mathrm{}$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
#plt.savefig('../plots/xxx.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
#plt.close()

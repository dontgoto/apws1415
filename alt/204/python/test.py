import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
import uncertainties.unumpy as unp
from uncertainties import ufloat
import sympy

#+++ Raw Messwerte und Parameter weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen

deltaZeit =np.array([8.26, 17.4, 63.3]) #deltazeit ist der zeitabstand zwischen den maxima der nahen und fernen messstelle
#1=alu 2=mes 3=stahl
Anah = np.array([[2.0, 3.5, 5.1, 5.8, 6.5, 7.1, 7.9, 9.3, 9.6, 9.5], [3.4, 4.3, 5.6, 6.2, 7.0, 7.6, 8.2, 9.4, 9.6, 9.6], [33, 36, 38, 40, 41.5, 39, 42, 45, 46.5, 48]])
Afern =np.array([[0.7, 2.2, 3.8, 4.5, 5.2, 5.9, 6.5, 7.9, 8.4, 8.2],[1.9, 2.9, 4.1, 4.8, 6.5, 6.1, 6.8, 7.8, 8.2, 8.3],[31, 33, 35, 36.5, 38, 31, 33, 35, 37, 38]])
Afern[1] = Afern[1] / 0.54 #Umrechnung: auf 2,6cm kommen 5 Grad, 0.52=2.6/5
Anah[1] = Anah[1] / 0.54
print(Afern[1])
Afern[2] = Afern[2] / 0.48 #Umrechnung: auf 2,6cm kommen 5 Grad, 0.52=2.6/5
Anah[2] = Anah[2] / 0.48

deltaX = 0.03
rho =np.array( [ 2800, 8520, 8000 ] )
kapazi =np.array( [ 830, 385, 400 ] )

waermeleit = ( rho[0] * kapazi[2]*  deltaX**2 )/(2 * deltaZeit[2] * np.log(Anah[2] / Afern[2]))
print("stahl:", waermeleit[2])
print(waermeleit[2].mean())

#waermekapazitaetMes = ( rhoMes * cMes*deltaX**2 )/(2 * deltaZeitMes * np.log(AnahMes / AfernMes))
#print("Messing:", waermekapazitaetMes)
#print(waermekapazitaetMes.sum()/10)

#waermekapazitaetAlu = ( rhoAlu *cAlu* deltaX**2 )/(2 * deltaZeitAlu * np.log(AnahAlu / AfernAlu))
#print("Alu:", waermekapazitaetAlu)
#print(waermekapazitaetAlu.sum()/10)

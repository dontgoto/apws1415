#Template für einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
#plt.style.use('ggplot')
#Messwerte aus Datei einlesen
messnummer, zeit, t1, t2, t3, t4, t5, t6, t7, t8, test = np.genfromtxt('../messwerte/teststatisch.txt', unpack = True) #+++
# Zeit.0 (s) Temperatur 1 (°C) Temperatur 2 (°C) Temperatur 3 (°C) Temperatur 4 (°C) Temperatur 5 (°C) Temperatur 6 (°C) Temperatur 6 (°C) Temperatur 7 (°C) Temperatur 8 (°C) Daten per Tastatur
print("Messnummer:", messnummer)
print (plt.style.available)

#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
t1 = const.C2K(t1)
t2 = const.C2K(t2)
t3 = const.C2K(t3)
t4 = const.C2K(t4)
t5 = const.C2K(t5)
t6 = const.C2K(t6)
t7 = const.C2K(t7)
t8 = const.C2K(t8)
tdiff21 = t2 -t1
tdiff78 = t7 - t8
tdiff21 = np.array(tdiff21)
print(tdiff21.std())

##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++
#params, covariance = curve_fit(f, a, b) #+++ Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#unp.uarray.paramswitherror = (params, error)
#np.savetxt('polynomfaktor.txt', paramswitherror , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung


#Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel('Zeit [s]') #+++
plt.ylabel('Temperaturdifferenz [K]') #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.grid(True) #+++
#set_yscale("log") #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX


#Messwerte plotten
#plt.plot(zeit, t1, 'k.', label='Messing breit T1') #+++
#plt.plot(zeit, t1, 'k.', label='Messing breit T1') #+++
#plt.plot(zeit, t2, 'k.', label='Messing breit T2') #+++
#plt.plot(zeit, t3, 'k.', label='Messing dünn T3') #+++
#plt.plot(zeit, t4, 'k.', label='Messing dünn T4') #+++
#plt.plot(zeit, t5, 'k.', label='Aluminium T5') #+++
#plt.plot(zeit, t6, 'k.', label='Aluminium T6') #+++
#plt.plot(zeit, t7, 'k.', label='Edelstahl T7') #+++
#plt.plot(zeit, t8, 'k.', label='Edelstahl T8') #+++
plt.plot(zeit[::25], tdiff21[::25], 'k.', label="T2 - T1", markersize=3 ) #+++
#plt.plot(zeit[::25], tdiff78[::25], 'k.', label="Messing breit T7 - T8", markersize=3) #+++
plt.savefig('../plots/tdiff78.pdf') #+++ Dateiname und Typ anpassen
#noch separat plotten

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften

##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen



#Wert bei 700 Sekunden von T1 T4 T5 T8
#700.00 35.98 34.79 37.47 35.90   ????31.17
#1= Messing dick 2=messing dünn 3=alu 4=stahl

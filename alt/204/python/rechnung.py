import numpy as np
import scipy.constants as const
import uncertainties.unumpy as unp
from uncertainties import ufloat
import uncertainties.unumpy as unp
import sympy
from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
from sympy import log, sin, cos, tan, Wild, Mul, Add
#+++ Raw Messwerte und Parameter weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen

#deltaZeitStahl = 63.3 #deltazeit ist der zeitabstand zwischen den maxima der nahen und fernen messstelle
deltaZeitStahl = np.array([1.15, 1.15, 1.15, 1.16, 1.18, 1.1, 1.1, 1.1, 1.0, 0.95, 1.1])
errZeitStahl = np.std(deltaZeitStahl)
deltaZeitStahl = unp.uarray(deltaZeitStahl, errZeitStahl)
deltaZeitStahl = (deltaZeitStahl/3)*200 #3cm entsprechen 200 sekunden
print("Periodenzeit für Stahl:", deltaZeitStahl.mean())
print(deltaZeitStahl)

#deltaZeitMes = 17.4
deltaZeitMes = np.array([0.35, 0.32, 0.4, 0.4, 0.4, 0.36, 0.38, 0.34, 0.35, 0.38])
errZeitMes = np.std(deltaZeitMes)
deltaZeitMes = unp.uarray(deltaZeitMes, errZeitMes)
deltaZeitMes = (deltaZeitMes/2.3)*100 #2.3cm entsprechen 100 sekunden
print("Periodenzeit für Mes:", deltaZeitMes.mean())
print(deltaZeitMes)

#deltaZeitAlu = 8.26
deltaZeitAlu = np.array([0.22, 0.19, 0.2, 0.2, 0.18, 0.2, 0.19, 0.18, 0.19, 0.2])
errZeitAlu = np.std(deltaZeitAlu)
deltaZeitAlu = unp.uarray(deltaZeitAlu, errZeitAlu)
deltaZeitAlu = (deltaZeitAlu/2.3)*100 #2.3cm entsprechen 100 sekunden
print("Periodenzeit für Alu:", deltaZeitAlu.mean())
print(deltaZeitAlu)

AnahAlu= np.array([2.0, 3.5, 5.1, 5.8, 6.5, 7.1, 7.9, 9.3, 9.6, 9.5]) # Angaben in cm Abstand von der 30 Grad linie
AfernAlu =np.array([0.7, 2.2, 3.8, 4.5, 5.2, 5.9, 6.5, 7.9, 8.4, 8.2])
AfernAlu = ( AfernAlu /0.54 )+30 #Umrechnung: auf 2,6cm kommen 5 Grad, 0.52=2.6/5
AfernAlu = unp.uarray(AfernAlu, [0.5])
AnahAlu = ( AnahAlu /0.54 )+30
#AfernAlu = const.C2K(AfernAlu)
#AnahAlu = const.C2K(AnahAlu)
AnahAlu = unp.uarray(AnahAlu, [0.5])
print("AnahAlu:", AnahAlu)
print("AfernAlu:", AfernAlu)

AnahMes = np.array([3.4, 4.3, 5.6, 6.2, 7.0, 7.6, 8.2, 9.4, 9.6, 9.6])
AfernMes = np.array([1.9, 2.9, 4.1, 4.8, 6.5, 6.1, 6.8, 7.8, 8.2, 8.3])
AfernMes = ( AfernMes /0.48 )+25
AfernMes = unp.uarray(AfernMes, [0.5])
AnahMes = ( AnahMes / 0.48 )+25
AnahMes = unp.uarray(AnahMes, [0.5])
print("AnahMes:", AnahMes)
print("AfernMes:", AfernMes)

AfernStahl = np.array([31, 33, 35, 36.5, 38, 31, 33, 35, 37, 38, 39]) #(Angaben in Grad)
AfernStahl = unp.uarray(AfernStahl, [0.5])
AnahStahl = np.array([33, 36, 38, 40, 41.5, 39, 42, 45, 46.5, 48, 49])
AnahStahl = unp.uarray(AnahStahl, [0.5])

deltaX = ufloat(0.03, 0.000)
rhoAlu =2800
rhoMes = 8520
rhoStahl = 8000
cAlu = ufloat(830, 10)
cMes = ufloat(385,10)
cStahl =ufloat(400, 100)

print("Wärmekapazitäten ")
waermekapazitaetStahl = ( rhoStahl * cStahl * deltaX**2 )/(2 * deltaZeitStahl * unp.log(AnahStahl / AfernStahl))
print("stahl:", waermekapazitaetStahl)
print(np.mean(noms(waermekapazitaetStahl)))
print("+-", np.mean(stds(waermekapazitaetStahl)))

waermekapazitaetMes = ( rhoMes * cMes * deltaX**2 )/(2 * deltaZeitMes * unp.log(AnahMes / AfernMes))
print("Messing:", waermekapazitaetMes)
print(np.mean(noms(waermekapazitaetMes)))
print("+-", np.mean(stds(waermekapazitaetMes)))

waermekapazitaetAlu = ( rhoAlu * cAlu * deltaX**2 )/(2 * deltaZeitAlu * unp.log(AnahAlu / AfernAlu))
print("Alu:", waermekapazitaetAlu)
print(np.mean(noms(waermekapazitaetAlu)))
print("+-", np.mean(stds(waermekapazitaetAlu)), "\n")

vAlu = 0.03 / deltaZeitAlu
vMes = 0.03 / deltaZeitMes
vStahl = 0.03 / deltaZeitStahl
print("vAlu:", vAlu.mean())
print("vMes:", vMes.mean())
print("vStahl:", vStahl.mean(), "\n")

fAlu = (vAlu**2 * rhoAlu* cAlu)/(const.pi*  waermekapazitaetAlu)
fStahl = (vStahl**2 * rhoStahl* cStahl)/(const.pi*  waermekapazitaetStahl)
fMes = (vMes**2 * rhoMes* cMes)/(const.pi*  waermekapazitaetMes)
print("fAlu:", fAlu.mean())
print("fMes:", fMes.mean())
print("fStahl:", fStahl.mean(), "\n")

lambdaAlu = fAlu/vAlu
lambdaMes = fMes/vMes
lambdaStahl = fStahl/vStahl
print("lambdaAlu:", lambdaAlu.mean())
print("lambdaStahl:", lambdaStahl.mean())
print("lambdaMes:", lambdaMes.mean())



#def error(f, err_vars=None):
    #from sympy import Symbol, latex
    #s = 0
    #latex_names = dict()

    #if err_vars == None:
        #err_vars = f.free_symbols

    #for v in err_vars:
        #err = Symbol('latex_std_' + v.name)
        #s += f.diff(v)**3 * err**2
        #latex_names[err] = '\\sigma_{' + latex(v) + '}'

    #return latex(sympy.sqrt(s), symbol_names=latex_names)

#rho, c, x, t, Tnah, Tfern = sympy.var('rho c x t Tnah Tfern')

#f = ( 1 )/(2 * t * log(Tnah ))

#print(error(f))

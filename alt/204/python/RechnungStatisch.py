import numpy as np
import scipy.constants as const
from uncertainties import ufloat 
t   =(100, 200, 300, 400, 500)#Zeit,[s]
T1C =(28.45, 31.28, 32.9, 33.96, 34.78)#Messing,Breit
T4C =(28.3, 30.8, 32.13, 32.29, 33.68)#Messing,Schmal
T5C =(30.68, 33.5, 34.79, 35.63, 36.36)#Aluminium,Breit
T8C =(25.79, 27.19, 28.3, 29.17, 29.91)#Stahl,Breit
Ad  = 0.0007 * 0.0004 #Querschnittsflaeche,Duenn
Ab  = 0.0012 * 0.0004 #Querschnitsflaaeche,Breit
KM  = 120 #Waermeleifaehigkeit,Messing {Quelle Wikipedia}
KA  = 236 #Aluminium
KS  = 48  #Stahl
dx  = 0.03#Abstand,Temp.Sensoren[m]

T1K = const.C2K(T1C)
T4K = const.C2K(T4C)
T5K = const.C2K(T5C)
T8K = const.C2K(T8C)

print("dQ/dt von T1:")
T1dQdt = 0
for T1Ki in T1K:
    a = -KM * Ab * (T1K[4]-T1Ki)/dx
    print(a)
    np.append(T1dQdt, a)
print("Done\n")

T4dQdt = 0
print("dQ/dt von T4:")
for T4Ki in T4K:
    d = -KM * Ad * (T4K[4]-T4Ki)/dx
    print(d)
    np.append(T4dQdt, d)
print("Done\n")

T5dQdt = 0
print("dQ/dt von T5:")
for T5Ki in T5K:
    b = -KA * Ab * (T5K[4]-T5Ki)/dx
    print(b)
    np.append(T1dQdt, b)
print("Done\n")

print("dQ/dt von T8:")
T8KdQdt = 0
for T8Ki in T8K:
    c = -KS * Ab * (T8K[4]-T8Ki)/dx
    print(c)
    np.append(T8KdQdt, c)
print("Done\n")

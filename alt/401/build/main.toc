\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Licht als Elektromagnetische Welle}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Punktförmige Lichtquellen}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Ausgedehnte Lichtquelle}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Das Michelson-Interferometer}{4}{subsection.1.4}
\contentsline {section}{\numberline {2}Durchführung}{6}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Wellenlängenbestimmung des He-Ne-Lasers}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Bestimmung der Brechungsindexe}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Diskussion}{9}{section.4}
\contentsline {section}{\nonumberline Literatur}{9}{section*.9}

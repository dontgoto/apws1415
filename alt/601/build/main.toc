\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Einflüsse auf die Gestalt der Franck-Hertz-Kurve}{4}{subsection.1.1}
\contentsline {section}{\numberline {2}Aufbau und Durchführung}{5}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Bestimmung der Energieverteilung der Elektronen}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Franck-Hertz-Kurve}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Ionisierungsspannung}{10}{subsection.3.3}
\contentsline {section}{\numberline {4}Diskussion}{10}{section.4}
\contentsline {section}{\nonumberline Literatur}{11}{section*.10}

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

Skala =np.array([2.35,2.55,2.55,2.45,2.55,2.55,2.4,2.4,2.5])
Uskala = ufloat(np.mean(Skala),np.std(Skala))
Uskala = Uskala/5
print('Uskala:')
print('{:L}'.format(Uskala))
peak = np.array([2.5,2.45,2.5,2.55,2.55,2.6,2.8])
Upeak = ufloat(np.mean(peak),np.std(peak))
VPeak = peak / Uskala
Vpeak = Upeak / Uskala
print('Vpeak:')
print('{:L}'.format(Vpeak))
udiff1 = 5.5
Vdiff = udiff1 / Uskala
print('Vdiff')
print('{:L}'.format(Vdiff))
VK = Vdiff - Vpeak
print('VK:')
print('{:L}'.format(VK))
print('tabelle [Upeak], [Vpeak]')
for i in range(0,7):
    print('       ', peak[i], ' & ', '\(', '{:L}'.format(VPeak[i]), '\)', '\\\\')



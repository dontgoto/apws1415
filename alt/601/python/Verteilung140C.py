import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

Skala =np.array([4.8,5.05,4.85,4.85,4.85])
Uskala = ufloat(np.mean(Skala),np.std(Skala))
Uskala = Uskala/2
print('Uskala: ')
print('{:L}'.format(Uskala))
YPos = np.array([1.2,1.2,1.2,1.2,1.2,1.0,1.0,0.6,0.4,0.1,0.0,0.0,0.1,0.0,0.1,0.0,0.1,0.0,0.1,0.1,0.0,0.0])#y-werte in 1 cm schritten
XPos = np.array([1.4,2.4,3.4,4.4,5.4,6.4,7.4,8.4,9.4,10.4,11.4,12.4,13.4,14.4,15.4,16.4,17.4,18.4,19.4,20.4,21.4,22.4])
Xpos = XPos/(np.mean(Skala)/2)
plt.plot(Xpos, YPos, 'bx', label='')
plt.xlabel('[V]') #+++
plt.ylabel('[cm]') #+++
plt.ylim(-0.05, 1.3)
plt.savefig('../plots/Verteilung140C.pdf')
print('Tabelle:')
for i in range(0,22):
    print('        ', XPos[i], ' & ', Xpos[i], ' & ', YPos[i], '\\\ ')

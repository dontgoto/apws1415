import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

Skala =np.array([3.45,3.5,3.35,3.5])
Uskala = ufloat(np.mean(Skala),np.std(Skala))
Uskala = Uskala/5
print('Uskala')
print('{:L}'.format(Uskala))
regr = np.array([3.5,4,3.5,4.5,4.5,5,5,6,6,7,10,15,18])
Uregr = ufloat(np.mean(regr),np.std(regr))
print('Uregr')
print('{:L}'.format(Uregr))
sp = 10.4#cm
VIon =sp / Uskala
print('Vion')
print('{:L}'.format(VIon))

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat
np.set_printoptions(precision=4)

Skala =np.array([2.35,2.45,2.55,2.55,2.6,2.55,2.55,2.55,2.55,2.55])
Uskala = ufloat(np.mean(Skala),np.std(Skala))
print('Uskala: ')
print('{:L}'.format(Uskala))
YPos = np.array([0.3,0.5,0.5,0.4,0.4,0.4,0.4,0.4,0.5,0.4,0.4,0.5,0.4,0.5,0.4,0.5,0.5,0.6,0.5,0.6,0.7,0.9,1.3,0.3,0.0])#y-werte in 1 cm schritten
XPos = np.array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25])
Xpos = XPos/np.mean(Skala)
UXpos = XPos/Uskala
plt.plot(Xpos, YPos, 'bx', label='')
plt.xlabel('[V]') #+++
plt.ylabel('[cm]') #+++
plt.savefig('../plots/Verteilung26C.pdf')
print('Tabelle:')
for i in range(0,24):
    print('        ', XPos[i], ' & ', Xpos[i], ' & ', YPos[i], '\\\ ')
print(UXpos[22])

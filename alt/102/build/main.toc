\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Torsion eines Zylinders}{4}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Magnetischer Moment eines Permanentmagneten}{5}{subsection.1.2}
\contentsline {section}{\numberline {2}Durchführung}{6}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Bestimmung des Torsionsmoduls G}{9}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Bestimmung des Elastizitätsmoduls}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bestimmung der Poissonschen Querkontraktionszahl $\mitmu $ und des Kompressionsmoduls Q}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Bestimmung des magnetischen Moments}{10}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Bestimmung des Erdmagnetfelds}{12}{subsection.3.5}
\contentsline {section}{\numberline {4}Diskussion}{12}{section.4}
\contentsline {section}{\nonumberline Literatur}{13}{section*.12}

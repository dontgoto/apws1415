import uncertainties
import matplotlib as mpl
import matplotlib.pyplot as plt
from uncertainties import ufloat
import uncertainties.unumpy as unp
import numpy as np
from scipy.optimize import curve_fit

dicke = np.genfromtxt("../messwerte/1.txt")
T = np.genfromtxt("../messwerte/b.txt")
T = unp.uarray(T, np.std(T))
print(T.mean())
print("dicke:",np.mean(dicke), "+-", np.std(dicke))
dicke = unp.uarray(dicke, np.std(dicke))
dicke = dicke * 0.001

masse = ufloat(512.2 , 0.20488)
masse = masse * 0.001
radius = ufloat(25.38 , 0.017766)
radius = radius *0.001
länge = ufloat(0.6245 , 0.0005)
trägheitsmoment = 2/5 * masse * radius**2
print(trägheitsmoment)
trägheitsmoment = trägheitsmoment + 22.5 *0.001*0.01*0.01
print(trägheitsmoment)

G = 128* np.pi * länge*trägheitsmoment /(T**2 * dicke **4)
print("Gexp:",G.mean())
Glit = 8.2*10**10
E= 2.08*10**11 #immer aus literatur
mu= E/(2*G) -1
mulit = E/(2*Glit) - 1
print("mulit:",mulit)
print("mu:",mu.mean())

Q= E/(9-3*(E/G))
print("Q:", Q.mean())

Qlit= E/(9-3*(E/Glit))
print("Qlit:", Qlit)

Terde = np.genfromtxt("../messwerte/a.txt")
Terde= unp.uarray(Terde, np.std(Terde))
print("Terde:", Terde.mean())







N=390
Rs=78*0.001
mu0 = 1.2566*10**(-6)
I = np.array([0.1, 0.2, 0.4, 0.6, 0.8])
Bfeld= (4/5)**(3/2) * I * mu0 * N /Rs
print("Bfeld:",Bfeld)
y = Bfeld
x = np.array([0.0,0.0,0.0,0.0,0.0])
x[0] = np.mean(np.genfromtxt("../messwerte/c1.txt"))
x[1] = np.mean(np.genfromtxt("../messwerte/c2.txt"))
x[2] = np.mean(np.genfromtxt("../messwerte/c3.txt"))
x[3] = np.mean(np.genfromtxt("../messwerte/c4.txt"))
x[4] = np.mean(np.genfromtxt("../messwerte/c5.txt"))
x = 1/x**2
#Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a*x + b  #+++
params, covariance = curve_fit(f, x, y) #+++ Namen der Variablen der Messwerte eintragen
error = np.sqrt(np.diag(covariance))
#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
print("params:",params,"error:",error)

#Plot gefittetes Polynom
x_plot = np.linspace(np.amin(0), np.amax(20), num=1000) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, 1000*f(x_plot, *params), 'b-', label='linearer Fit') #+++ Label anpassen


#Messwerte plotten
plt.plot(x, y*1000, 'kx', label='Messwerte') #+++
print("x:",x)
print("y:",y)
##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$1 / T^2\, [\mathrm{s}^{-2}]$') #+++
plt.ylabel(r'$B \,[\mathrm{mT}]$') #+++
#plt.grid(True) #+++
plt.xlim(0 ,0.04 ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(0 ,0.004*1000 ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
alpha = ufloat(0.10451506, 2.7045155e-03)
beta = ufloat(-0.00031655, 6.435061e-05)

print("Terde:", Terde.mean())
magmoment = 4*np.pi**2 * trägheitsmoment / alpha
print("magmoment:",magmoment)
print("alpha:",alpha,"beta:",beta)
Berde= alpha/(Terde**2) + beta
print("B Erde:",Berde.mean())
print("beta:",beta)



##Plot als Grafik speichern
plt.savefig('../plots/bfeld.pdf') #+++ Dateiname und Typ anpassen

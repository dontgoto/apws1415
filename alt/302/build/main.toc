\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Abgeglichene Brückenschaltungen}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Brückenschaltungen mit komplexen Widerständen}{5}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Die Wheatstonesche Brückenschaltung}{5}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Die Kapazitäts- und Induktivitätsmessbrücke}{7}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Die Maxwell-Brücke}{8}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Die Wien--Robinson-Brücke}{10}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Klirrfaktor}{11}{subsection.1.7}
\contentsline {section}{\numberline {2}Durchführung}{11}{section.2}
\contentsline {subsection}{\numberline {2.1}Die Wheatstonsche Brücke}{11}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Die Kapazität- und Induktivitätssmessbrücke}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Die Wien-Robinson-Brücke}{12}{subsection.2.3}
\contentsline {section}{\numberline {3}Auswertung}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Wheatstonesche Brücke}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Kapazitätsmessbrücke}{13}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Induktivitätsbestimmung}{13}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Wien-Robinson-Brücke}{14}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Klirrfaktor}{14}{subsection.3.5}
\contentsline {section}{\numberline {4}Diskussion}{18}{section.4}
\contentsline {section}{\nonumberline Literatur}{18}{section*.20}

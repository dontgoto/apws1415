import numpy as np
import scipy.constants as const

v, Ubr, Us =np.genfromtxt("../messwerte/eMesswerte.txt", unpack=True)
v0=240
R=1000
R2=320
C=700*10**(-9)
O=np.array(v/v0)
vLit=1/(2*const.pi*R*C)
v0diff=(v0-vLit)/(np.mean([vLit, v0])/100)
print("vLit:")
print(vLit)
print("Prozentualleabw. vom Lit. Wert:")
print(v0diff)

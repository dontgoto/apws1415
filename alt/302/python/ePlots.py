# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import ticker

##Messwerte aus Datei einlesen
nu, ubr, us = np.genfromtxt('../messwerte/eMesswerte.txt', skip_header=2, unpack=True) #+++Richtige Namen für Variablen!
nu0= 240

#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
x1 = (nu/nu0)
y1 =(ubr/us)
y2=np.sqrt(((x1**2 -1)**2)/((1- x1**2)**2 + 16*x1**2))
y3= (1/3)*np.sqrt((x1**2 -1)**2/((1- x1**2)**2 + 9*x1**2))

print(x1,y1)
##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++
#params, covariance = curve_fit(f, x, y) #+++ Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung

np.savetxt('../tabellen/tabellenwerte.txt', np.array([nu, ubr, us, x1, y1, y3 ]).T, delimiter=' & ' , newline='\\\\ \n' , header="", comments='%', fmt='%1.4f')

##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen


#Messwerte plotten
plt.plot(x1, y1, 'kx', label='Messwerte') #+++
#plt.plot(x1, y2, 'b-', label='Errechnete Funktion') #+++
plt.plot(x1, y3, 'b-', label='Theoriekurve') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r"$\nu / \nu_0$") #+++
plt.ylabel(r"$U_{\mathrm{Br} } / U_\mathrm{S}$") #+++
plt.grid(True) #+++
#plt.xlim(-3 ,135 ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.xscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX


##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/e.pdf') #+++ Dateiname und Typ anpassen

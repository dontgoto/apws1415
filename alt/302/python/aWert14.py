import numpy as np

R2, R3=np.genfromtxt("../messwerte/aMesswerte.txt", unpack=True)

R4=np.array(1000-R3)
Rx=R2*(R3/R4)
RxM=np.mean(Rx)
RxMStd=np.std(Rx)
print("RxM:")
print(RxM)
DR2=np.mean(np.array(R2/500))
DR3R4=np.mean(np.array((R3/R4)/500))
FRx=DR2+DR3R4+RxMStd
print("FehlerRx:")
print(FRx)

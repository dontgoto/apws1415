import numpy as np
import scipy.constants as const
import uncertainties.unumpy as unp
from uncertainties import ufloat


zeit, t1, t2, pa, pb, N = np.genfromtxt('../messwerte/messwerte.txt', skip_header=1, unpack=True)
#dT2dt = np.genfromtxt('../tabellen/dT2dt', skip_header=1, unpack=True)
dT2dt = unp.uarray(np.genfromtxt('../tabellen/dT2dt', unpack=True, skip_header=1 ), np.genfromtxt('../tabellen/dT2dtfehler', unpack=True, skip_header=1) )

cw = 4182
mkck = 660
m1 = 3
L=ufloat(123000, 5000)
c = m1*cw + mkck
print(c)
#1m ,5m ,10m, 15m, 20m
print("dm/dt")
dmdt = (c * dT2dt) / L
print(dmdt)
np.savetxt('../tabellen/dmdt', unp.nominal_values(dmdt) ,fmt='%1.4e', header="dm/dt bei1m,5m,10m,15m,20m:")
np.savetxt('../tabellen/dmdtfehler', unp.std_devs(dmdt), fmt='%1.4e', header="dm/dt bei1m,5m,10m,15m,20m:")

import numpy as np
import scipy.constants as const
import uncertainties.unumpy as unp
from uncertainties import ufloat

f1t1 = unp.uarray(np.genfromtxt('../plots/parameter/f1t1', unpack=True), np.genfromtxt('../plots/parameter/f1t1fehler', unpack=True) )
f1t2 = unp.uarray(np.genfromtxt('../plots/parameter/f1t2', unpack=True), np.genfromtxt('../plots/parameter/f1t2fehler', unpack=True) )

print(f1t1)
print(f1t2)

t =np.array([60, 300, 600, 900, 1200])
dT1dt= np.absolute( f1t1[1] + 2 * f1t1[2] * t)
dT2dt= np.absolute( f1t2[1] + 2 * f1t2[2] * t)
print ("done")
print (dT1dt)
print(dT2dt)
np.savetxt('../tabellen/dT1dt', unp.nominal_values(dT1dt), fmt='%1.4e', header='#T2/dt B + C * t fuer t=[60, 300, 600, 900, 1200]')
np.savetxt('../tabellen/dT2dt', unp.nominal_values(dT2dt), fmt='%1.4e', header='#T2/dt B + C * t fuer t=[60, 300, 600, 900, 1200]')
np.savetxt('../tabellen/dT1dtfehler', unp.std_devs(dT1dt), fmt='%1.4e', header='#T2/dt B + C * t fuer t=[60, 300, 600, 900, 1200]')
np.savetxt('../tabellen/dT2dtfehler', unp.std_devs(dT2dt), fmt='%1.4e', header='#T2/dt B + C * t fuer t=[60, 300, 600, 900, 1200]')

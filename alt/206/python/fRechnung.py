import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp


zeit, t1, t2, pa, pb, N = np.genfromtxt('../messwerte/messwerte.txt', skip_header=1, unpack=True)
pa = (pa+1)*100000 #druck um 1 bar nach oben
pb = (pb+1)*100000
pb= np.array(pb[np.array([0,4,9,13,18])])
pa= np.array(pa[np.array([0,4,9,13,18])])
t1= const.C2K(t1[np.array([0,4,9,13,18])])
t2= const.C2K(t2[np.array([0,4,9,13,18])])
kappa = 1.14
rho = np.array([(pa*5510*t1[0])/(pa[0]*t2)])
#rho= 5510
print('rho:',rho)
dmdt = unp.uarray(np.genfromtxt('../tabellen/dmdt', unpack=True, skip_header=1), np.genfromtxt('../tabellen/dmdtfehler', unpack=True, skip_header=1) )
print(dmdt)
Wm = (1 /(kappa -1)) * (pb * ((pa/pb)**(1/kappa))-pa) * (dmdt/rho)
print(Wm)
np.savetxt('../tabellen/Wm', Wm, fmt='%r', header='#Kompressorleistung in Watt')

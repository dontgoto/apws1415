import numpy as np
import scipy.constants as const
######################Berechnung von cgmg
m_y = (457.13 -195.0)*10**(-3)#g
m_x = (721.0-457.13)*10**(-3)#g
c_w = 4.18 #J/(g*K)
T_y = const.C2K(87.2) #K
T_x = const.C2K(28.0) #K
T_m= const.C2K(54.1)  #K
cgmg=np.absolute((c_w*m_y*(T_y-T_m)-c_w*m_x*(T_m-T_x))/(T_m-T_x))
print("m_x")
print(m_x)
print("m_y")
print(m_y)
print("T_x")
print(T_x)
print("T_y")
print(T_y)
print("T_m")
print(T_m)
print("c_w")
print(c_w)
print("cgmg:")
print(cgmg)
#######################Fertig!!!!!!!
#######################Werte einlesen.
m_AL= (254.8-14.5)*10**(-3)#g
m_ku = (378.06-14.0)*10**(-3)#g
m_Bl= (521-14.4)*10**(-3)#g
mwb1, Tb1, Twb1, Tmb1 = np.genfromtxt('../messwerte/blei.txt',unpack=True)
mwk1, Tk1, Twk1, Tmk1 = np.genfromtxt('../messwerte/kupfer.txt',unpack=True)
mwa1, Ta1, Twa1, Tma1 = np.genfromtxt('../messwerte/alu.txt',unpack=True)
mwb=mwb1*10**(-3)
mwk=mwk1*10**(-3)
mwa=mwa1*10**(-3)
Tb=const.C2K(Tb1)
Twb=const.C2K(Twb1)
Tmb=const.C2K(Tmb1)
Tk=const.C2K(Tk1)
Twk=const.C2K(Twk1)
Tmk=const.C2K(Tmk1)
Ta=const.C2K(Ta1)
Twa=const.C2K(Twa1)
Tma=const.C2K(Tma1)
print("Tb")
print(Tb)
print("Tmb")
print(Tmb)
print("Twb")
print(Twb)
print("mwb")
print(mwb)
print("mb")
print(m_Bl)
#######################Berechnung von ck!!!!!! 
ckb1=((c_w*mwb+cgmg)*(Tmb-Twb))/(m_Bl*(Tb-Tmb))
print("ckb1")
print(ckb1)
ckb=np.mean(ckb1)
ckbf=np.std(ckb1)
print("ckb:")
print(ckb)
print("ckbf")
print(ckbf)
ckk=((c_w*mwk+cgmg)*(Tmk-Twk))/(m_ku*(Tk-Tmk))
print("ckk:")
print(ckk)
cka=((c_w*mwa+cgmg)*(Tma-Twa))/(m_AL*(Ta-Tma))
print("cka:")
print(cka)
ab=29e-6 
kb=42e9
M_kb=207.2
gb=11.35e6 
V0b=M_kb/gb
Cvb=np.abs(ckb1*M_kb-(9*(ab**2) * kb* V0b *Tmb))
print("Cvb")
print(np.mean(Cvb))
print("Cpb")
print( np.mean(ckb1*M_kb))
ak=16.8e-6 
kk=136e9
M_kk=63.5
gk=8.96e6 
V0k=M_kk/gk
Cvk=np.abs(ckk*M_kk-(9*(ak**2) * kk* V0k *Tmk))
print("Cvk")
print(Cvk)
print("Cpk")
print( ckk*M_kk)
aa=23.5e-6 
ka=75e9
M_ka=27
ga=2.7e6 
V0a=M_ka/ga
Cva=np.abs(cka*M_ka-(9*(aa**2) * ka* V0a *Tma))
print("Cva")
print(Cva)
print("Cpa")
print( cka*M_ka)
cr=3*const.R/100
print("Blei")
print(np.mean(Cvb)/cr-100)
print("Kupfer")
print(Cvk/cr  -100)
print("Alu")
print(Cva/cr-100)

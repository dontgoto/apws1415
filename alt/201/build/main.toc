\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Einleitung}{3}{section.1}
\contentsline {section}{\numberline {2}Theorie}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Das Dulong-Petitsche Gesetz}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Quantenmechanische Betrachtung}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Versuchsaufbau und Durchführung}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Messung im Mischungskaloriemeter}{6}{subsection.3.1}
\contentsline {section}{\numberline {4}Auswertung}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Bestimmmung der Wärmekapazität und der Atomwärme von Blei}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Bestimmmung der Wärmekapazität und der Atomwärme von Kupfer}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Bestimmmung der Wärmekapazität und der Atomwärme von Aluminium}{8}{subsection.4.3}
\contentsline {section}{\numberline {5}Diskussion}{8}{section.5}
\contentsline {section}{\nonumberline Literatur}{9}{section*.10}

\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Natur des Lichts}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Der Photoeffekt}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Die Gegenfeldmethode}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchführung}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Messprogramm}{5}{subsection.2.1}
\contentsline {section}{\numberline {3}Auswertung}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Bestimmung der Grenzspannungen \( \mathup {U}_\text {g} \)}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Bestimmung von \(\mathup {e}_0/\mathup {h}\) und \(\mathup {A}_k\)}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Untersuchung der gelben Spektrallinie}{10}{subsection.3.3}
\contentsline {section}{\numberline {4}Diskussion}{12}{section.4}
\contentsline {section}{\nonumberline Literatur}{12}{section*.14}

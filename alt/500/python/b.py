# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
strom, spannung = np.genfromtxt('../messwerte/Bgelb.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!

#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen

plt.style.use('ggplot')


xValue1 = spannung
yValue1 = strom
##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++
#params, covariance = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))

#print('a: ',params[0])
#print('b: ',params[1])
#print('c: ',params[2])
#print('d: ',params[3])
#print('a Fehler: ',errors[0])
#print('b Fehler: ',errors[1])
#print('c Fehler: ',errors[2])
#print('d Fehler: ',errors[3])

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(xValue1), np.amax(xValue1)) # den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen


#Messwerte plotten
plt.plot(xValue1, yValue1, 'x', label='')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$U/\mathrm{V} $') #+++
plt.ylabel(r'$I/\mathrm{nA}$') #+++
#plt.grid(True) #+++
plt.xlim(-21, 21 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(-0.3, 16 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/Bplot.pdf') #+++ Dateiname und Typ anpassen
plt.show()
plt.close()




plt.plot(xValue1, yValue1, 'x', label='')
plt.legend(loc="best")
plt.xlabel(r'$U/\mathrm{V} $') #+++
plt.ylabel(r'$I/\mathrm{nA}$') #+++
plt.ylim(-0.3 ,10 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.xlim(-1 ,2 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.savefig('../plots/Bplotklein.pdf') #+++ Dateiname und Typ anpassen
plt.show()

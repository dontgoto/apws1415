# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
Igelb, Vgelb = np.genfromtxt('../messwerte/gelb.txt', skip_header=1, unpack=True)
Iviolett1, Vviolett1 = np.genfromtxt('../messwerte/violett1.txt', skip_header=1, unpack=True)
Iviolett2, Vviolett2 = np.genfromtxt('../messwerte/violett2.txt', skip_header=1, unpack=True)
Igrün, Vgrün = np.genfromtxt('../messwerte/grün.txt', skip_header=1, unpack=True)
Iblaugrün, Vblaugrün = np.genfromtxt('../messwerte/blaugrün.txt', skip_header=1, unpack=True)

Igelb = Igelb*10**-9
Iblaugrün = Iblaugrün*10**-9
Iviolett1 = Iviolett1*10**-9
Iviolett2 = Iviolett2*10**-9
Igrün = Igrün*10**-9
#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen




xValue1 = -1*Vgelb
yValue1 = np.sqrt(Igelb)
xValue2 = -1*Vblaugrün
yValue2 = np.sqrt(Iblaugrün)
xValue3 = -1*Vgrün
yValue3 = np.sqrt(Igrün)
xValue4 = -1*Vviolett1
yValue4 = np.sqrt(Iviolett1)
xValue5 = -1*Vviolett2
yValue5 = np.sqrt(Iviolett2)
##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x  #+++
params1, covariance1 = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
params2, covariance2 = curve_fit(f, xValue2, yValue2)
params3, covariance3 = curve_fit(f, xValue3, yValue3)
params4, covariance4 = curve_fit(f, xValue4, yValue4)
params5, covariance5 = curve_fit(f, xValue5, yValue5)
errors1= np.sqrt(np.diag(covariance1))
errors2= np.sqrt(np.diag(covariance2))
errors3= np.sqrt(np.diag(covariance3))
errors4= np.sqrt(np.diag(covariance4))
errors5= np.sqrt(np.diag(covariance5))

A1 = ufloat(params1[0], errors1[0])
A2 = ufloat(params2[0], errors2[0])
A3 = ufloat(params3[0], errors3[0])
A4 = ufloat(params4[0], errors4[0])
A5 = ufloat(params5[0], errors5[0])
B1 = ufloat(params1[1], errors1[1])
B2 = ufloat(params2[1], errors2[1])
B3 = ufloat(params3[1], errors3[1])
B4 = ufloat(params4[1], errors4[1])
B5 = ufloat(params5[1], errors5[1])

print('a1: ',A1)
print('a2: ',A2)
print('a3: ',A3)
print('a4: ',A4)
print('a5: ',A5)
print('b1: ',B1)
print('b2: ',B2)
print('b3: ',B3)
print('b4: ',B4)
print('b5: ',B5)
print('U_g 1', (-1*A1/B1))
print('U_g 2', (-1*A2/B2))
print('U_g 3', (-1*A3/B3))
print('U_g 4', (-1*A4/B4))
print('U_g 5', (-1*A5/B5))
#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung



plt.style.use('ggplot')
##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(xValue1), (-1*params1[0]/params1[1]))
x_plot = np.linspace(-2,100)
plt.plot(x_plot, f(x_plot, *params1)*1000000, 'y-', label='')
x_plot = np.linspace(np.amin(xValue2), (-1*params2[0]/params2[1]))
x_plot = np.linspace(-2,100)
plt.plot(x_plot, f(x_plot, *params2)*1000000, 'c-', label='')
x_plot = np.linspace(np.amin(xValue3), (-1*params3[0]/params3[1]))
x_plot = np.linspace(-2,100)
plt.plot(x_plot, f(x_plot, *params3)*1000000, 'g-', label='')
x_plot = np.linspace(np.amin(xValue4), (-1*params4[0]/params4[1]))
x_plot = np.linspace(-2,100)
plt.plot(x_plot, f(x_plot, *params4)*1000000, 'b-', label='')
x_plot = np.linspace(np.amin(xValue5), (-1*params5[0]/params5[1]))
x_plot = np.linspace(-2,100)
plt.plot(x_plot, f(x_plot, *params5)*1000000, 'm-', label='')


#Messwerte plotten
plt.plot(xValue1, yValue1*1000000, 'yx', label='Gelb (577nm)')
plt.plot(xValue2, yValue2*1000000, 'cx', label='Blaugrün (492nm)')
plt.plot(xValue3, yValue3*1000000, 'gx', label='Grün (546nm)')
plt.plot(xValue4, yValue4*1000000, 'bx', label='Violett 1 (434nm)')
plt.plot(xValue5, yValue5*1000000, 'mx', label='Violett 2 (405nm)')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$U/\mathrm{V}$') #+++
plt.ylabel(r'$\sqrt{I/\mathrm{nA}}$') #+++
#plt.grid(True) #+++
plt.xlim(-1.4 ,0.2 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(0,45 ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')



##besondere Punkte beschriften



##Plot als Grafik speichern
plt.savefig('../plots/sqrtI.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
plt.close()

#Ug Plot

x_0 = np.array([ (-1*params1[0]/params1[1]),(-1*params2[0]/params2[1]),  (-1*params3[0]/params3[1]),  (-1*params4[0]/params4[1]),  (-1*params5[0]/params5[1]) ])

wellenlaenge = np.array([577,492,546, 434, 405])
wellenlaenge = 3e8/(wellenlaenge*10**-9)
wellenlaenge = wellenlaenge* 10**-20

print(x_0)
plt.plot(x_0, wellenlaenge*10**20, 'kx', label='')
paramsB, covarianceB = curve_fit(f, x_0, wellenlaenge) # Namen der Variablen der Messwerte eintragen
errorsB = np.sqrt(np.diag(covarianceB))
x_plotB = np.linspace(-2, 0)
plt.plot(x_plotB, f(x_plotB, *paramsB)*10**20, 'k-', label='Ausgleichsgerade')

yerr1=errorsB[0] + errorsB[1] * x_0
yerr1 = yerr1*10**20
#plt.errorbar(x_0, wellenlaenge*10**20, 'kx',  xerr=xerr1, yerr=yerr1)
#plt.errorbar(x_0, f(x_0, *paramsB)*10**20, yerr=yerr1)
yerr2=wellenlaenge[0]/42*10**20
yerr3=wellenlaenge[2]/160*10**20
yerr4=wellenlaenge[1]/23*10**20
yerr6=wellenlaenge[4]/40*10**20
yerr5=wellenlaenge[3]/27*10**20
plt.errorbar(x_0[0], wellenlaenge[0]*10**20, yerr=yerr2, fmt='kx' )
plt.errorbar(x_0[2], wellenlaenge[2]*10**20, yerr=yerr3 ,fmt='kx' )
plt.errorbar(x_0[1], wellenlaenge[1]*10**20, yerr=yerr4,fmt='kx' )
plt.errorbar(x_0[3], wellenlaenge[3]*10**20, yerr=yerr5,fmt='kx' )
plt.errorbar(x_0[4], wellenlaenge[4]*10**20, yerr=yerr6,fmt='kx' )

plt.xlim(-1.3 ,-0.4 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(5e14,7.7e14 ) # Wenn auskommentiert werden limits automatisch gesetzt

plt.legend(loc="best")
plt.xlabel(r'$U_\mathrm{g}/\mathrm{V}$') #+++
plt.ylabel(r'$\nu/\mathrm{Hz}$') #+++

Ab = ufloat(paramsB[0], errorsB[0])
Bb = ufloat(paramsB[1], errorsB[1])
print("a:", Ab*10**20)
print("A_k:", Ab*(6.626e-34)*10**20)
print("A_k in eV:", Ab*(6.626e-34)*10**20/(1.602e-19))
print("e/h:", Bb*10**20)
print("e/h theorie:", (1.602/6.626)*10**15 )
plt.savefig('../plots/Ug.pdf') #+++ Dateiname und Typ anpassen
plt.show()

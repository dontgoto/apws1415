# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
tantalU, tantalI, wolframU, wolframI, silberU, silberI  = np.genfromtxt('../messwerte/widerstände.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!

silberD, kupferD, tantalD = np.genfromtxt('../messwerte/draehte.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen



xValue1 = tantalI
yValue1 = tantalU / tantalI
xValue2 = wolframI
yValue2 = wolframU / wolframI
xValue3 = silberI
yValue3 = silberU / silberI
np.set_printoptions(precision=3)
np.set_printoptions(formatter={'all':lambda x: str(x) + '\n'}, precision=3)
print(yValue1)
print(yValue2)
print(yValue3)
print('widerstände')
print(np.mean(yValue1), np.std(yValue1))
print(np.mean(yValue2), np.std(yValue2))
print(np.mean(yValue3), np.std(yValue3))
##Polynom fitten
def f(x, a, b ): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x   #+++
def fconst(x, a): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a   #+++
params1, covariance = curve_fit(fconst, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
errors1 = np.sqrt(np.diag(covariance))
params2, covariance = curve_fit(f, xValue2, yValue2) # Namen der Variablen der Messwerte eintragen
errors2 = np.sqrt(np.diag(covariance))
params3, covariance = curve_fit(fconst, xValue3, yValue3) # Namen der Variablen der Messwerte eintragen
errors3 = np.sqrt(np.diag(covariance))

_a1= ufloat(params1[0], errors1[0])
_a3= ufloat(params3[0], errors3[0])
_a2= ufloat(params2[0], errors2[0])
_b2= ufloat(params2[1], errors2[1])
print(_a1)
print(_a2)
print(_b2)
print(_a3)

print('a1: ',params1[0])
print('a1 Fehler: ',errors1[0])
print('a2: ',params2[0])
print('b2: ',params2[1])
print('a2 Fehler: ',errors2[0])
print('b2 Fehler: ',errors2[1])
print('a3: ',params3[0])
print('a3 Fehler: ',errors3[0])

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
plt.plot([0,1.6], [params1[0],params1[0]], 'k-', label='')
x_plot = np.linspace(np.amin(xValue2-100), np.amax(xValue2+100))
plt.plot(x_plot, f(x_plot, *params2), 'b-', label='')
plt.plot([0,1.6], [0.581979024565,0.581979024565], 'y-', label='')

#Messwerte plotten
plt.plot(xValue1, yValue1, 'kx', label='Tantal', markersize=7)
plt.plot(xValue2, yValue2, 'bx', label='Wolfram', markersize=7)
plt.plot(xValue3, yValue3, 'yx', label='Silber', markersize=7)


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$R/ \mathrm{\Omega}$') #+++
plt.xlabel(r'$I/ \mathrm{A}$') #+++
#plt.grid(True) #+++
plt.xlim(0.0 ,1.5 ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(0.0, 10.5 ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/widerstände.pdf') #+++ Dateiname und Typ anpassen
plt.show()
#plt.close()

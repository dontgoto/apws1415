# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
iprobe1, uhall1, iprobe2, uhall2, iprobe3, uhall3, iprobe4, uhall4, iprobe5, uhall5, iprobe6, uhall6, = np.genfromtxt('../messwerte/sliber.txt', unpack=True) #+++Richtige Namen für Variablen!
dsilber, dkupfer, dtantal, dwolfram, dzink= np.genfromtxt('../messwerte/folien.txt', unpack=True) #+++Richtige Namen für Variablen!
ddsilber, ddkupfer, ddtantal= np.genfromtxt('../messwerte/draehte.txt', unpack=True) #+++Richtige Namen für Variablen!

#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
länge = 1.730


dsilber= ufloat(np.mean(dsilber), np.std(dsilber))*10**-3
dkupfer= ufloat(np.mean(dkupfer), np.std(dkupfer))*10**-3
dtantal= ufloat(np.mean(dtantal), np.std(dtantal))*10**-3
dwolfram= ufloat(np.mean(dwolfram), np.std(dwolfram))*10**-3
dzink= ufloat(np.mean(dzink), np.std(dzink))*10**-3
print(dsilber*10**+3)
print(dkupfer*10**+3)
print(dtantal*10**+3)
print(dwolfram*10**+3)
print(dzink*10**+3)

ddsilber= ufloat(np.mean(ddsilber), np.std(ddsilber))*10**-3
ddkupfer= ufloat(np.mean(ddkupfer), np.std(ddkupfer))*10**-3
ddtantal= ufloat(np.mean(ddtantal), np.std(ddtantal))*10**-3
print(ddsilber*10**+3)
print(ddkupfer*10**+3)
print(ddtantal*10**+3)

uhall1 = uhall1*10**-3
uhall2 = uhall2*10**-3
uhall3 = uhall3*10**-3
uhall4 = uhall4*10**-3
uhall5 = uhall5*10**-3
uhall6 = uhall6*10**-3

iprobe1plus =  iprobe1[0:5]
iprobe1minus = iprobe1[6:11]
iprobe2plus =  iprobe2[0:5]
iprobe2minus = iprobe2[6:11]
iprobe3plus =  iprobe3[0:5]
iprobe3minus = iprobe3[6:11]
iprobe4plus =  iprobe4[0:5]
iprobe4minus = iprobe4[6:11]
iprobe5plus =  iprobe5[0:5]
iprobe5minus = iprobe5[6:11]
iprobe6plus =  iprobe6[0:5]
iprobe6minus = iprobe6[6:11]
uhall1plus =  uhall1[0:5]
uhall1minus = uhall1[6:11]
uhall2plus =  uhall2[0:5]
uhall2minus = uhall2[6:11]
uhall3plus =  uhall3[0:5]
uhall3minus = uhall3[6:11]
uhall4plus =  uhall4[0:5]
uhall4minus = uhall4[6:11]
uhall5plus =  uhall5[0:5]
uhall5minus = uhall5[6:11]
uhall6plus =  uhall6[0:5]
uhall6minus = uhall6[6:11]

##Polynom fitten
def f(x, a, b, ): #+++
        return a + b*x  #+++

params1plus, covariance1plus = curve_fit(f, iprobe1plus, uhall1plus)
errors1plus = np.sqrt(np.diag(covariance1plus))
params2plus, covariance2plus = curve_fit(f, iprobe2plus, uhall2plus)
errors2plus = np.sqrt(np.diag(covariance2plus))
params3plus, covariance3plus = curve_fit(f, iprobe3plus, uhall3plus)
errors3plus = np.sqrt(np.diag(covariance3plus))
params4plus, covariance4plus = curve_fit(f, iprobe4plus, uhall4plus)
errors4plus = np.sqrt(np.diag(covariance4plus))
params5plus, covariance5plus = curve_fit(f, iprobe5plus, uhall5plus)
errors5plus = np.sqrt(np.diag(covariance5plus))
params6plus, covariance6plus = curve_fit(f, iprobe6plus, uhall6plus)
errors6plus = np.sqrt(np.diag(covariance6plus))

params1minus, covariance1minus = curve_fit(f, iprobe1minus, uhall1minus)
errors1minus = np.sqrt(np.diag(covariance1minus))
params2minus, covariance2minus = curve_fit(f, iprobe2minus, uhall2minus)
errors2minus = np.sqrt(np.diag(covariance2minus))
params3minus, covariance3minus = curve_fit(f, iprobe3minus, uhall3minus)
errors3minus = np.sqrt(np.diag(covariance3minus))
params4minus, covariance4minus = curve_fit(f, iprobe4minus, uhall4minus)
errors4minus = np.sqrt(np.diag(covariance4minus))
params5minus, covariance5minus = curve_fit(f, iprobe5minus, uhall5minus)
errors5minus = np.sqrt(np.diag(covariance5minus))
params6minus, covariance6minus = curve_fit(f, iprobe6minus, uhall6minus)
errors6minus = np.sqrt(np.diag(covariance6minus))

plus1 = params1plus + params1minus
plus2 = params2plus + params2minus
plus3 = params3plus + params3minus
plus4 = params4plus + params4minus
plus5 = params5plus + params5minus
plus6 = params6plus + params6minus
print('plus1:',plus1)
print('plus1:',plus2)
print('plus1:',plus3)
print('plus1:',plus4)
print('plus1:',plus5)
print('plus1:',plus6)

bplus1 = ufloat(params1plus[1], errors1plus[1])
bplus2 = ufloat(params2plus[1], errors2plus[1])
bplus3 = ufloat(params3plus[1], errors3plus[1])
bplus4 = ufloat(params4plus[1], errors4plus[1])
bplus5 = ufloat(params5plus[1], errors5plus[1])
bplus6 = ufloat(params6plus[1], errors6plus[1])
bminus1 = ufloat(params1minus[1], errors1minus[1])
bminus2 = ufloat(params2minus[1], errors2minus[1])
bminus3 = ufloat(params3minus[1], errors3minus[1])
bminus4 = ufloat(params4minus[1], errors4minus[1])
bminus5 = ufloat(params5minus[1], errors5minus[1])
bminus6 = ufloat(params6minus[1], errors6minus[1])

minus1 = np.abs(errors1plus +errors1minus)
minus2 = np.abs(errors2plus +errors2minus)
minus3 = np.abs(errors3plus +errors3minus)
minus4 = np.abs(errors4plus +errors4minus)
minus5 = np.abs(errors5plus +errors5minus)
minus6 = np.abs(errors6plus +errors6minus)

uhall1a= ufloat(plus1[0], minus1[0])
uhall2a= ufloat(plus2[0], minus2[0])
uhall3a= ufloat(plus3[0], minus3[0])
uhall4a= ufloat(plus4[0], minus4[0])
uhall5a= ufloat(plus5[0], minus5[0])
uhall6a= ufloat(plus6[0], minus6[0])

uhall1b= ufloat(plus1[1], minus1[1])
uhall2b= ufloat(plus2[1], minus2[1])
uhall3b= ufloat(plus3[1], minus3[1])
uhall4b= ufloat(plus4[1], minus4[1])
uhall5b= ufloat(plus5[1], minus5[1])
uhall6b= ufloat(plus6[1], minus6[1])
dtantal=dsilber
print('b+')
print('{:L}'.format(bplus1*10**6))
print('{:L}'.format(bplus2*10**6))
print('{:L}'.format(bplus3*10**6))
print('{:L}'.format(bplus4*10**6))
print('{:L}'.format(bplus5*10**6))
print('{:L}'.format(bplus6*10**6))


print('b-')
print('{:L}'.format(bminus1*10**6))
print('{:L}'.format(bminus2*10**6))
print('{:L}'.format(bminus3*10**6))
print('{:L}'.format(bminus4*10**6))
print('{:L}'.format(bminus5*10**6))
print('{:L}'.format(bminus6*10**6))

print('deltab')
print('{:L}'.format(uhall1b*10**6))
print('{:L}'.format(uhall2b*10**6))
print('{:L}'.format(uhall3b*10**6))
print('{:L}'.format(uhall4b*10**6))
print('{:L}'.format(uhall5b*10**6))
print('{:L}'.format(uhall6b*10**6))

epsilon0 = 1.602e-19
kconst = -1*np.mean(np.array([uhall1b, uhall2b, uhall3b, uhall4b, uhall5b, uhall6b]))
print('kconst:', kconst)
nkonst = 1/(kconst* epsilon0 * dtantal)

strom, magnetfeld = np.genfromtxt('../messwerte/magnetfeld.txt', unpack=True) #+++Richtige Namen für Variablen!
print('test!!!!')
nneu1 = magnetfeld[9]*10**-3 /(epsilon0*dtantal * uhall1b)
nneu2 = magnetfeld[10]*10**-3 /(epsilon0*dtantal * uhall2b)
nneu3 = magnetfeld[11]*10**-3 /(epsilon0*dtantal * uhall3b)
nneu4 = magnetfeld[12]*10**-3 /(epsilon0*dtantal * uhall4b)
nneu5 = magnetfeld[13]*10**-3 /(epsilon0*dtantal * uhall5b)
nneu6 = magnetfeld[14]*10**-3 /(epsilon0*dtantal * uhall6b)
print('nneu1:',nneu1)
print('nneu2:',nneu2)
print('nneu3:',nneu3)
print('nneu4:',nneu4)
print('nneu5:',nneu5)
print('nneu6:',nneu6)
nneu = np.array([nneu1, nneu2, nneu3, nneu4, nneu5, nneu6])
print(np.mean(nneu))


widerstand=ufloat(5.87, 0.01)
querschnitt = np.pi *ddtantal**2 / 4
#print('Q:',querschnitt)
tau = np.mean(2*9.109e-31*1.730/((1.602e-19)**2 * widerstand * querschnitt * nneu))
vdrift = np.mean(10**-6/(nneu * 1.602e-19))
mu = np.mean(1.602e-19* tau /(2*9.109e-31))
efermi = np.mean((6.626e-34)**2 /(2*9.109e-31)* (3*np.mean(nneu)/(8*np.pi))**(2/3))
weglänge = np.mean(tau * (2*efermi/(9.109e-31))**0.5)
vtotal = np.mean( (2*efermi/(9.109e-31))**0.5)
print(efermi)
print(kconst)
print('konstanten:')
print(np.mean(nkonst))
print(np.mean(1))
print(np.mean(tau))
print(vdrift)
print(weglänge)
print(vtotal)
print(mu)


print('konsts')
print('{:L}'.format(np.mean(nkonst*10**-29)))
#print('{:L}'.format(1))
print(1)
print('{:L}'.format(tau*10**13))
print('{:L}'.format(vdrift*10**15))
print('{:L}'.format(weglänge*10**7))
print('{:L}'.format(vtotal*10**-6))
print('{:L}'.format(mu))
#print('a: ',params[0])
#print('b: ',params[1])
#print('c: ',params[2])
#print('d: ',params[3])
#print('a Fehler: ',errors[0])
#print('b Fehler: ',errors[1])
#print('c Fehler: ',errors[2])
#print('d Fehler: ',errors[3])

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(0), np.amax(3)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params1plus)*1000, 'k-', label='') #+++ Label anpassen
plt.plot(x_plot, f(x_plot, *params1minus)*-1000, 'b-', label='') #+++ Label anpassen
plt.plot(x_plot, f(x_plot, *plus1)*1000, 'r-', label='Differenz')
plt.plot(x_plot, f(x_plot,*plus1-minus1)*1000, 'r-', alpha=0)
plt.plot(x_plot, f(x_plot,*plus1+minus1)*1000, 'r-', alpha=0)
plt.fill_between(x_plot, f(x_plot,*plus1-minus1)*1000, f(x_plot,*plus1+minus1)*1000, facecolor='green', alpha=0.15)

#Messwerte plotten
plt.plot(iprobe1plus, uhall1plus*1000, 'kx', label='Erstmessung')
plt.plot(np.abs(iprobe1minus), uhall1minus*1000, 'bx', label='Umpolung')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$\, |I_\mathrm{P}|/\mathrm{A}$') #+++
plt.ylabel(r'$\, U/\mathrm{mV}$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(np.amin(0))  # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften

mol =6.022e+23
dichte = np.array([1665,1049,1925,714])
atommasse = 0.001*np.array([180.95,107.87,183.84,65.38])
mole = dichte/atommasse
zdichte = mole[1]*mol/np.mean(nneu)
print('z:', zdichte)
print('{:L}'.format(mu))
print('{:L}'.format(zdichte))



##Plot als Grafik speichern
#plt.savefig('../plots/halltantal.pdf') #+++ Dateiname und Typ anpassen
#plt.show()
#plt.close()

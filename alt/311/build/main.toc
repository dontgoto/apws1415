\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Hall-Effekt}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Mikroskopische Leitfähigkeitsparameter}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Durchführung}{4}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Widerstandsmessung}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Verhalten der Magnetfeldspulen}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Messung des Hall-Effekts}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Materialkonstanten}{16}{subsection.3.4}
\contentsline {section}{\numberline {4}Diskussion}{16}{section.4}
\contentsline {section}{\nonumberline Literatur}{17}{section*.18}

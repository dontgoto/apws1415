import numpy as np
import scipy.constants as const
druck, temp = np.genfromtxt('../messwerte/messwerteA', unpack=True)
reziproktemp = 1/const.C2K(temp)
#temp = const.C2K(temp)
lnDruck = np.log(druck)
np.savetxt('tabellenwerteA1.txt', np.array([druck, temp, reziproktemp, lnDruck]).T, header="Druck \t Temperatur \t 1/Temperatur \t Ln des Drucks")

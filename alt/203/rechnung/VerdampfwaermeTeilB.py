import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
import uncertainties.unumpy as unp
from uncertainties import ufloat
temp, druck = np.genfromtxt('../messwerte/messwerteB+dp')
druck = druck -0.58
print (druck)
temp = const.C2K(temp)
#print(druck)
#Druck in Si einheit umreichnen, Bar zu Pascal
druck = druck*100000

#print(druck)
covariance =  druck, temp
errors = np.sqrt(np.diag(covariance))
p = unp.uarray(druck, errors[0])
t = unp.uarray(temp, errors[1])

#L(T) = Formel auf seite 9
R= ufloat(8.3144, 0 )
#statt 8,3144 noch den richtigen wert für R einsetzen
# Parameter des Ausgleichspolynoms
# ax**3 + bx**2 + cx + d
#im altprotokoll sind die faktoren genau in entgegengesetzter reihenfolge

d, c, b ,a  = np.genfromtxt('../polynomfaktor.txt')

L = ((R*t*0.5)+unp.sqrt((R* t / 2)**2 - 0.9*(a+ b*t + c*t**2 + d*t**3) ))* (b * t + 2*c * t**2 + 3*d * t**3)/(a +b * t +c * t**2+ d * t**3)


#joule in kjoule umrechnen
L = L/1000
#graph plotten
plt.plot(temp, unp.nominal_values(L), 'k')
#plt.plot(temp, L, 'k.')
plt.xlabel('T[K]')
plt.ylabel('L in kJ/mol')
plt.xlim(370, 470)
plt.ylim(27,  43)

plt.grid(True) #+++
#plt.xticks(np.linspace(290, 470, 10)) #+++
#plt.yticks(np.linspace(15,55,21) ) #+++
#Plot Literaturwerte (Formel nach wikipedia)
plt.plot(temp, (50.09 - 0.9298*(temp/1000) - 65.19*((temp/1000)**2)), 'b-', label='Literaturwerte')

plt.savefig('Brichtigneu.pdf')
#print(a)
#print(b)
#print(L)

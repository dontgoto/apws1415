#%matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
y = np.array([1,2,4,6,9,12,16,20,25,30,36,
42,49,56,64,72,81,90,100,110,121,132,
144,156,169,182,196,210,225,240,256,272,289,
306,324,342,361,380,400,420,441,462,484,506,
529,552,576,600,625,650,676,702,729,756,784,
812,841,870,900,930,961,992])
x = np.linspace(1,62,62)
#print(x, y)

####Polynom dritten Grades fitten

def f(x, a, b):
        return a * x + b * x**2
params, covariance = curve_fit(f, x, y)


print('a =', params[0])
print('b =', params[1])
#np.savetxt('polynomfaktortest.txt', params , fmt='%1.4e')
#############



plt.plot(x, y, 'k.')
#plt.plot(x, y, 'r-') #niemals Messpunkte durch Linie verbinden
#plt.ylim(0 , 14)
#plt.xlim(np.amin(x)-7, np.amax(x)+7)

#Plot gefittetes Polynom
x_plot = np.linspace(np.amin(x), np.amax(x))
plt.plot(x_plot, f(x_plot, *params), 'b-', label='linearer Fit')
plt.savefig('atest.pdf')
#t = np.linespace(0, 10)
#plt.plot(t, 5 * t, 'r-')

k=np.linspace(1,64,160)
print('k:',k)
print('f:', np.round(2.5*params[0]*k + 2.5*params[1]*k**2))
print(len(k))

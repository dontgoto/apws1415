import numpy as np
import scipy.constants as const
import uncertainties.unumpy as unp
druck, temp, reziproktemp, druckLn = np.genfromtxt('tabellenwerteA1.txt', unpack=True)
temp = const.C2K(temp)
covariance =  druck, temp
errors = np.sqrt(np.diag(covariance))
p = unp.uarray(druck, errors[0])
t = unp.uarray(temp, errors[1])
verdampfWaerme = -unp.log(p) * t *8.3144
#vnoch den durchschnitt der Werte für die Verdampfungswärme berechnen falls wir das je was vernünftiges dafür kriegen
#alternativ einfach L= -mR setzten m= parameter a der ausgleichsgerade
print(p)
print(t)
print(verdampfWaerme)

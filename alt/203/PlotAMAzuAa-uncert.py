import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from uncertainties import ufloat
from scipy.optimize import curve_fit
b, a = np.genfromtxt('messwerte/messwerteB+dp')	#Werte aus Tabelle messwerteA einlesen*/
#a, b = np.genfromtxt('messwerte/tab1neu', unpack = True)
#a = a- 0.1 #normierung 1Bar bei 25 Grad
#a = a*100 #Umrechnung in Pascal
a = a-0.58
a= a*100000
x = np.log(a)
#x = a
y=1/const.C2K(b)		 #s.o.
print(a)
#print(np.std())
print( "least squares manuell" )
print("b", (len(y)* sum(y*x) - sum(x)*sum(y))/(len(y) * sum(y**2) - (sum(y))**2 ))
BBB =  (len(y)* sum(y*x) - sum(x)*sum(y))/(len(y) * sum(y**2) - (sum(y))**2 )
print("a", (sum(y**2)* sum(x) - sum(y)*sum(x*y))/(len(y) * sum(y**2) - (sum(y))**2 ))
AAA=  (sum(y**2)* sum(x) - sum(y)*sum(x*y))/(len(y) * sum(y**2) - (sum(y))**2 )
print("fehler b:", (np.sqrt(sum((x-AAA-BBB*y)**2)/(len(y)-2)))*np.sqrt((sum(y**2))/(len(y) * sum(y**2) - (sum(y))**2)))
print("fehler a:", (np.sqrt(sum((x-AAA-BBB*y)**2)/(len(y)-2)))*np.sqrt((len(y))/(len(y) * sum(y**2) - (sum(y))**2)))
#######Ausgleichsgerade fitten
def f(x, a, b):
        return a * x + b
params, covariance = curve_fit(f, y, x)
errors = np.sqrt(np.diag(covariance))
errors[0]= errors[0] #Güteklasse + fehler durch diskrete messanzeige
errors[1]= errors[1]
print('a =', params[0], '±', errors[0])
print('b =', params[1], '±', errors[1])
np.savetxt('linfitfaktor.txt', params , fmt='%1.4e')
#Steigung ist nur ~halb so groß wie im Altprotokol unsere Verdampfungswärme wird also auch entsprechend zu klein sein, warum?

########################

#print(x, y)
plt.plot(y, x, 'k.', label='Messwerte')		 #PLotten der x, y Werte in Schwarz als Punkte ('k.')
plt.xlabel('T[1/K]')		 #x-Achsen Beschriftung
plt.ylabel('ln(p)[ln(mBar)]')	 #y-Achsen Beschriftung
plt.ylim(np.amin(x), np.amax(x)) #y-Achsen Skallierung
plt.xlim(np.amin(y), np.amax(y))	 #x-Achsen Skallierung
plt.grid(True) #+++
#Ausgleichsgerade plotten
x_plot = np.linspace(0, 1000)
plt.plot(x_plot, f(x_plot, *params), 'b-', label='linearer Fit')
plt.legend(loc="best")
plt.savefig('plotAneuneu.pdf')	#Plot in .pdf File speichern
#t = np.linespace(0, 10)


#%matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
a, b = np.genfromtxt('messwerte/messwerteB+dp')
#b = b*10000 #umrechnung in Pascal
#Messwerte korrigieren
b = b - 0.58
print(b)
y = b
x = const.C2K(a)
#print(x, y)

####Polynom dritten Grades fitten

def f(x, a, b, c, d):
        return a * x**3 + b * x**2 + c * x + d
params, covariance = curve_fit(f, x, y)

errors = np.sqrt(np.diag(covariance))

print('a =', params[0], '±', errors[0])
print('b =', params[1], '±', errors[1])
print('c =', params[2], '±', errors[2])
print('d =', params[3], '±', errors[3])
np.savetxt('polynomfaktor.txt', params , fmt='%1.4e')
#############



plt.plot(x, y, 'k.')
#plt.plot(x, y, 'r-') #niemals Messpunkte durch Linie verbinden
plt.xlabel('T[K]') #zweiter aufgabenteil nicht als ln plot
plt.ylabel('p[Bar]')
plt.ylim(np.amin(y) ,np.amax(y))
plt.xlim(np.amin(x), np.amax(x))

#Plot gefittetes Polynom
x_plot = np.linspace(np.amin(x), np.amax(x))
plt.plot(x_plot, f(x_plot, *params), 'b-', label='linearer Fit')

plt.savefig('plotAMBzuAa-uncert.pdf')
#t = np.linespace(0, 10)
#plt.plot(t, 5 * t, 'r-')

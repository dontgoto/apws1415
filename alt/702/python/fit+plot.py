# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

np.set_printoptions(precision=4)
##Messwerte aus Datei einlesen
NAg = np.genfromtxt('../messwerte/Ag.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!

NIn = np.genfromtxt('../messwerte/In.txt', skip_header=1, unpack=True)
#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
lAg=np.size(NAg)
lIn=np.size(NIn)
tAg=10
tIn=220
t0=900
N0=164
#====================Berechnugen=====================
N=N0/t0
FN=np.sqrt(N0)/t0
NE=ufloat(N, FN)#NullEffekt
print("N0:")
print('{:+.4uL}'.format(NE))
TIn=np.zeros(lIn)
lnInd=np.zeros(lIn)
FlnInd=np.zeros(lIn)
NInd=np.zeros(lIn)
FNInd=np.zeros(lIn)
print("Time & Ngem. & N-N0 & FN-No & lnN-N0 & FlnN-N0\\\\")
for i in range(0,lIn):
    TIn[i]=((i+1) * tIn)
    NInd[i]=NIn[i]-N*tIn
    FNInd[i]= np.sqrt(NIn[i] + (FN * tIn)**2)
    lnInd[i]=np.log(NInd[i])
    FlnInd[i]=max(np.absolute(np.log(NInd[i] + FNInd[i]) - np.log(NInd[i])), np.absolute(np.log(NInd[i] - FNInd[i]) - np.log(NInd[i])))
    print("        ",TIn[i], " & ", NIn[i], " & ", NInd[i], " & ", FNInd[i], " & ", lnInd[i]," & ",FlnInd[i], " \\\\")

lnArg=np.zeros(lAg)
FlnArg=np.zeros(lAg)
TAg=np.zeros(lAg)
NArg=np.zeros(lAg)
FNArg=np.zeros(lAg)
print("LnAg:")
for i in range(0,lAg):
    TAg[i]=((i+1) * tAg)
    NArg[i]=NAg[i]-N*tAg
    FNArg[i]= np.sqrt(NAg[i] + (FN * tAg)**2)
    lnArg[i]=np.log(NArg[i])
    FlnArg[i]=min(np.absolute(np.log(NArg[i] + FNArg[i]) - np.log(NArg[i])), np.absolute(np.log(NArg[i] - FNArg[i]) - np.log(NArg[i])))
    print("        ",TAg[i], " & ", NAg[i], " & ", NArg[i], " & ", FNArg[i], " & ", lnArg[i]," & ",FlnArg[i], " \\\\")


##Polynom fitten Indium
Indium = 1
Silber=1
if(Indium):
    plt.figure()
    def f(t, m, n_0): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return np.log(n_0 * (1 - np.e**(-m * 220))) -t * m #+++
    params, covariance = curve_fit(f, TIn, lnInd, sigma=FlnInd,  maxfev = 100000000) # Namen der Variablen der Messwerte eintragen
    errors = np.sqrt(np.diag(covariance))
    uparam1= ufloat(params[0], errors[0])
    uparam2= ufloat(params[1], errors[1])
    #uparam3= ufloat(params1[2], errors[2])
    #uparam4= ufloat(params1[3], errors[3])

    #print(uparam1)

    #print(uparam2)
    print('{:L}'.format(uparam1))
    print('{:L}'.format(uparam2))
#print('{:L}'.format(uparam3))
#print('{:L}'.format(uparam4))
    print("lambda In")
    print('{:+.4uL}'.format(uparam1))
    print("N_0 In")
    print('{:+.4uL}'.format(uparam2))
    print("T In")
    print('{:+.4uL}'.format(np.log(2)/uparam1))
    paramswitherror = unp.uarray(params, errors)
    np.savetxt('../plots/parameter/InPolynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
    x_plot = np.linspace(0, 4000) # den Fit möglichst nur da plotten wo auch Messwerte sind
    plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen

#vllt errorbar plot
#xerr1=
#yerr1=

#plt.errorbar(x, y, xerr=xerr1, yerr=yerr1)

    for i in (0,2,4,6,8,10,12,14,16):
        plt.errorbar(TIn[i], lnInd[i], yerr=FlnInd[i], fmt='k.')
##vllt 1sigma bereich darstellen
#plt.plot(x_plot, f(x_plot,*plus1-minus1)1000, 'r-', alpha=0)
#plt.plot(x_plot, f(x_plot,*plus1+minus1), 'r-', alpha=0)
#plt.fill_between(x_plot, f(x_plot,*plus1-minus1)*1000, f(x_plot,*plus1+minus1)*1000, facecolor='green', alpha=0.15)


#Messwerte plotten
#plt.plot(xValue1, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
    plt.legend(loc="best")
    plt.xlabel(r'$/\mathrm{t} / \mathrm{s}$') #+++
    plt.ylabel(r'$/\mathrm{N}$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(TIn) ,np.amax(TIn) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
    plt.ylim(6.5 , 7.5) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
    plt.savefig('../plots/Indium.pdf') #+++ Dateiname und Typ anpassen
#    plt.show()
#plt.close()
#=====================Silber Plot===========================
#    plt.clf()
#    plt.cla()

if(Silber):
    plt.figure()

    def g(t, m, n_0): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return np.log(n_0 * (1 - np.e**(-m * 10))) -t * m
    for i in range(0,lAg):
        if i % 1 == 0:
            plt.errorbar(TAg[i], lnArg[i], yerr=FlnArg[i], fmt='k.')
    lnAgPlot=np.zeros(lAg)
    FlnAgPlot=np.zeros(lAg)
    TAgPlot=np.zeros(lAg)
    ti=0
    for i in range(ti, lAg):
        lnAgPlot[i-ti]=lnArg[i]
        FlnAgPlot[i-ti]=FlnArg[i]
        TAgPlot[i-ti]=TAg[i]
    x_plot = np.linspace(ti*10, 420)
    params, covariance = curve_fit(g, TAgPlot, lnAgPlot, sigma=FlnAgPlot, maxfev = 100000000) # Namen der Variablen der Messwerte eintragen
    errors = np.sqrt(np.diag(covariance))
    uparam1= ufloat(params[0], errors[0])
    uparam2= ufloat(params[1], errors[1])
    print("lambda Ag")
    print('{:+.4uL}'.format(uparam1))
    print("N_0 Ag")
    print('{:+.4uL}'.format(uparam2))
    print("T Ag")
    print('{:+.4uL}'.format(np.log(2)/uparam1))
    plt.plot(x_plot, g(x_plot, *params), 'b-', label='Ausgleichsgerade')
    plt.legend(loc="best")
    plt.savefig('../plots/Silber.pdf')
    paramswitherror = unp.uarray(params, errors)
    np.savetxt('../plots/parameter/AgPolynomfaktor.txt', paramswitherror , fmt='%r')
    #plt.show()

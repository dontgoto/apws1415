\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Ziel}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Spektren der Alkali-Metalle}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Durchführung}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Messmethode und Versuchsapparatur}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Messprogramm}{5}{subsection.2.2}
\contentsline {section}{\numberline {3}Auswertung}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Beugungswinkel der Spektrallinien des Hg-Spektrums}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Bestimmung der Eichgröße}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Berechnung der Abschirmungszahlen}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Bestimmung des Auflösungsvermögens}{7}{subsection.3.4}
\contentsline {section}{\numberline {4}Diskussion}{7}{section.4}
\contentsline {section}{\nonumberline Literatur}{7}{section*.6}

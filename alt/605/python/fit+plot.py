# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen

#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen

#gelb1 &    577.0 & 341.4 & \\
#gelb2 &    570.1 & 341.1 & \\
#grün &     546.1 & 339.0 & \\
#blaugrün & 491.6 & 334.1 & \\
#violett1 & 435.8 & 331.5 & \\
#violett2 & 407.8 & 320.7 & \\
#violett3 & 404.7 & 320.4 & \\
#UV       & 365.0 & 327.0 & \\

wellenlaenge = np.array([577.0, 570.1, 546.1, 491.6, 435.8, 407.8, 404.7, 354.0])
#wellenlaenge = wellenlaenge * 10**-9
winkel = np.array([341.4, 341.1, 339.0, 334.1, 331.5, 320.7, 320.4, 327.0])
winkel = winkel -305.2

xValue1 = np.sin(np.pi * winkel/180)
yValue1 =wellenlaenge

#ryd = 10973731
ryd = 13.6056
alpha = 7.297e-3
n = np.array([3, 3, 3, 5, 5])
z = np.array([11, 11, 11, 37, 37])
gitter = ufloat(5.6, 1.2)*10**-7
Lambda =np.array([345.1, 343.1, 341.7, 345.4, 346.1])
Lambda = Lambda - 305.2 + 5.24
print('Winkel', Lambda)
Lambda = np.pi*Lambda/180
Lambda = np.sin(Lambda)*gitter
print('Lambda',Lambda)
deltaLambda = np.array([3.35, 3.01, 3.35, 11.05, 11.05])*10**-9

ED = 6.626e-34 * 3e8 * deltaLambda / (Lambda **2)
ED = ED/(1.602e-19)
#ED *= 100000000
sigma = z - (2*n**3 * ED / (ryd*alpha**2))**(0.25)

print('ED')
#print(ED/(1.602*10**-19))
print(ED)
print(sigma)
#print('{:L}'.format(ED))
#print('{:L}'.format(sigma))


##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b * x  #+++
params, covariance = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
uparam1= ufloat(params[0], errors[0])
uparam2= ufloat(params[1], errors[1])

print('{:L}'.format(uparam1))
print('{:L}'.format(uparam2))
#print('{:L}'.format(uparam3)
#print('{:L}'.format(uparam4)

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung

##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(0.25), np.amax(0.6)) # den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen

#vllt errorbar plot
#xerr1=
#yerr1=

#plt.errorbar(x, y, xerr=xerr1, yerr=yerr1)


##vllt 1sigma bereich darstellen
#plt.plot(x_plot, f(x_plot,*plus1-minus1)1000, 'r-', alpha=0)
#plt.plot(x_plot, f(x_plot,*plus1+minus1), 'r-', alpha=0)
#plt.fill_between(x_plot, f(x_plot,*plus1-minus1)*1000, f(x_plot,*plus1+minus1)*1000, facecolor='green', alpha=0.15)


#Messwerte plotten
plt.plot(xValue1, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.ylabel(r'$\lambda /\mathrm{nm}$') #+++
plt.xlabel(r'$\mathrm{sin}(\phi)/ $') #+++
#plt.grid(True) #+++
plt.xlim(np.amin(0.25) ,np.amax(0.6) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(np.amin(350) ,np.amax(600) ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/xxx.pdf') #+++ Dateiname und Typ anpassen
plt.show()
#plt.close()

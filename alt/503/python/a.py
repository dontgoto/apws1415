import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
import uncertainties.unumpy as unp
from uncertainties import ufloat
import uncertainties.unumpy as unp
import uncertainties
import sympy
from fractions import gcd

#numpy.savetxt('arr.txt', arr, fmt='%r')

#converters = dict.fromkeys(range(num_cols), uncertainties.ufloat_fromstr)
#arr = numpy.loadtxt('arr.txt', converters=converters, dtype=object)

raster = 0.0005 #0.5mm
d = ufloat((7.625/1000), (0.0051/1000))#7.625mm s. prot
v_auf = np.zeros((6,6,3))
v_ab = np.zeros((6,6,3))
t_0 = np.zeros((6,6))
V = np.zeros(5)
V = [0, 296, 281, 270, 254, 239]
p = 101325#pascal
doel = 886#kg/m^3
dluft = 1.293#kg/m^3
g = 9.81#m/s^2
B = 6.17 /1000#s. prot 
F = ufloat(96485.3365,0)#farraday konstante
eta_l= 18.65#viskositaet Luft bei raumtemp.
#Messwerte aus Datei einlesen
t_0[5,1] = 92.20
t_0[5,2] = 44.35
t_0[5,3] = 148.27
t_0[5,4] = 52.75
t_0[5,5] = 37.76

t_0[4,1] = 54.84
t_0[4,2] = 109.86
t_0[4,3] = 108.43
t_0[4,4] = 68.90
t_0[4,5] = 92.20

t_0[3,1] = 118.36
t_0[3,2] = 70.24
t_0[3,3] = 66.24
t_0[3,4] = 52.75
t_0[3,5] = 58.33

t_0[2,1] = 54.38
t_0[2,2] = 74.84
t_0[2,3] = 13.56
t_0[2,4] = 62.77
t_0[2,5] = 88.63

t_0[1,1] = 87.40
t_0[1,2] = 81.93
t_0[1,3] = 64.33
t_0[1,4] = 104.35
t_0[1,5] = 61.86



v_auf[1,1], v_ab[1,1] = np.genfromtxt('../messwerte/296V1', unpack=True, skip_header=1 ) #+++
v_auf[1,2], v_ab[1,2] = np.genfromtxt('../messwerte/296V2', unpack=True, skip_header=1 ) #+++
v_auf[1,3], v_ab[1,3] = np.genfromtxt('../messwerte/296V3', unpack=True, skip_header=1 ) #+++
v_auf[1,4], v_ab[1,4] = np.genfromtxt('../messwerte/296V4', unpack=True, skip_header=1 ) #+++1.80
v_auf[1,5], v_ab[1,5] = np.genfromtxt('../messwerte/296V5', unpack=True, skip_header=1 ) #+++1.80

v_auf[2,1], v_ab[2,1] = np.genfromtxt('../messwerte/281V1', unpack=True, skip_header=1 ) #+++
v_auf[2,2], v_ab[2,2] = np.genfromtxt('../messwerte/281V2', unpack=True, skip_header=1 ) #+++
v_auf[2,3], v_ab[2,3] = np.genfromtxt('../messwerte/281V3', unpack=True, skip_header=1 ) #+++
v_auf[2,4], v_ab[2,4] = np.genfromtxt('../messwerte/281V4', unpack=True, skip_header=1 ) #+++ 1.80
v_auf[2,5], v_ab[2,5] = np.genfromtxt('../messwerte/281V5', unpack=True, skip_header=1 ) #+++1.80

v_auf[3,1], v_ab[3,1] = np.genfromtxt('../messwerte/270V1', unpack=True, skip_header=1 ) #+++
v_auf[3,2], v_ab[3,2] = np.genfromtxt('../messwerte/270V2', unpack=True, skip_header=1 ) #+++
v_auf[3,3], v_ab[3,3] = np.genfromtxt('../messwerte/270V3', unpack=True, skip_header=1 ) #+++
v_auf[3,4], v_ab[3,4] = np.genfromtxt('../messwerte/270V4', unpack=True, skip_header=1 ) #+++ 1.80
v_auf[3,5], v_ab[3,5] = np.genfromtxt('../messwerte/270V5', unpack=True, skip_header=1 ) #+++1.80

v_auf[4,1], v_ab[4,1] = np.genfromtxt('../messwerte/254V1', unpack=True, skip_header=1 ) #+++
v_auf[4,2], v_ab[4,2] = np.genfromtxt('../messwerte/254V2', unpack=True, skip_header=1 ) #+++
v_auf[4,3], v_ab[4,3] = np.genfromtxt('../messwerte/254V3', unpack=True, skip_header=1 ) #+++
v_auf[4,4], v_ab[4,4] = np.genfromtxt('../messwerte/254V4', unpack=True, skip_header=1 ) #+++
v_auf[4,5], v_ab[4,5] = np.genfromtxt('../messwerte/254V5', unpack=True, skip_header=1 ) #+++

v_auf[5,1], v_ab[5,1] = np.genfromtxt('../messwerte/239V1', unpack=True, skip_header=1 ) #+++
v_auf[5,2], v_ab[5,2] = np.genfromtxt('../messwerte/239V2', unpack=True, skip_header=1 ) #+++
v_auf[5,3], v_ab[5,3] = np.genfromtxt('../messwerte/239V3', unpack=True, skip_header=1 ) #+++
v_auf[5,4], v_ab[5,4] = np.genfromtxt('../messwerte/239V4', unpack=True, skip_header=1 ) #+++
v_auf[5,5], v_ab[5,5] = np.genfromtxt('../messwerte/239V5', unpack=True, skip_header=1 ) #+++

print(v_auf[1][1])
print(np.mean(v_auf[1][1]))
print(v_auf[1][1])


#Parameter des gefitteten Polynoms aus Datei einlesen
#converters = dict.fromkeys(range(num_cols), unp.ufloat_fromstr) #+++ num_cols anpassen
#parameter = np.loadtxt('arr.txt', converters=converters, dtype=object) #+++
#+++ Raw Messwerte und Parameter weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen
#v_auf[1,1]= ufloat(np.mean(v_auf[1,1]), np.std(v_auf[1,1]))




v_0 = np.zeros((6,6))
#v_0 berechnen
for i in (1,2,3,4,5):
    for j in (1,2,3,4,5):
        v_0[i][j] = raster/t_0[i][j]
        v_ab[i][j] = raster/v_ab[i][j]
        v_auf[i][j] = raster/v_auf[i][j]

V_auf = np.zeros((6,6))
V_ab = np.zeros((6,6))
#test ob messwerte benutzbar sind
auf = np.array([ufloat(0, 0)])
ab  = np.array([ufloat(0, 0)])
Vtr   = np.zeros(0)
k = 0
for i in (1,2,3,4,5):
    for j in (1,2,3,4,5):
        tempAuf =ufloat(np.mean(v_auf[i,j]), np.std(v_auf[i,j]))
        tempAb =ufloat(np.mean(v_ab[i,j]), np.std(v_ab[i,j]))
        if np.absolute(2*v_0[i][j]/(tempAb -tempAuf))<= 1.2 and np.absolute(2*v_0[i][j]/(tempAb -tempAuf))>= 0.8:
            auf = np.append(auf, tempAuf)
            ab = np.append(ab, tempAb)
            Vtr = np.append(Vtr, V[i])
            k = k+1
            var = ' verwendbar '
        else:
            var = ' nicht verwendbar '
        print('        ',V[i],'  &',j,'  &','\(','{:+.1uL}'.format(2*v_0[i][j]/(tempAb -tempAuf)) ,'\)', '  & ',var,'\\\\')
#funktionen
def r(vab, vauf):
    return unp.sqrt((9 * eta_l * np.absolute(vab-vauf))/(2 * g * (doel - dluft)))
def E(U):
    return(U/d)
def etaeff(vab, vauf):
    return(eta_l * (1/(1 + B * (1/(p * r(vab, vauf))))))
def q(etaeff, vab, vauf, U):
    return(3 * const.pi * etaeff * unp.sqrt((9/4) * (etaeff/g) * (np.absolute(vab-vauf)/(doel-dluft))) * ((vab+vauf)/E(U))) *0.000000001

#etaeff1V4 = 1.87 * (1/(1 + B * (1/(p * r(1.87, ufloat(np.mean(v_ab[1,4]), np.std(v_ab[1,4])), ufloat(np.mean(v_auf[1,4]), np.std(v_auf[1,4])))))))
Etaeff = np.array([ufloat(0, 0)])
Q      = np.array([ufloat(0, 0)])
Rad    = np.array([ufloat(0, 0)])
print("k" ,k)
print("effektive etas:")
for i in range(1,k):
    Etaeff = np.append(Etaeff, etaeff(ab[i],auf[i]))
    print("        ", '{:+.4uL}'.format(etaeff(ab[i],auf[i])))
print("ladungen:")
for i in range(1,k):
    Q = np.append(Q, q(etaeff(ab[i], auf[i]), ab[i], auf[i], Vtr[i-1]))
    print("        ", '{:+.4uL}'.format(q(etaeff(ab[i], auf[i]), ab[i], auf[i], Vtr[i-1])))
print("rad:")
for i in range(1,k):
    Rad = np.append(Rad, r(ab[i], auf[i]))
    print("        ", r(ab[i], auf[i]))
print("Tabelle mit rad etta ladung")
for i in range(1, k):
    print("        ", i, " & ", "\( ", '{:+.1uL}'.format(Rad[i]*1000), "\) & ", "\( ", '{:+.1uL}'.format(Etaeff[i]), "\) & ", "\( ", '{:+.4uL}'.format(Q[i]), "\) \\\\")
#Ausgabe der berechneten Werte zur Weiterverarbeitung in Tabellenform
#np.savetxt('../tabellen/tabellenwerte.txt', np.array([ ]).T, delimiter=' ' , newline='\\' , header="", comments='%')

#Vielleicht noch plotten...

##Plot mit errorbars
#plt.errorbar(x, unp.nominal_values(y), yerr=unp.std_devs(y), fmt='rx')


##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++

#params, covariance = curve_fit(f, a, b) #+++ Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#np.savetxt('polynomfaktor.txt', unp.uarray([params, error]) , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung

##Messwerte plotten#x ist hier r und  y ist q
for i in range(1,k):
    plt.errorbar(unp.nominal_values(Rad[i]), unp.nominal_values(Q[i]), fmt='k.') #+++
#plt.errorbar(r(1.862, np.mean(v_ab[4,4]), np.mean(v_auf[4,4])), unp.nominal_values(q4V4), xerr=unp.std_devs(q1V4), fmt='k.', label='')
#plt.errorbar(r(1.865, np.mean(v_ab[5,3]), np.mean(v_auf[5,3])), unp.nominal_values(q5V3), xerr=unp.std_devs(q5V3), fmt='k.', label='')
for i in range(1, k):
    if Q[i] > 2.4*10**(-19):
        Q[i] = Q[i]/2
    elif Q[i] > 4*10**(-19):
        Q[i] = Q[i]/3
    elif Q[i] > 5.6*10**(-19):
        Q[i] = Q[i]/4
    elif Q[i] > 10**(-18):
        Q[i] = Q[i]/8
print("Elemntarladung:")
e = np.mean(Q)
print('{:+.1uL}'.format(e))
print("NA:")
NA=F/e
print('{:+.1uL}'.format(NA))
NAL=6.0*10**(23)
eL=1.6*10**(-19)
print("abw. NA: ",100-(NA/(NAL/100)))
print("abw. e: ",100-(e/(eL/100)))

##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen


##Achsenbeschriftungen, kosmetisches
plt.xlabel(r'$\,Radius[\mathrm{m}]$')#+++
plt.ylabel(r'$\,Ladung[\mathrm{C}]$') #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.legend(loc="best")


##Plot als Grafik speichern
plt.savefig('qzur.pdf') #+++ Dateiname und Typ anpassen

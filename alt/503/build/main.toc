\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {section}{\numberline {2}Versuchsaufbau}{4}{section.2}
\contentsline {section}{\numberline {3}Versuchsdurchführung}{5}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Relevanz der Messung}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Bestimmung des Radius und der Ladung der Tropfen}{5}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Bestimmung der Elementarladung und Avogadro-Konstante}{7}{subsection.4.3}
\contentsline {section}{\numberline {5}Diskussion}{8}{section.5}
\contentsline {section}{\nonumberline Literatur}{8}{section*.7}

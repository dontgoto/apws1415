\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {section}{\numberline {1}Theorie}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Auf- und Entladevorgang eines Kondensators}{3}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Entladevorgang}{3}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Aufladevorgang}{4}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Relaxationserscheinungen unter Einfluss einer periodischen Auslenkung}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Der RC-Kreis als Integrator}{6}{subsection.1.3}
\contentsline {section}{\numberline {2}Durchführung}{7}{section.2}
\contentsline {section}{\numberline {3}Auswertung}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Berechnung des RC-Gliedes}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Abhängigkeit der Kondensatorspannungsamplitude von der Frequenz}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Phasenverschiebung der Kondensatorspannung bei variierender Frequenz}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Das RC-Glied als Integrator}{12}{subsection.3.4}
\contentsline {section}{\numberline {4}Diskussion}{13}{section.4}
\contentsline {section}{\nonumberline Literatur}{13}{section*.12}

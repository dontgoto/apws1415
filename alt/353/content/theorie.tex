\section{Theorie}
\label{sec:Theorie}

Relaxationserscheinungen bezeichnen die nicht oszillatorische Rückkehr eines Systems zurück in seinen Ausgangszustand.
Diese werden generell durch folgende Gleichung beschrieben:
~
\begin{equation}
   	\frac{\mathrm{d} A}{\mathrm{d} t} = c \left[ A(t) - A(\infty) \right] \label{1}.
\end{equation}
~
Die Änderungsrate der physikalischen Größe A wird dabei als proportional zur Abweichung von A vom Endzustand $A(\infty)$ angenommen. 
Dieser Endzustand kann in endlicher Zeit nicht völlig erreicht werden.
Wird diese Gleichung von $0$ bis zum Zeitpunkt $t$ ergibt sich:
~
\begin{eqnarray}
	\int_{A(0)}^{A(t)} \! \frac{\mathrm{d} A\prime}{A\prime  - A(\infty)} &=& \int_0^t \! c \, \mathrm{d} t\prime \label{2}\\
	\Leftrightarrow \ln{\frac{A(t) - A(\infty)}{A(0) - A(\infty)}} &=& c t \label{3}\\
	\Leftrightarrow A(t) &=& A(\infty) + \left[ A(0) - A(\infty) \right] \, e^{c t} \label{4},
\end{eqnarray}
~
wobei in \eqref{4} $c < 0$ sein muss, damit $A$ beschränkt bleibt.

\subsection{Auf- und Entladevorgang eines Kondensators}

\begin{figure}
  \centering
    \includegraphics[width=0.60\textwidth]{abbildungen/rc.jpg}
    \caption{RC-Kreis.}
    \label{rc}
\end{figure}

Als Beispiel für einen Relaxationsvorgang kann der in Abbildung \ref{rc} dargestellte Auf- und Entladevorgang eines Kondensators dienen.

\subsubsection{Entladevorgang}
Die Gleichung für die Kondensatorspannung eines Kondensators mit Kapazität $C$ und Ladung $Q$ ist wie folgt:
~
\begin{equation}
	U_\mathrm{C} = \frac{Q}{C} \label{5}.
\end{equation}
~
Nach dem Ohmschen Gesetzt führt diese zu einem Strom durch den Widerstand R
~
\begin{equation}
	I = \frac{U_\mathrm{C}}{R} \label{6}.
\end{equation}
~
Für die Ladungsänderung auf dem Kondensator ergibt sich
~
\begin{equation}
	\mathrm{d} Q = -I\mathrm{d}t \label{7}
\end{equation}

Werden  Gleichungen \eqref{7}, \eqref{6} und \eqref{5} in einander eingesetzt ergibt sich folgende Differentialgleichung
~
\begin{equation}
	\frac{\mathrm{d}Q}{\mathrm{d}t} = -\frac{1}{RC}Q(t) \label{8}.
\end{equation}

Mit 

\begin{equation}
	U(\infty) = 0 \label{9}.
\end{equation}

Integration von \eqref{4} liefert
~
\begin{equation}
	 Q(t) = Q(0) \exp{ \left( -\frac{t}{RC} \right) } \label{9}.
\end{equation}

\subsubsection{Aufladevorgang}
Der Aufladevorgang verhält sich analog zum Entladevorgang, jedoch mit den Anfangsbedingungen
~
\begin{equation}
	Q(0) = 0 \qquad \mathrm{und} \qquad Q(\infty) = C U_\mathrm{0} \label{10}.
\end{equation}

Somit ergibt sich für die Differentialgleichung 
~
\begin{equation}
	\frac{Q(t = RC)}{Q(0)} = \frac{1}{e} \approx 0,368 \label{11}.
\end{equation}
~
$RC$ wird auch als Zeitkonstante bezeichnet und gibt an wie schnell der Auf- bzw. Entladevorgang abläuft.
Für $\Delta T = RC$ ändert sich die Ladung auf dem Kondensator um 
~
\begin{equation}
	\frac{Q(t = RC)}{Q(0)} = \frac{1}{e} \label{11}
\end{equation}
~
Nach $\Delta T = 2.3 RC$ sind noch $10\%$ der Ausgangsladung vorhanden und nach $\Delta T = 4.6 RC$ noch etwa $1\%$.


\subsection{Relaxationserscheinungen unter Einfluss einer periodischen Auslenkung}
 
\begin{figure}
  \centering
    \includegraphics[width=0.60\textwidth]{abbildungen/rcharm.jpg}
    \caption{RC-Kreis mit harmonischer Anregung.}
    \label{rcharm}
\end{figure}

Im folgenden wird der in Abbildung \ref{rcharm} dargestellte RC-Schwingkreis, der von einer Wechselspannung angetrieben wird,  näher beschrieben.

Für die Kreisfrequenz $\omega$  der Wechselspannung mit
~
\begin{equation}
	U(t) = U_\mathrm{0} \cos{\omega t} \label{12}
\end{equation}
~
soll $\omega \ll \frac{1}{RC}$ gelten. Dann wird die Spannung $U_{\mathrm{C}}(t)$ praktisch gleich $U(t)$ sein. Bei größer werdender Frequenz bleibt die Auf- und Entladung immer weiter zurück und es kommt zu einer Phasenverschiebung $\phi$ zwischen beiden Spannungen, wobei gleichzeitig die Amplitude $A$ der Kondensatorspannung $U_{\mathrm{C}}$ abnimmt.

Die Frequenzabhängigkeit von Amplitude $A$ und Phase $\phi$ von $U_{\mathrm{C}}$ wird nun näher betrachtet.

Es wird versucht eine Lösung mit dem Ansatz
~
\begin{equation}
	U_{\mathrm{C}}(t) = A(\omega) \cos(\omega t + \phi {\omega}) \label{13}
\end{equation}
~
zu finden.

Nach dem 2. Kirchhoff'sche Gesetz gilt:
~
\begin{eqnarray}
	U(t) &=& U_{\mathrm{R}}(t) + U_{\mathrm{C}}(t) \label{14} \\ 
	\Rightarrow U_{\mathrm{C}} \cos{\omega t} &=& I(t)R + A(\omega) \cos{\left( \omega t + \phi \right) } \label{15}.
\end{eqnarray}
~
Dabei ist $R_{\mathrm{i}}$ der Spannungsquelle null.


Mit Hilfe von \eqref{3} und \eqref{5} lässt sich $I(t)$ durch $U_{\mathrm{C}}$ ausdrücken und durch \eqref{13}, \eqref{15} und \eqref{16} weiter umformen
~
\begin{eqnarray}
	I(t) &=& \frac{\mathrm{d}Q}{\mathrm{d}t} = C \frac{\mathrm{d}U_{\mathrm{C}}}{\mathrm{d}t} \label{16}\\ 
	\Rightarrow U_{\mathrm{C}} \cos{\omega t} &=& -A\omega RC \sin{\left( \omega t + \phi \right)} + A(\omega)\cos{\left( \omega t + \phi \right)} \label{17}.
\end{eqnarray}
~
Dabei muss \eqref{17} für alle $t$ gültig sein.

Für $\omega = \frac{\pi}{2}$ ergibt sich:
~
\begin{equation}
	0 = -\omega RC \sin{\left( \frac{\pi}{2} + \phi \right) } + \cos{\left( \frac{\pi}{2} + \phi \right) } \label{18}
\end{equation}
~
und damit
~
\begin{equation}
    \phi(\omega) = \arctan{(-\omega RC)} \label{19}.
\end{equation}


Wie bei der Beziehung erwartet geht also $\phi(\omega \ll RC) \rightarrow 0$ und $\phi(\omega \gg RC) \rightarrow \frac{\pi}{2}$.

Um die Amplitude $A$ zu berechnen nutzen wird $\omega t + \phi = \frac{\pi}{2}$  in \eqref{17} eingesetzt:
~
\begin{eqnarray}
	U_{\mathrm{0}} \cos{\left( \frac{\pi}{2} - \phi \right)} &=& -A \omega RC \label{20}\\
	\Rightarrow A(\omega) &=& -\frac{\sin{\phi}}{\omega RC}U_{\mathrm{0}} \label{21}.
\end{eqnarray}

Aus \eqref{19} kann man mit $\sin^2{\phi} + \cos^2{\phi} = 1$ die Beziehung
~
\begin{equation}
	\sin{\phi} = \frac{\omega RC}{\sqrt{1 + \omega^2 R^2 C^2}}
\end{equation}
~
ableiten und daraus ergibt sich die frequenzabhängige Amplitude
~
\begin{equation}
	A(\omega) = \frac{U_{\mathrm{0}}}{\sqrt{1+\omega^2 R^2 C^2}} \label{23}.
\end{equation}

Die Grenzwertbetrachtungen zeigen hier, dass für $\omega \rightarrow 0$ $A(\omega)$ gegen $U_0$ geht und für $\omega \rightarrow \infty$ $A(\omega)$ verschwindet. $A(\frac{1}{RC}) = \frac{U_0}{\sqrt{2}}$. Aufgrund dieser Eigenschaft können RC-Glieder als Tiefpässe verwendet werden. Nachteilig für viele Anwendungen ist allerdings, dass nach \eqref{23} $A(\omega)$ nur mit $\frac{1}{\omega}$ gegen $0$ geht.







\subsection{Der RC-Kreis als Integrator}
Aus dem Kirchhoff'schen und Ohmschen Gesetz folgt

\begin{equation}
	U(t) = U_{\mathrm{R}}(t) + U_{\mathrm{C}}(t) = I(t)R + U_{\mathrm{C}}(t) \label{24}.
\end{equation}

Mit Hilfe von \eqref{5} und \eqref{7} ergibt sich

\begin{eqnarray}
	I(t) &=& \frac{\mathrm{d}Q}{\mathrm{d}t} = C \frac{\mathrm{d}U_{\mathrm{C}}}{\mathrm{d}t}\label{25}\\
	\Rightarrow U(t) &=& RC \frac{\mathrm{d}U_{\mathrm{C}}}{\mathrm{d}t} + U_{\mathrm{C}}(t). \label{26}
\end{eqnarray}

Wenn $\omega \gg \frac{1}{RC}$ ist, ergibt sich $|U_{\mathrm{C}}| \ll |U_{\mathrm{R}}|$ und $|U_{\mathrm{C}}| \ll |U|$. 

Als Näherung ergibt sich

\begin{eqnarray}
	U(t) &=& RC\frac{\mathrm{d}U_{\mathrm{C}}}{\mathrm{d}t} \label{27} \\
	\Rightarrow U_{\mathrm{C}}(t) &=& \frac{1}{RC} \int_0^t \, U(t') \, \mathrm{d}t'. \label{28}
\end{eqnarray}

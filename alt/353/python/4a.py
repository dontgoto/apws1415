# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
Ua, ta = np.genfromtxt('../messwerte/4a.txt', skip_header=2, unpack=True) #+++Richtige Namen für Variablen!
U=Ua[np.arange(5,13,1)]
t=ta[np.arange(5,13,1)]
print(U)
Uc=np.array(np.log(U))
print(Uc)
t=t-9

#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen


##Polynom fitten
def f(x, a, b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return a + b*x  #+++
params, covariance = curve_fit(f, t, Uc) #+++ Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
unp.uarray.paramswitherror = (params, errors)
np.savetxt('../plots/parameter/4apolynomfaktor.txt', unp.uarray.paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
print(params)
print(errors)

##Plot gefittetes Polynom
x_plot = np.linspace(np.amin(t)-1, np.amax(t)+1) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='Ausgleichsgerade') #+++ Label anpassen


#Messwerte plotten
plt.plot(t, Uc, 'kx', label='Messwerte', markersize='7') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel('ms') #+++
plt.ylabel('ln(V)') #+++
plt.grid(True) #+++
plt.xlim(np.amin(t)-0.1 ,np.amax(t)+0.1) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
plt.ylim(np.amin(Uc)-0.1 ,np.amax(Uc)+0.1 ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.xscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/4a[entlad].pdf') #+++ Dateiname und Typ anpassen

# -*- coding: utf-8 -*-
#Template für plot und einen fit aus messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
U0=35.4		#s. ../messwerte/4b.txt im header
Uc, f = np.genfromtxt('../messwerte/4b.txt', skip_header=2, unpack=True) #+++Richtige Namen für Variablen!
kf=2*const.pi*f
U=Uc/U0
print(Uc)
print(kf)
#+++Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen


##Polynom fitten
def f(x,  b): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        return  1/np.sqrt(1+b**2*x**2) #+++
params, covariance = curve_fit(f, kf, U) #+++ Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
unp.uarray.paramswitherror = (params, errors)
np.savetxt('../plots/parameter/4bpolynomfaktor.txt', unp.uarray.paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung
RC=0.00067659393441633442
lit=np.array(1/np.sqrt(1+kf**2*RC**2))
print(lit)
##Plot gefittetes Polynom
x_plot = np.linspace(0.1,35000) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot, f(x_plot, *params), 'b-', label='non-linearfit') #+++ Label anpassen


#Messwerte plotten
plt.plot(kf, U, 'kx', label='Messwerte', markersize='7') #+++
plt.plot(kf, lit, 'r-', label='Literaturwert')

##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel('kf') #+++
plt.ylabel('ln(A(kf))') #+++
plt.grid(True,which="both") #+++
plt.xlim( np.amin(kf)-10, np.amax(kf)+10) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(kf)-10 ,np.amax(kf)+10 ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
plt.xscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/4b.pdf') #+++ Dateiname und Typ anpassen

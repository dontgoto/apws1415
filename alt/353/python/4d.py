import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
f, Uc, Ug, a = np.genfromtxt('../messwerte/4c.txt', skip_header=2, unpack=True)

phi=((a/1000)*f*2*const.pi)%(2*const.pi)
print(phi)
kf=f*2*const.pi
RC=0.00147799
lit=np.array(np.arctan(kf*RC))
A=(np.sin(phi)/kf*RC)
phiid=np.arctan(kf*RC)
print(phiid)
Aid=(np.sin(phiid)/kf*RC)
print(A)
print(Aid)
ax = plt.subplot(111, polar=True)
ax.plot(phi, A*1000000, 'k.', label='Messwerte')
ax.plot(phiid, Aid*1000000, color='b', linewidth=2, label='Theoriewert')
#ax.set_rmax(np.amax(Aid))
ax.grid(True)
plt.legend(loc="best")
plt.savefig('../plots/4d.pdf')

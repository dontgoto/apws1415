import numpy as np
import scipy.constants as const
import sympy as sy
CKa, n = np.genfromtxt('../messwerte/a.txt', skip_header=1, unpack=True)
vg =35.80
L = 23.954*10**(-3)
FL = 0.05 * 10**(-3)
Csp =  0.028*10**(-9)
FCsp = 0.0001*10**(-9)
C= 0.7932 *10**(-9)
FC = 0.0005 * 10**(-9)
CK=CKa*10**(-9)

vm=(1/(2*const.pi*np.sqrt(L*(1/(1/C + 2/CK))+L*Csp)))/1000
#f=(1/(2*const.pi*sy.sqrt(L*(1/(1/C + 2/CK))+L*Csp)))/1000
#print(f.diff(L))
#print(f.diff(C))
#print(f.diff(Csp))
print("vm")
print(vm)
Fvm=np.sqrt(((L**2 * FC**2)/(16 * C**4 * (1/C + 2/CK)**4 * (L/(1/C + 2/CK+Csp * L))**3 * const.pi**2))
+((L**2 * FCsp**2)/(16 * (L/(1/C + 2/CK)) + Csp * L)**3 * const.pi**2 )
+ ((1/(1/C + 2/CK) + Csp)**2 *FL**2)/((16 * (L/(1/C + 2/CK) + Csp * L)**3)* const.pi**2)  /1000)
print("Fvm")
#((L**2 * FCsp**2)/(4 * ((1/C + 2/CK)) + Csp * L)**3 * const.pi**2 ))
print(Fvm)
vp=(1/(2*const.pi*np.sqrt(L*(C+Csp))))/1000
Fvp=(1/(4 * const.pi * ((C + Csp) * L)**(3/2)) * np.sqrt((C + Csp)**2 * FL**2 + L**2 *(FC**2 + FCsp**2)/1000))
print("Fvp")
print(Fvp)
print("vp")
print(vp)
nn=(vm+vp)/(2*(vm-vp))
print("nn")
print(nn)

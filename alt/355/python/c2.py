# -*- coding: utf-8 -*-
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
Ca, messwert2 = np.genfromtxt('../messwerte/a.txt', skip_header=1, unpack=True) #+++Richtige Namen für Variablen!
dt, t1, t2 = np.genfromtxt('../messwerte/c.txt', skip_header=1, unpack=True)
f=np.arange(20000,65000,1)
print(f)
omeg=2*const.pi*f
R=85
C=0.7932*10**(-9)
Ck=Ca*(10**(-9))
U=1
L=23.954*10**(-3)
Z=omeg*L-1/omeg * ((1/C)+(1/Ck[7]))
print(Z)
I=U/(np.sqrt(4 * omeg**2 * Ck[7]**2 * R**2 * Z**2 + (1/(omeg*Ck[7]) - omeg*Ck[7]*Z**2 + omeg*R**2*Ck[7])**2))
print(I)
#+++ Raw Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen


##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++
#params, covariance = curve_fit(f, x, y) #+++ Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen


#Messwerte plotten

plt.plot(f, I, 'b-', label='Theoriekurve',)# markersize='7') #+++


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$\omega \, [\mathrm{Hz}]$') #+++
plt.ylabel(r'$I \, [\mathrm{A}]$') #+++
#plt.grid(True) #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')




##Plot als Grafik speichern
plt.savefig('../plots/c2.pdf') #+++ Dateiname und Typ anpassen

import numpy as np
import scipy.constants as const
import sympy as sy
from uncertainties import ufloat
import uncertainties.unumpy as unp
import sympy as sy
CKa, n = np.genfromtxt('../messwerte/a.txt', skip_header=1, unpack=True)
vg =35.80
L1 = 23.954*10**(-3)
FL1 = 0.05 * 10**(-3)
Csp1 =  0.028*10**(-9)
FCsp1 = 0.0001*10**(-9)
C1= 0.7932 *10**(-9)
FC1 = 0.0005 * 10**(-9)
CK=CKa*10**(-9)

L=ufloat(L1, FL1)
Csp=ufloat(Csp1,FCsp1)
C=ufloat(C1,FC1)


print("vm:")
print((1/(2*const.pi*unp.sqrt(L*(1/(1/C + 2/CK))+L*Csp)))/1000)
print("vp:")
print((1/(2*const.pi*unp.sqrt(L*(C+Csp))))/1000)
#vL,vCsp,vC,vpi,vCK = sy.var('vL vCsp vC vpi vCK')
#f=(1/(2*vpi*sy.sqrt(vL*(1/(1/vC + 2/vCK))+vL*vCsp)))
#print(f.diff(vL))
def error(f, err_vars=None):
    from sympy import Symbol, latex
    s = 0
    latex_names = dict()
    
    if err_vars == None:
        err_vars = f.free_symbols
        
    for v in err_vars:
        err = Symbol('latex_std_' + v.name)
        s += f.diff(v)**2 * err**2
        latex_names[err] = '\\sigma_{' + latex(v) + '}'
        
    return latex(sy.sqrt(s), symbol_names=latex_names)

vL,vCsp,vC,vCK = sy.var('vL vCsp vC vCK')

f=(1/(2*sy.pi*sy.sqrt(vL*(1/(1/vC + 2/vCK))+vL*vCsp)))

print(error(f))

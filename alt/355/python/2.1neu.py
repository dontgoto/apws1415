import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy.optimize import curve_fit

phi, Am, Gain = np.genfromtxt("../messwerte/2.2.txt", unpack = True)
Amp = Am/Gain
print(Amp)
thet = np.arange(361)
print(np.radians(thet))
table = np.array([phi, Am, Gain, Amp])
np.savetxt('../tabellen/3.txt', table.T, delimiter=' & ' , newline='\\\\ ' , header="", comments='%', fmt='%1.3g')
#U0=np.array(-(2/const.pi *6.61 * np.cos(thet)))
#print(U01)
def f(thet):
	return -(2/const.pi)*6.61*np.cos(np.radians(thet))
#a,b=curve_fit(f, Amp, thet)
#print(U0)

#x_plot = np.linspace(0, 350)

plt.plot(np.radians(thet+90)/const.pi, f( thet), 'b-', label='IdealPlot')
#plt.plot(thet*50, U0, 'b-', label='ideal')
plt.plot(np.radians(phi)/const.pi, Amp, 'kx', label='Messwerte')

plt.legend(loc='best')
plt.xlabel('Phase')
plt.ylabel('U[V]')
#plt.xlim(0-0.2, 2*const.pi+0.2)
plt.ylim(np.amin(Amp)-1, np.amax(Amp)+1)

plt.savefig('../plots/23.pdf')

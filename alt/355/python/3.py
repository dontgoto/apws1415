import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy.optimize import curve_fit

phase, gain, amplitude = np.genfromtxt("../messwerte/3.txt", unpack = True)
amp  = amplitude/gain
thet = np.arange(361)
table = np.array([phase, amplitude, gain, amp])
np.savetxt('../tabellen/31.txt', table.T, delimiter=' & ' , newline='\\\\ ' , header="", comments='%', fmt='%1.3g')
#U0=np.array(-(2/const.pi *6.61 * np.cos(thet)))
#print(U01)

##Polynom fitten
def f(x, a): #+++ Namen der Vorfaktoren, nicht die der Messwerte
	    return -(2/const.pi)*4.4*np.cos(x+ a)

params, covariance = curve_fit(f, np.radians(phase), amp) #+++ Namen der Variablen der Messwerte eintragen
errors = np.sqrt(np.diag(covariance))
print(params*180/np.pi)
print(errors*180/np.pi)
##Plot gefittetes Polynom
x_plot =np.linspace(np.amin(np.radians(phase-90), ), np.amax(np.radians(phase+90), ), 1000) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
plt.plot(x_plot/const.pi, f(x_plot, *params), 'b-', label='nichtlinearer Fit') #+++ Label anpassen









#plt.plot(thet*50, U0, 'b-', label='ideal')
plt.plot(np.radians(phase)/const.pi, amp, 'kx', label='Messwerte')






plt.legend(loc='best')
plt.xlabel('Phase [rad]')
plt.ylabel('U[V]')
plt.xlim(-0.2, 2.2)
plt.ylim(np.amin(amp)-0.5, np.amax(amp)+0.5)
#plt.show()
plt.savefig('../plots/3.pdf')

#__V102 Drehschwingungen__
- Materialkonstanten der Elastizität von Metallen bestimmen
- Bestimmung der Schallgeschwindigkeit
- Magnetisches Moment eines Magneten bestimmen
- Bestimmung des Erdmagnetfelds
###Messwerte
- Periodendauer der Drehschwingung
- Magnetfeldstärke

##Theorie
Elastische Konstanten
: Verhältnis zwischen wirkender Kraft und Gestalts- und Volumenänderung eines Festkörpers

Volumenkräfte
: Ändern den Bewegungszustand des Körpers

Oberflächenkräfte
: Bewirken Gestalts- und Volumenänderungen

Isotroper Körper
: Elastische Konstanten sind Richtungsunabhängig

Kompressionsmodul
: charakterisiert die Volumenelastizität

Schubmodul oder Torsionsmodul
: charakterisiert die Gestaltselastizität -> Scherung, Torsion

Poissonsche Querkontraktionszahl
: Beschreibt Längenänderung senkrecht zur Spannungsrichtung unter Einfluss einer Normalspannung

- Oberflächenkräfte sorgen für neues Kräftegleichgewicht im Körper
- 3 Konstanten für Gestaltselastizität 3 für Volumenelastizität

##Aufbau
- Elektromagneten/Helmholz-Spule
- Durch FlipFlop-Schaltung wurde jede 2. Reflektion des Spiegels aufgenommen(s. Periodendauer) 

##Durchführung
- Messung der Längen etc
- Messung der Schwingdauern ohne B-Feld
- Messung der Schwingdauern mit B-Feld der Erde
- Messung der Schwingdauern mit verschieden starkem B-Feld

##Auswertung
- Torsionsmodul mit Größenordnung 10^10
- Erdmagnetfeld weicht um 175% ab.
- Magnetischermoment(Magn. in Kugel) durch Lineareregression der Schwingdauern bei verschiedener bekannter Magn.Feldstärken, der Helmholzspule. 

###Fehlerquellen
- Kugel pendelt -> ???


#__V103 Biegung elastischer Stäbe__
- Bestimmung der Elastizitätsmodule verschiedener Metallstäbe
###Messwerte
- Länge der Stäbe
- Auslenkung in Abhängigkeit von Distanz zum Einspannpunkt
- Dicken, Radien der Stäbe

##Theorie
Spannung
: Kraft pro Fläche

Druck
: Normalspannung Sigma senkrecht zur Oberfläche

Schubspannung
: Tangentialspannung parallel zur Oberfläche

Elastizitätsmodul
: Proportionalitätskonstante zwischen Spannung und Verformung, beschreibt Längenänderung in Spannungsrichtung unter Einfluss einer Normalspannung

Neutrale Faser
: Fläche zwischen den oberen Schichten in denen Zugspannungen wirken und den unteren in denen Druckspannungen auftreten, in der keine Spannungen auftreten

Elastische Nachwirkung
: Tatsache das sich die Deformation, besonders bei Metallen mit niedrigem Schmelzpunkt, nicht sofort nach Belastung auf einen festen Wert einstellt und bei verschwinden der Spannung auch nicht sofort auf Null geht

- Flächenträgheitsmoment ???

##Aufbau
- Apparatur an der Stäbe ein- oder zweiseitig eingespannt werden können
- Gewicht am Stab
- 2 verschiebbare Messuhren mit federndem Taststift

##Durchführung
- erst Messung ohne Last
- Stäbe werden nacheinander eingespannt und bei gleichem Gewicht an verschiedenen Abständen die Auslenkungen vermessen

##Auswertung
- 
###Fehlerquellen
- zu wenig / zu stark ausgelenkt -> ???


#__107 Kugelfall-Viskosimeter nach Höppler__
###Messwerte
- Temperatur des Wassers
- Fallzeit der Kugel

##Theorie
- Auftrieb
Laminare Strömung
: tritt auf wenn Reynoldszahl < 2000 -> keine Verwirbelungen

Reynoldszahl
: ???

Stokes'sche Reibung
: Reibung für Kugelförmigen Körper der sich langsam bewegt

Viskosität
: je höher die Viskosität ist desto dickflüssiger ist eine Flüssigkeit/Gas

- Gravitation wirkt auf die Kugel nach unten, Reibungskraft wirkt entgegen der Bewegungsrichtung -> Kräftegleichgewicht und konstante Geschwindigkeit stellen sich ein
- Andradesche Gleichung
- Verlauf der dynamischen Viskosität von Wasser ???

##Aufbau
- Viskosimeter schräg gestellt, da die Kugel sonst im Fall unkontrolliert an die Wand stoßen und Wirbel bilden würde
- Fallrohr mit 3 Markierungen
- Thermometer und Thermostat zum aufheizen des Wassers

##Durchführung
- mehrere Messungen für verschiedene Temperaturen für große Kugel
- kleine Kugel nur bei Raumtemperatur
- Füllrohr mit Wasser befüllen, von Luftblasen befreien

##Auswertung
- mit Hilfe der Messreihen bei verschiedenen Temperaturen Ausgleichsrechnung für Abhängigkeit der Viskosität von Wasser von der Temperatur
- Bestimmung der Apparaturkonstante aus eta_0
###Fehlerquellen
- Luftblasen im Rohr -> ???



#__201 Dulong-Petitsches Gesetz__
###Messwerte
- Temperaturen, Massen und Mischtemperaturen

##Theorie
Äquipartitionstheorem
: Atom hat im thermischen Gleichgewicht pro Freiheitsgrad E_kin von 1/2 fkT

###spezifische Wärmekapazität
- bei konstantem Druck:
- bei konstantem Volumen:
###Begründung des Dulong-Petit-Gesetzes
- Atome durch Gitterkräfte gebunden -> können nur Schwingung um Gleichgewichtslage durchführen
- Rückstellkraft als harmonisch angenommen
- Mittlere Innere Energie also 2E_kin (Äquipartitionstheorem) -> u = kT pro Freiheitsgrad und Atom
###Quantenmechanische Betrachtung
- bei tiefen Temperaturen werden Wärmekapazitäten beliebig gering
- immer kleiner als klassisch klassisch näherbar wenn hquer omega << kT
- 

##Aufbau
- Wasserbad zum erhitzen der Messprobe
- Dewargefäß
- Temperaturmessgerät

##Durchführung
- Probe wird erhitzt
- Probe ins Dewargefäß (kaltes Wasser und Gefäß haben schon Temperatur angeglichen)
- Warten bis Temperaturgleichgewicht eintritt

##Auswertung
-Wärmekapazität und Atomwärme von Blei, Kupfer und Aluminium
###Fehlerquellen
- es wird Wärme mit der Umgebung ausgetauscht


#__203 Verdampfungswärme und Dampfdruck-Kurve__
###Messwerte
- Druck im Gasraum
- Temperatur in Flasche und Gasraum

##Theorie
Phase
: ein räumlich abgegrenzter Bereich in einem abgeschlossenem System, in dem sich ein Stoff in einem homogenem Zustand befindet

###Zustandsdiagramm
- in den Bezirken jeweils 2 Freiheitsgrade
- Auf den Grenzkurven 1 Freiheitsgrad
- am Triplepunkt kein Freiheitsgrad
- auf den Kurven existieren 2 Phasen nebeneinander
###Verdampfungswärme
- molare Verdampfungswärme ist die Energie die nötig ist um ein Mol Flüssigkeit bei gleichbleibender Temperatur in Dampf umzuwandeln
- genau genommen Temperaturabhängig
- charakteristisch für jeden Stoff
- verschwindet am kritischen Punkt
- Experiment wird dort durchgeführt wo L nahezu konstant ist
- z.B beim Kochen von Wasser auf dem Herd zu beobachten
- wird bei Kondensation wieder freigesetzt
Maxwellsche Geschwindigkeitsverteilung
: bei bestimmter Temperatur sind Moleküle mit höheren und niedrigeren Geschwindigkeiten vorhanden

###Mikroskopischer Verdampfungsvorgang
- ein Anteil der Moleküle mit höherer Geschwindigkeit sind in der Lage die Molekularen Bindungskräfte zu überwinden und die Flüssigkeitsoberfläche zu verlassen -> der Druck über der Flüssigkeit erhöht sich
- einige Moleküle aus der Gasphase treffen auf die Oberfläche und werden wieder eingefangen
- bei konstanten Bedingungen und genug Zeit stellt sich ein Gleichgewichtszustand ein
###Sättigungsdampfdruck
- der Druck der sich beim Gleichgewichtszustand einstellt
- hängt nur von der Temperatur der Flüssigkeit ab nicht vom Volumen über der Flüssigkeit
###Thermodynamische Betrachtung des Verdampfens
- Verdampfungsvorgang wird als isobar und isotherm angesehen
- nur Flüssigkeit vorhanden -> Erhitzung -> Verdampfungswärme wird zugeführt -> Flüssigkeit verdampft. -> Dampf wird abgekühlt -> Dampf kondensiert
- der Kreisprozess ist reversibel
- Clausius-Clapeyronsche Gleichung
###Näherungsannahmen
- T ist weit unter der Kritischen Temperatur
- V_F ist gegenüber V_D vernachlässigbar
- V_D gehorcht der idealen Gasgleichung
- L ist Druck- und Temperaturunabhängig

##Aufbau
###Teilversuch 1
- Mehrhalskolben mit Flüssigkeit wird von Heizplatte erwärmt
- im Mehrhalskolben ein Thermometer im Wasser und ein Thermometer im Gasraum
- Rückflusskühler kondensiert aufsteigende Dämpfe
- Manometer misst Druck im Gasraum
- mit Wasserstrahlpumpe wird evakuiert
- Woulffsche Flasche verhindert eindringen von kaltem Wasser
- es wird bei Drücken unter 1 Bar gemessen
###Teilversuch 2
- Hohler Stahlzylinder in dem Wasser ist
- elektronisches Thermometer im Zylinder
- Drucksensor mit U-Rohr und Kühlschale verbunden
- Heizwicklung im Zylinder
- es werden Drücke bei über 1 Bar gemessen

##Durchführung
- es werden konstant Temperatur und Dampfdruck abgelesen
- Wasser wird langsam aufgeheizt

##Auswertung
-aus der Steigung von ln(p) in y und 1/T in x lässt sich die Verdampfungswärme errechnen
- im Zweiten Versuchsteil Fit eines Polynoms dritten Grades
- L = -mR
- gemessene Verdampfungswärme enthält die Volumenarbeit der Ausdehnung Flüssigkeit -> Gas (L_a = p(V_D - V_F)) diesen noch Abziehen um die innere Verdampfungswärme zu erhalten
###Fehlerquellen
- Apparatur nicht ausreichend evakuiert -> ???
- zu schnell erhitzt -> kein Gleichgewichtszustand (Druck bei bestimmter Temperature eigentlich höher) -> Steigung höher -> L zu groß


#__204 Wärmeleitung von Metallen__
###Messwerte
- Temperatur von 2 Stäben an je 2 Punktepaaren links und rechts von Peltierelement
- Abstand der Thermoelemente von Peltierelement

##Theorie
###Wärmeleitung
- findet bei Temperaturgefällen statt
- Konvektion
- Wärmestrahlung
- direkter Kontakt (Wärmeleitung)
Wärmeleitfähigkeit
: die Wärmemenge die in 1s durch 1m dicke Stoffschicht mit 1m^2 Fläche fließt wenn der Temperaturunterschied 1K ist, je höher desto schneller fließt die Wärme

###Spezifische Wärme/Wärmekapazität
- die Energie die benötigt wird um 1kg des Stoffes um 1K zu erwärmen
- gibt es auch als molare Wärmekapazität
###Theorie der Wärmeleitung
- Stab wird periodisch erhitzt und abgekühlt
- Temperaturwelle setzt sich im Stab fort
###Peltierelement
-   
###Thermoelement
-    
##Aufbau
- XplorerGLX nimmt die Messdaten auf
- jeweils 2 Proben
- 4 Messpunkte pro Probe, Symmetrisch um das Peltierelement
- Heiß/Kalt Schalter
- Peltierelement oben isoliert

##Durchführung
- es werden die Amplituden der Temperaturwelle an verschiedenen Abständen vom Peltierelement gegen die Zeit gemessen
- Abstand zwischen den Sensoren ist wichtig
- Statische Methode: Stab wird dauerhaft erhitzt, 700s
- Dynamische Methode: Stab wird periodisch erhitzt/gekühlt, je 40s

##Auswertung
- Phasengeschwindigkeit der Welle erhält man aus Zeit die die Temperatur Min/Max von Messpunkt zu Messpunkt brauchen
- Amplituden werden direkt gemessen
- Wärmeleitfähigkeit ergibt sich aus Gleichung ~ dx^2 , ~1/dt^2 , ~1/(ln(A_nah/A_fern))
###Fehlerquellen
- Peltierelement heizt oben auch beim Kühlen, wenn schlecht isoliert -> A_fern zu groß -> ???
- Abstand der Sensoren nicht richtig gemessen -> ???
- identifizieren der Min/Max ist schwer -> ???

#__206 Die Wärmepumpe__
- Transport von Wärmeenergie zwischen zwei Wärmereservoiren
- Güteziffer der Pumpe, Massendurchsatz an Kühlflüsigkeit und Wirkungsgrad des Kompressors bestimmen
##Theorie
Güteziffer
: Verhältnis zwischen Transportierter Wärmemenge und aufgewendeter Arbeit

Wärmepumpe
: Vorrichtung die mit zufuhr von mechanischer Arbeit den Wärmefluss umkehrt
: Arbeitet umso günstiger je kleiner Temperaturdifferenz zwischen beiden Reservoiren ist

###Thermodynamische Analyse
- 1. Hauptsatz: kaltem Reservoir entnommene Wärmemenge gleich der an das wärmere Abgegebenen Wärmemenge
- 2. Hauptsatz: Temperaturen der beiden Reservoire ändern sich praktisch nicht -> reduzierten Wärmemengen verschwinden ???
- Vorgang wird als reversibel angenommen
##Aufbau
- Transportmedium ist Gas mit hoher Kondensationswärme
- 2 Reservoire mit jeweils verschiedenen Drücken die durch ein Druckventil getrennt sind
- im Kompressor wird das Gas adiabatisch auf p_b komprimiert -> es kondensiert in Reservoir 1 und gibt Wärme L ab
- Flüssigkeit fließt durch den Reiniger
- fließt durch Drosselventil -> nimmt Druck p_a an und Verdampft und entzieht Reservoir 2 Wärme L
- Wattmeter misst die Leistung des Kompressors
- Druckmesser in beiden Druckgebieten
- Rührmotoren für Wasser im Wärmebad
- Behälter thermisch isoliert

##Durchführung
- Reservoire mit Wasser befüllen
- Kompressor einschalten, 60s Drücke messen, Temperaturen und Leistungsaufnahme ablesen
- Rührer einschalten
- Versuch bei 50° T_2 beenden

##Auswertung
- Massendurchsatz aus der pro Zeiteinheit aus Reservoir 2 entnommenen Wärmeenergie und L sowie Molgewicht des Gases (Dichlorfluoridmethan)
- Güteziffer ergibt sich aus transportierter Energie pro aufgewendete mechanische Energie pro Zeiteinheit
- 
###Fehlerquellen
- Behälter schlecht isoliert (wärmeres Reservoir verliert Energie -> niedrigere Temperatur gemessen -> Güteziffer sinkt
- Näherung dass sich die Reservoiretemperaturen fast nicht ändern ist falsch -> ???
-Kompressor komprimiert nicht adiabatisch -> Gas wird aufgeheizt -> ???


#__303__
###Messwerte

##Theorie

##Aufbau
- Photodetektor Signaleingang
- Mischer moduliert Signal
- verrauschtes Signal läuft durch Bandpassfilter entfernt Freq. >< omega0
 Signal wird mit Referenzsignal mit Freq. omega0 gemischt
- Phasenschieber eliminiert Phasenverschiebung
- Tiefpassfilter integriert Mischsignal, zufällige Rauschsignale werden dabei zu einer Gleichspannung gemittelt
- U_aus propto U_0 cos(phi)
##Durchführung
- mit Noise-Generator
- mit Photodetektorschaltung

##Auswertung
- Zusammenhang zwischen Phase und Ausgangsspannung
- 1/x^2 Abfall vom Lichtsignal verifizieren

###Fehlerquellen


#__302__
###Messwerte

##Theorie
Klirrfaktor
: Verhältnis von Oberwelle zu Grundwelle
: zur Messung Sinusgenerator auf Sperrfrequenz einstellen und es bleiben die der Grundwelle verschiedenen Frequenzen übrig

##Aufbau

##Durchführung
- bekannte Impedanzen oder Frequenz (20Hz - 30KHz) des Wechselstroms so anpassen, dass die an der Brücke abfallende Spannung Null wird
##Auswertung
- Impedanzen von Bauteilen
- Verhalten verschiedener Schaltungen als Frequenzfilter
###Fehlerquellen


#__353__
###Messwerte
- V,t Wertepaare

##Theorie
- RC Glieder als Tiefpass
- RC Kreis als Integrator wenn omega >> 1/RC

Relaxation
: nicht oszillatorische Rückkehr eines Systems in seinen Ausgangszustand

Zeitkonstante
: beim Kondensator = RC
: Zeit nach der nur noch 1/e der Ausgangsladung vorhanden ist
##Aufbau
- RC-Kreis auf/zu
- RC-Kreis mit Wechselspannung

##Durchführung

##Auswertung
- Zeitkonstante eines RC-Glieds bestimmen
- Amplitude der Kondensatorspannung in Abhängigkeit der Frequenz
- zeitliche Verschiebung  zwischen Generator- und Kondensatorspannung bestimmen

###Fehlerquellen


#__354__
###Messwerte
- Amplituden, Messpunkte
##Theorie
Schwingfall
: R^2/4L^2 < 1/LC

Kriechfall
: R^2/4L^2 > 1/LC

Resonanzüberhöhung
:

##Aufbau

##Durchführung

##Auswertung
- optimaler widerstand für den aperiodischen Grenzfall
- Res.Überhöhung durch U_c/U_g bestimmen
###Fehlerquellen


#__355__
###Messwerte

##Theorie
- Impedanz geht für omega gegen Null oder unendlich gegen Null dazwischen zwei maxima
- Phase zwischen Generator und Schwingkreisstrom verschwindet bei Resonanzfrequenz
- 

Fundamentalschwingungen
:

Schwebung
:

##Aufbau

##Durchführung

##Auswertung
###Fehlerquellen


#__311 Hall-Effekt und Elektrizitätsleitung bei Metallen__
- es sollen mikroskopische Parameter der Leitungselektronen bestimmt werden z.b mittlere Driftgeschwindigkeit in Stromrichtung,
###Messwerte
- geometrischen Abmessungen der metallischen Proben
- elektrischen Widerstand der Proben
- Dicke metallischer Folien
- Hall-Effekt
- Flussdichte B der verwendeten Elektromagneten bei wachsendem und fallendem Strom

##Theorie
- Elektronen besetzter Bänder tragen nicht zur Leitfähigkeit bei
- 3s Band des Natrium hat nur ein Elektron und kann daher Energie aufnehmen
Elektrische Leitfähigkeit
: Proportionalitätsfaktor zwischen Strom und Spannung

Beweglichkeit µ von Leitern
: Proportionalitätskonstante zwischen Driftgeschwindigkeit und äußerer Feldstärke

Anomaler Hall-Effekt
: Hall-Effekt mit positiven Löchern statt Elektronen

- spezifische Leitfähigkeit ???
- spezifischer Widerstand ???
- Totalgeschwindigkeit ???

###Hall-Effekt
- Strom durch Metallplatte
- B-Feld liegt an -> Lorenzkraft drückt die Elektronen in y-Richtung
- E-Feld durch Ladungsdifferenzen im Metallkörper -> Gleichgewichtszustand
###Leitungselektronen
- können sich nahezu frei bewegen
- kann man wie Atome im idealen Gas behandeln
- besteht nur aus Elektronen des Leitfähigkeitbandes, des höchsten teilweise besetzen Bandes
- Metallatome im Festkörper spalten ein oder mehrere Elektronen aus ihrer äußeren Schale ab
- Metallatome liegen so eng zusammen dass die Elektronen ein gemeinsames System bilden
- Elektronen spalten sich wegen Pauli Prinzip in quasikontinuierliche Energiebänder auf
- Energiebänder überschneiden sich, bilden aber auch verbotene Zonen
- treten praktisch nicht in Wechselwirkung mit den Ionenrümpfen
- stoßen mit Fehlstellen, Strukturdefekten und Ionenrümpfen die sich wegen Wärmebewegung aus ihrer Gleichgewichtslage entfernen
- nur die Elektronen mit E etwa E_F sind für die Leitung verantwortlich
##Aufbau

##Durchführung
- Strom durch Drahtprobe um Widerstand zu messen
- Messung der Hall-Spannung mit jeweils umgekehrten Magnetfeld
##Auswertung
###Fehlerquellen
- bei Titan z.B liegen Löcher- und Elektronenleitung in vergleichbarer Stärke vor, R und U_H müssen dann in Abhängigkeit von der Temperatur untersucht werden
- es können nur sehr geringe Spannungen gemessen werden
- beide Kontaktpunkte nicht auf einem Potential


#__401 Das Michelson-Interferometer__
###Messwerte
- Interferenzringe werden gezählt
- Brechzahlen von Gasen bestimmen

##Theorie
Interferenz
: Addition der Wellenamplituden in Abhängigkeit ihrer Phasenverschiebung
: findet nur statt wenn Wellen Kohärent und monochromatisch sind
: tritt bei linear polarisierten Wellen nur auf wenn die Polarisationsvektoren senkrecht aufeinander stehen

Kohärentes Licht
: festes k, omega und phasenveschiebung; in einheitliche Richtung
: damit Ausgedehnte Lichtquellen kohärent sind dürfen die Weglängenunterschiede nach BÜndelung nicht zu groß sein, also entweder sehr große Betrachtungsdistanz oder Begrenzung mit einer Blende

Kohärenzlänge
: Wegunterschied l bei dem die Interferenzerscheinungen verschwinden

Kohärenzzeit
: Dauer des Wellenzuges l/c

Interferometer
: erlaubt die Ausmessung optischer Größen durch Interferenz

##Aufbau
- He-Ne-Laser 630nm
- CO2 und Luft als Gas

##Durchführung
- Dublettlinie nicht vermessen

##Auswertung
-0.2 bis 1.0 bar Druck Luft z=40, CO2 z=50
- nLuft = 1.0003 nCO2= 1.00045

###Fehlerquellen


#__406 Beugung am Spalt__
###Messwerte
- Intensitäten an verschiedenen x punkten

##Theorie
- Zusammenhänge zwischen Intensität(phi) in Abhängigkeit der Gestalt des beugenden Objektes (Aperaturfunktion)
Beugung von Licht
: tritt auf wenn Licht durch Öffnungen hindurchtritt oder auf undurchlässige Hindernisse trifft mit Abmessungen kleiner Strahldurchmesser

Huygensche Prinzip
:

Fresnelsche Lichtbeugung
: Lichtquelle und Beobachtungspunkt P liegen im endlichen => divergente Stahlenbündel treten auf => Strahlen die unter verschiedenen Winkeln gebeugt werden interferieren in P

Frauenhofersche Lichtbeugung
: Lichtquelle im unendlichen =< parallele Lichtbündel mit ebener Wellenfront

- (klassisches Modell immer gut wenn man über eine große Zahl von Lichtquanten mitteln kann)

##Aufbau

##Durchführung
- Einzelspalt
- Doppelspalt

##Auswertung
###Fehlerquellen


#__408 Geometrische Optik__
###Messwerte

##Theorie
Brechungsgesetz
:

Sammellinse
: reelles Bild, positive Brenn- und Bildweite

Streulinse
: virtuelles  Bild, negative Brenn- und Bildweite

Sphärische Abberation
: Brennpunkt von achsenfernen Strahlen liegt näher an der Linse als der von achsennahen

Chromatische Abberation
: Brennpunkt von blauem Licht liegt näher an der Linse als der vom roten Licht, blaues Licht wird stärker gebrochen als rotes

- Abbildungsgesetz
- Linsengleichung, folgt für dünne Linsen aus dem Abbildungsgesetz
- Brechkräfte


##Aufbau


##Durchführung
Brennweitenbestimmung nach Bessel
: G-B Abstand konstant, zwei Linsenpositionen suchen für die das Bild scharf ist
: und Brennweite nochmal mit verschiedenen Farbfiltern bestimmen
: f = e^2 -d^2)/4e

Brennweitenbestimmung nach Abbe
: 

##Auswertung
- Bestimmung von Brennweiten verschiedener Linsen(konstruktionen)

###Fehlerquellen


#__500 Der Photoeffekt__
- Austrittsarbeit der Photokathode
- Verhältnis e/h
##Theorie
Photoeffekt
: Herauslösung von Elektronen aus Metalloberflächen durch Bestrahlung mit Licht

Wellenmodell
: Licht ist eine elektromagnetische Welle
: Anwendbar wenn man über viele Feldquanten mitteln kann

Photozelle
: evakuierter Glaskolben mit 2 Elektroden
: Anode ist Drahtring der in einigen Millimetern Abstand zur Kathodenfläche angebracht ist

###Gegenfeldmethode
- bei Anode-Kathode wird ein elektrisches Feld angelegt das die Elektronen überwinden müssen
- nur Elektronen mit Kinetischer Energie größer eU kommen an
- hf = A_k + eU
###Fermi--Dirac-Statistik
- beschreibt das Verhalten eines Systems von vielen gleichen Fermionen
- Aussage über die Verteilung der Elektronenenergie im Festkörper
- basiert auf Pauli-Prinzip und der Tatsache dass man beim vertauschen von zwei Teilchen keinen statistisch zu berücksichtigen neuen Zustand erhält
- Fermi-Verteilung
Fermi-Energie
: Energiewert den die energiereichsten Elektronen wegen Pauli-Verbot am absoluten Nullpunkt besitzen

- Fermi Energie liegt bei Metallen bei einigen eV entsprechend Fermitemperatur von 10.000K
- hängt im wesentlichen von der Elektronendichte im Festkörper ab
###Fermi-Niveaus
-???

##Aufbau
- Festkörperoberfläche (Photokathode) im Vakuum wird mit Licht bestrahlt
- Photokathode strahlt Elektronen ab auf Auffängerelektrode
- Elektrode und Kathode sind mit Strommessgerät verbunden
- Fokussierlinse vor der Lichtquelle (HG-Lampe)
- an einem breitenverstellbaren Spalt wird das Licht wieder gebrochen
- Geradsichtprisma und danach noch eine Fokussierlinse die nun sichtbaren Spektrallinien auf die Kathode fokussiert
- mit einer Blende wird eine spezielle Spektralfarbe vom Prisma durchgelassen
Geradsichtprisma
: zerlegt Licht spektral ohne die Bündelachse zu ändern
: besteht aus 3 zusammengesetzten Prismen A-B-A

##Durchführung
- Zuleitungen von Picoamperemeter und Gehäuse der Photozelle gegen Störfelder abschirmen -> Koaxialkabel und Gehäuse erden (Kabel waren aber keine Koaxialkabel)
- optische Bauelemente richtig Anordnen: Kondensorlinse mit Bild des Dampfvolumens in die Spaltebene werfen dass die gleich breite wie der Spalt hat, gleiches für Abbildung der Spaltblende auf Eintrittsöffnung der Photozelle

##Auswertung
- Zahl der pro Zeiteinheit herausgelösten Elektronen ist proportional zur Lichtintensivität
- Energie der Photoelektronen ist Proportional zu Lichtwellenlänge und unabhängig von der Intensität
- Grenzfrequenz unterhalb der der Photoeffekt nicht auftritt
- Lineare Ausgleichsrechnung der 5 Spektrallinien (sqrt(I) auf y gegen V auf x)
- 5 Nullstellen bestimmen -> 5 U_g's
- Lineare Ausgleichsrechnung der 5 U_g's (U_g auf x nu auf y) -> Parameter A*h = A_k, B=e/h
###Fehlerquellen
- Kabel sind schlecht isoliert, wenn die Kabel bewegt werden entstehen Stromspitzen
- nicht Konstanz der Lichtquelle wurde aber (mit dem bloßen Auge) nicht beobachtet
- kurz nach Aufnehmen der ersten Werte Stromspitzen bzw. Abfälle ohne jeglichen erkenntlichen Grund (magnetische Spulen? Verbraucher am Stromnetz?)


#__503 Millikan-Versuch__
- Bestimmung der Elementarladung e
###Messwerte
- v_0 , v_auf, v_ab der Tröpchen durch Fallzeit und Raster
- Temperatur der Kammer über Thermowiderstand

##Theorie
- Öltröpfchen werden zerstäubt und dabei elektrisch geladen, Ladung immer ganzzahliges Vielfaches von e
- Gravitationskraft beschleunigt nach unten Stokessche Reibung entgegengesetzt zur Bewegungsrichtung
- E-Feld kann angeschaltet werden und die Tröpfen mehr nach unten beschleunigen, nach oben beschleunigen, und so nach oben beschleunigen, dass sie sich nicht mehr bewegen
- nach einiger Zeit stellt sich ein Gleichgewichtszustand zwischen Reibung und anderen Kräften ein -> v ist konstant
- Auftriebskraft wirkt eigentlich auch nach oben, aber Dichte von Luft ist vernachlässigbar
- Tröpfchenradius ist kleiner als mittlere freie Weglänge in der Luft -> Cunningham Korrektur -> effektive Viskosität von Luft wird kleiner -> gemessenes q muss nach oben korrigiert werden
##Aufbau
- Kammer mit Plattenkondensator, Plattenabstand 7,5mm
- Öffnung in der Mitte für Öltröpfchen aus dem Zerstäuber
- Halogenlampe strahlt die Tröpfchen an
- Tröpfchen werden mit Mikroskop beobachtet
- Thermowiderstand zur Kontrolle der Temperatur
- Thorium Alphastrahler zur Veränderung der Ladung auf den Tröpfchen

##Durchführung
- mit Libelle Apparatur waagerecht stellen
- Kondensatorspannung mit Multimeter kontrollieren
- Lufttemperatur alle 15min Messen
- Mikroskop fokussieren
- bei abgeschaltetem E-Feld Öl in Kammer sprühen
- 

##Auswertung
###Fehlerquellen


#__601 Der Franck-Hertz-Versuch__
- bestätigt Bohrschen Postulate
- ursprünglich Anregung des Hg Atoms
- Energie zwischen erstem und Grundzustand soll gemessen werden
- näheres über die Energieverteilung der Elektronen
- Ionisationsenergie von Hg
###Messwerte
- Beschleunigungsspannung
- Gegenfeldspannung
- Auffangstrom

##Theorie
Elektronenstoßexperiment
: Atome werden mit Elektronen beschossen, Energieverlust der Elektronen wird gemessen

Kontaktpotential
: Wird vom Beschleunigungspotential abgezogen und hat den Wert der Differenz der Fermi-Niveaus * 1/e

- die beim unelastischen Stoß der Elektronen mit Hg-Atom abgegebene Energie ist Energie zwischen erstem angeregten und Grundzustand
- ist Elektronenenergie beim Stoß gering tritt elastischer Stoß auf
- Elektronen mit Energie größer E_1-E_0 Stoßen inelastisch
- Hg Atom bleibt etwa e-8 Sekunden im ersten angeregten Zustand
- Energiemessung der Elektronen mit Gegenfeld der Auffängerelektrode
- Auffängerstrom steigt stark an wenn kinetische Energie der Elektronen größer eU wird und sinkt wider wenn sie inelastisch Stoßen
- Anfangsenergie der Elektronen im Material sorgt für weniger abrupte Änderung der Franck-Hertz-Kurve
- wenn Elektronen im Raum zwischen Kathode und Beschleunigungselektrode auftreten sorgt die Verteilung der z-Komponente der Geschwindigkeit für Verbreiterung und Abflachung der Kurve
- Dampfdruck so einstellen, dass mittlere freie Weglänge der Atome etwa 1000-4000 mal kleiner ist als Abstand zwischen Katode und Beschleunigungselektrode
- zu kleiner Dampfdruck -> mehr Elektronen ohne Wechselwirkung -> haben Energie die ausreicht um 2tes Niveau anzuregen, passiert aber kaum da geringe Stoßwahrscheinlichkeit
- zu großer Dampfdruck -> viele elastische Stöße -> viele starke Ablenkungen -> Elektronen die Auffängerelektrode erreichen nimmt stark ab
- bei erhöhter Beschleunigungsspannung treten immer mehr Elektronen aus
- 80 Elektronen 54 in abgeschlossener Schale von Xe 4f mit 14e und 5d mit 10e
- Glühdraht aus Material mit niedriger Austrittsarbeit, Netzelektrode aus einem mit hoher

##Aufbau
- monoenergetische Elektronen mit Hg-Dampf in abgeschlossenem Raum
- evakuiertes Gefäß mit einem Tropfen Quecksilber
- über T kann der Dampfdruck angepasst werden
- Glühdraht dampft Elektronen aus
- Positive Netzelektrode gegenüber Glühdraht, die die Elektronen beschleunigt
- dahinter Auffängerelektrode mit Gegenfeld
- Glasrohr in beheizbarem Blechgehäuse

##Durchführung
- integrale Energieverteilung wird für 20°C und 140-160°C gemessen
- Franck Hertz kurven bei verschiedene Temperaturen 0_B 0-60V U_A ~1V
- Ionisierungsspannung bei 100-110°C

##Auswertung
- U Abstand zwischen aufeinander folgender Maxima gibt die erste Anregungsenergie des Hg-Atoms
###Fehlerquellen


#__605 Spektren der Alkali-Atome__
- Untersuchung der Struktur der Elektronenhülle von Alkali-Atomen
- Bestimmung der inneren Abschirmungszahl
###Messwerte

##Theorie
- Alkali-Atome haben nur ein äußeres Elektron das für Anregungen in Frage kommt, Leuchtelektron das sich im Feld des Atomrumpfes befindet
- innere Gebundene Elektronen schirmen das Coulombfeld des Kerns ab
- 
Konstante der vollständigen Abschirmung
: nimmt mit n,l,z zu , da Leuchtelektron im ganzen Atom verteilt ist

Konstante der inneren Abschirmung
: kleiner als sigma1 wächst mit n und l
: ergibt sich aus Energiedifferenz der Doublett-Niveaus

Bahndrehimpulsquantenzahl l
:

Spin-Bahn-Kopplung
:

Ein-Elektronen-Näherung
: innere Elektronen Schirmen Kern nach außen ab -> Näherungsweise wie 

Doublett-Struktur
: Energieniveaus mit gleichem l und unterschiedlichem j liegen viel näher beieinander als solche mit unterschiedlichem l

- Auswahlregeln

##Aufbau
- Beugungsgitter

##Durchführung

##Auswertung
###Fehlerquellen


#__702 Aktivierung von Neutronen__ 
###Messwerte
- Zählraten von Zerfällen

##Theorie
Halbwertszeit
:

Zerfallskonstante
:

Zwischenkern oder Compoundkern
:

Thermische Neutronen
:

Nulleffekt
:

- bei Aufnahme eines Neutrons in den Kern verteilt sich die Energie auf die Nukleonen im Kern -> wenig Energie pro Nukleon -> keins kann abgestoßen werden -> Anregungszustand zerfällt nach e-16s -> Gamma Quant Emission
- bei De-Broglie Wellenlänge klein gegen Kernradius -> geometrische Optik -> Neutronen werden wie Licht
- langsame Neutronen -> Interferenz
- Resonanzabsorption wenn Neutronenenergie genau zwischen zweier Energieniveaus des Zwischenkerns -> System harmonischer Oszillatoren mit diskreten Energieeigenwerten
- sigma ~ 1/sqrt(E) ~ 1/v
- 




##Aufbau
- langsame Neutronen werden durch Beschuss von Be mit Alphateilchen erzeugt
- Abbremsung durch dicke Materieschichten (möglichst Teilchen de so schwer sind wie Neutronen)
- Energie entspricht dann schließlich etwa der mittleren kin. Energie der Moleküle in der Umgebung


##Durchführung
- 
##Auswertung
- Halbwertszeiten von Silber und Indium

###Fehlerquellen


#__704 Absorption von Alpha- und Betastrahlung__
- Bestimmung von Absorptionskoeffizienten
- Bestimmung der maximalen Energie in Al von Betastrahlung
###Messwerte

##Theorie
Wirkungsquerschnitt
: kann als Größe der Fläche bei deren Kontakt die Strahlung absorbiert wird gesehen werden

Exponentielles Absorptionsgesetz
: gilt bei höchstens einer Wechselwirkung mit der gesamten Materialschicht, N(d) = n0*e^-n*Sigma*D

Absorptionskoeffizient
: mu = n sigma, gibt an wie stark ein Material Strahlung absorbiert

###Wechselwirkung von Gammastrahlung mit Materie
Annhilation
: Photoeffekt ~z^5 nimmt mit E ab
: Kernphotoeffekt
: Paarerzeugung ~z^2 ab etwa 1MeV

Elastische Streuung
: Thomson Streuung
: Delbrück Streuung

Inelastische Streuung
: Compton Effekt ~z sink für E > 200KeV gegen 0
: Kernresonanz-Streuung

Kernphotoeffekt
: 

Compton-Effekt
: Streuung der Gammaquanten an (asymptotisch)freien Elektronen, mit Richtungsänderung, Energiebereich:

Thomsonscher Wirkungsquerschnitt
:
- Gammaquant verliert durch Photoeffekt Energie, Photoeffekt ist für stark an den Kern gebundene Kerne am wahrscheinlichsten => tritt bei großen Atomen häufiger auf

###Wechselwirkung von Betastrahlung
Rutherford Streuung
: elastisch
: geringe Energieabnahme
: große Streuung

Bremsstrahlung
: ~z^2

Inelastische Streuung an den Elektronen des Materials
- führt bei Al nach 150µm zur vollständigen absorption


##Aufbau
- Radioaktive Probe zeigt auf Geiger-Müller Zählrohr, dazwischen ist Absorbermaterial
- ganzer Aufbau mit Blei abgeschirmt

##Durchführung
- Nullmessung
- Messung der Ereignisse mit Absorbermaterial verschiedener Dicken

##Auswertung
- lineare Ausgleichsrechung ln(N) gegen Dicke des Materials gibt Absorptionskeoffizienten mu


###Fehlerquellen


#__US1__
- Schallgeschwindigkeit in Acryl bestimmen
- Abmessung und Position von Fehlerstellen untersuchen
- Abmessungen des Auges bestimmen
###Messwerte
- Probengrößen, Abstände
- Schalllaufzeit

##Theorie
Durchschallungs-Verfahren
: An einem Ende der Probe wird ein kurzer Schallimpuls ausgesendet, am anderen Ende wird der Impuls empfangen. Ist eine Fehlstelle vorhanden wird I abgeschwächt.

A-Scan
: Echoamplituden als Funktion der Laufzeit, Methode zur Abtastung von Strukturen

B-Scan
: Echoamplituden in Helligkeitsabstufungen ergeben zweidimensionales Schnittbild

TM-Scan
: Time Motion Scan, schnelle Abtastung um Bewegung von Sachen sichtbar zu machen

Impuls-Echo Verfahren
: Ultraschallsender ist auch Empfänger, Puls wird an einer Grenzfläche reflektiert und nach Rückkehr vom Empfänger aufgenommen. Höhe des Echos gibt Aufschluss über Größe der Fehlstelle. Ist c bekannt kann die Lage der Fehlstelle errechnet werden s= 0.5 ct

Piezo-Elektrischer Effekt
:

- Schall ist eine longitudinale Welle
- Akustische Impedanz
- Schall wird an Grenzflächen reflektiert
- Schall wird vom Medium mit höher werdender zurückgelegter Strecke absorbiert

##Aufbau
- Schallsonden
- zu vermessende Proben
- Computer zur Datenauswertung

##Durchführung
- zuerst Probengrößen vermessen
- Impuls-Echo und Durchschallungsverfahren der Acrylzylinder
- Acrylblock mit Fehlstellen mit Impuls-Echo und A- sowie B-Scan
- A-Scan vom Auge

##Auswertung
###Fehlerquellen
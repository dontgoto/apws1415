# -*- coding: utf-8 -*-
#Template für plot und einen Fit aus Messwerten
#Bei #+++ müssen wahrscheinlich je nach Anwendung Änderungen vorgenommen werden
#Zielort: Versuch/plot/
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
from matplotlib import ticker
import uncertainties.unumpy as unp
from uncertainties import ufloat

##Messwerte aus Datei einlesen
messwert1, messwert2 = np.genfromtxt('../messwerte/messwerteB', skip_header=2, unpack=True) #+++Richtige Namen für Variablen!

#+++ Messwerte weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen




for c1 in messwerte:
    print ("%.0f" % (c1))

xValue1 =
yValue1 =

##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++
#params, covariance = curve_fit(f, xValue1, yValue1) # Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#uparam1= ufloat(params1[0], errors[0])
#uparam2= ufloat(params1[1], errors[1])
#uparam3= ufloat(params1[2], errors[2])
#uparam4= ufloat(params1[3], errors[3])

#print('{:L}'.format(uparam1))
#print('{:L}'.format(uparam2))
#print('{:L}'.format(uparam3))
#print('{:L}'.format(uparam4))

#unp.uarray.paramswitherror = (params, error)
#np.savetxt('../plots/parameter/polynomfaktor.txt', paramswitherror , fmt='%r') #Speichern der Parameter zur Weiterverarbeitung


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(xValue1), np.amax(xValue1)) # den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen

#vllt errorbar plot
#xerr1=
#yerr1=

#plt.errorbar(x, y, xerr=xerr1, yerr=yerr1)


##vllt 1sigma bereich darstellen
#plt.plot(x_plot, f(x_plot,*plus1-minus1)1000, 'r-', alpha=0)
#plt.plot(x_plot, f(x_plot,*plus1+minus1), 'r-', alpha=0)
#plt.fill_between(x_plot, f(x_plot,*plus1-minus1)*1000, f(x_plot,*plus1+minus1)*1000, facecolor='green', alpha=0.15)


#Messwerte plotten
plt.plot(xValue1, yValue1, 'kx', label='')


##Achsenbeschriftungen, kosmetisches
plt.legend(loc="best")
plt.xlabel(r'$/\mathrm{}$') #+++
plt.ylabel(r'$/\mathrm{}$') #+++
#plt.grid(True) #+++
#plt.xlim(np.amin(xValue1) ,np.amax(xValue1) ) # Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim(np.amin(yValue1) ,np.amax(yValue1) ) # Wenn auskommentiert werden limits automatisch gesetzt
#plt.yscale('log') #+++ Vielleicht noch log Skalierung
#plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi]) #+++
#plt.yticks([-1, 0, +1]) #+++
#title('title') #+++ Titel eher als Annotation in LaTeX
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

##x und y Werte als Zehnerpotenz darstellen
#formatter = ticker.ScalarFormatter(useMathText=True)
#formatter.set_scientific(True)
#formatter.set_powerlimits((-1,1)) #+++
#ax.yaxis.set_major_formatter(formatter)


##besondere Punkte beschriften




##Plot als Grafik speichern
plt.savefig('../plots/xxx.pdf') #+++ Dateiname und Typ anpassen
plt.show()
#plt.close()

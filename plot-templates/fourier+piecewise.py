import numpy as np
import matplotlib.pyplot as plt

def cn(x, y, n, L):
    c = y * np.exp(-1j * 2. * np.pi * n * x / L)
    return c.sum()/c.size

def f(x, y, Nh, L):
    rng = np.arange(.5, Nh+.5)
    coeffs = np.array([cn(x,y,i,L) for i in rng])
    f = np.array([2. * coeffs[i] * np.exp(1j*2*i*np.pi*x/L) for i in rng])
    return f.sum(axis=0)

def piecew1(x):
    conds = [(x < 0) & (x > -L/2), (0 <= x) & (x < L/2)]
    funcs = [lambda x: x+L , lambda x: x ]
    return np.piecewise(x, conds, funcs)

def piecew2(x):
    conds = [(x < 0) & (x > -L/2), (0 < x) & (x < L/2)]
    funcs = [lambda x: -1.0  , lambda x: 1.0 ]
    return np.piecewise(x, conds, funcs)

def piecew3(x):
    conds = [(x < 0) & (x > -L/2), (0 <= x) & (x < L/2)]
    funcs = [lambda x: -L/4 -x , lambda x: x - L/4 ]
    return np.piecewise(x, conds, funcs)

def piecew4(x):
    conds = [(-0.0001 < x ) & (x  < 0.0001) , (-L/2 -0.0001< x) & ( x < -L/2+0.0001)]
    funcs = [lambda x: 1 , lambda x: -1 ]
    return np.piecewise(x, conds, funcs)

L= 3.0
Nh = 10
n= Nh
N=20000
a=L/2
b= -L/2
j=0+1.j

time = np.linspace( a, b, N )
f1 = piecew1(time)
f2 = piecew2(time)
f3 = piecew3(time)
f4 = piecew4(time)

L = np.abs(a-b)

y1 = f(time,f1,Nh,L).real
y2 = f(time,f2,Nh,L).real
y3 = f(time,f3,Nh,L).real
y4 = f(time,f4,Nh,L).real

plt.style.use('ggplot')
fig = plt.figure()
ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)

ax1.plot(time, piecew1(time))
ax1.plot(time, y1)
ax2.plot(time, piecew2(time))
ax2.plot(time, y2)
ax3.plot(time, piecew3(time))
ax3.plot(time, y3)
ax4.set_xlim(b -0.01, a)
ax4.plot(time, piecew4(time))
ax4.plot(time, y4)
plt.show()

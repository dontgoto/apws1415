import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from scipy.optimize import curve_fit
import uncertainties.unumpy as unp
from uncertainties import ufloat
import uncertainties.unumpy as unp
import uncertainties
import sympy


#numpy.savetxt('arr.txt', arr, fmt='%r')

#converters = dict.fromkeys(range(num_cols), uncertainties.ufloat_fromstr)
#arr = numpy.loadtxt('arr.txt', converters=converters, dtype=object)


#Messwerte aus Datei einlesen
a, b = np.genfromtxt('../messwerte/messwerteB', unpack=True) #+++

#Parameter des gefitteten Polynoms aus Datei einlesen
#converters = dict.fromkeys(range(num_cols), unp.ufloat_fromstr) #+++ num_cols anpassen
#parameter = np.loadtxt('arr.txt', converters=converters, dtype=object) #+++
#+++ Raw Messwerte und Parameter weiter für die vorliegende Aufgabe umrechnen, möglichst neue Variablen erstellen









for c1 in messwerte:
    print ("%.0f" % (c1))


#Ausgabe der berechneten Werte zur Weiterverarbeitung in Tabellenform
np.savetxt('../tabellen/tabellenwerte.txt', np.array([ ]).T, delimiter=' ' , newline='\\' , header="", comments='%')



#Vielleicht noch plotten...

##Plot mit errorbars
#plt.errorbar(x, unp.nominal_values(y), yerr=unp.std_devs(y), fmt='rx')


##Polynom fitten
#def f(x, a, b, c, d): #+++ Namen der Vorfaktoren, nicht die der Messwerte
        #return a + b*x + c*x**2 + d*x**3  #+++

#params, covariance = curve_fit(f, a, b) #+++ Namen der Variablen der Messwerte eintragen
#errors = np.sqrt(np.diag(covariance))
#np.savetxt('polynomfaktor.txt', unp.uarray([params, error]) , fmt='%1.4e') #Speichern der Parameter zur Weiterverarbeitung

##Messwerte plotten
#plt.plot(x, y, 'k.', label='') #+++


##Plot gefittetes Polynom
#x_plot = np.linspace(np.amin(), np.amax()) #+++ den Fit möglichst nur da plotten wo auch Messwerte sind
#plt.plot(x_plot, f(x_plot, *params), 'b-', label='') #+++ Label anpassen


##Achsenbeschriftungen, kosmetisches
#plt.xlabel('') #+++
#plt.ylabel('') #+++
#plt.xlim( , ) #+++ Graph nur in dem Bereich wo Messwerte sind oder etwas mehr
#plt.ylim( , ) #+++ Wenn auskommentiert werden limits automatisch gesetzt
#plt.legend(loc="best")


##Plot als Grafik speichern
#plt.savefig('plotAMBzuAa-uncert.pdf') #+++ Dateiname und Typ anpassen
